package com.marriage;

public interface ProcessListener {

	public void onStart();
	public void onFinish();
}
