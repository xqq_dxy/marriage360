package com.marriage.network;

public interface WillCancelDelegate {
	boolean willCancelWhenOnDestroy();
}
