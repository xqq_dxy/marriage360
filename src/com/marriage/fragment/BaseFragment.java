package com.marriage.fragment;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RequestQueue.RequestFilter;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.dialog.WeddingDialog;
import com.marriage.dialog.WeddingProgressDialog;
import com.marriage.network.WillCancelDelegate;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.PreferenceHelper;

public class BaseFragment extends Fragment {

	private WeddingProgressDialog mWeddingProgressDialog;

	public boolean onBackPressed() {
		return false;
	}

	protected void executeRequest(Request<?> request) {
		final RequestQueue queue = getRequestQueue();
		queue.add(request);
	}

	public static RequestQueue getRequestQueue() {
		return MApp.get().getRequestQueue();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		dismissLoadingDialog();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (MApp.get().getRequestQueue() != null) {
			MApp.get().getRequestQueue().cancelAll(new RequestFilter() {
				@Override
				public boolean apply(Request<?> request) {
					return (request instanceof WillCancelDelegate)
							&& ((WillCancelDelegate) request)
									.willCancelWhenOnDestroy();
				}
			});
		}

	}

	public void onNewIntent(Intent intent) {
		
	}
	
	@Override
	public void onPause() {
		dismissLoadingDialog();
		MApp.get().getRequestQueue().cancelAll(filter);
		super.onPause();
	}
	
	private RequestFilter filter = new RequestFilter() {
		
		@Override
		public boolean apply(Request<?> request) {
			String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPICS;
			if(url.equals(request.getUrl())) return false;
			return true;
		}
	};

	protected void showLoadingDialog() {
		Activity a = getActivity();
		if (a == null) {
			return;
		}
		if (mWeddingProgressDialog != null
				&& mWeddingProgressDialog.isShowing()) {
			mWeddingProgressDialog.dismiss();
		}
		mWeddingProgressDialog = new WeddingProgressDialog(a);
		mWeddingProgressDialog.setCancelable(false);
		mWeddingProgressDialog.show();
	}

	protected void dismissLoadingDialog() {
		Activity a = getActivity();
		if (a == null) {
			return;
		}
		if (mWeddingProgressDialog != null
				&& mWeddingProgressDialog.isShowing()) {
			mWeddingProgressDialog.dismiss();
		}

	}

	protected void showAlert(String message) {
		FragmentActivity a = getActivity();
		if (a == null) {
			return;
		}
		new WeddingDialog.Builder(a).setTitle(R.string.prompt)
				.setMessage(message).setPositiveButton(R.string.confirm, null)
				.show();
	}

	protected void saveToCache(String key, String value) {
		long currentTime = System.currentTimeMillis();
		PreferenceHelper.setString(key, value);
		PreferenceHelper.setLong(key + "_time", currentTime);
	}

	protected String getFromCache(String key) {
		long cachedTime = PreferenceHelper.getLong(key + "_time", 0);
		long current = System.currentTimeMillis();
		if (current - cachedTime > 60 * 60 * 1000) {

			return null;
		}
		return PreferenceHelper.getString(key, null);
	}
}
