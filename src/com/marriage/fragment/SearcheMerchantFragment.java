package com.marriage.fragment;

import java.util.HashMap;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.MerchantDetailActivity;
import com.marriage.bean.Merchant;
import com.marriage.bean.MerchantListBean;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.widget.adapter.MerchantListAdapter;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class SearcheMerchantFragment extends BaseFragment implements OnClickListener, OnItemClickListener, OnScrollListener, Listener<String>, ErrorListener{

	public static final String TAG = "SearcheMerchantFragment";
	
	private ListView mMerchantListView;
	
	private MerchantListAdapter mAdapter;
	
	private boolean isFetchingData;
	
	private int pageIndex = 1;
	private int pageSize = 10;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mAdapter = new MerchantListAdapter(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_search_merchant, container, false);
		initViews(mainView);
		mainView.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				fetchData(true);
			}
		}, 500);
		return mainView;
	}
	
	private void initViews(View v){
		
		mMerchantListView = (ListView) v.findViewById(R.id.un_got_merchant_list);
		mMerchantListView.setAdapter(mAdapter);
		mMerchantListView.setOnItemClickListener(this);
		mMerchantListView.setOnScrollListener(this);
		v.findViewById(R.id.back_btn).setOnClickListener(this);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		if (!isFetchingData && totalItemCount-1 > visibleItemCount
				&& totalItemCount - (firstVisibleItem + visibleItemCount) < 3) {
			
			fetchData(false);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Merchant merchant = (Merchant) parent.getAdapter().getItem(position);
		goMerchantDetail(merchant);
		
	}

	private void goMerchantDetail(Merchant merchant) {
		Intent goMerchantDetail = new Intent(MApp.get(), MerchantDetailActivity.class);
		goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_NAME, merchant.sa_name);
		goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, merchant.sl_id);
		startActivity(goMerchantDetail);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;

		}
	}
	
	private void fetchData(boolean isFirst) {
		isFetchingData = true;
		if (isFirst){
			showLoadingDialog();
		}
		Location l = MApp.get().getCurrentLocation();
		String _x = "";
		String _y = "";
		if (l != null){
			_x = String.valueOf(l.getLongitude());
			_y = String.valueOf(l.getLatitude());
		}
		final String x = _x;
		final String y = _y;
		StringRequest sr = new StringRequest(Method.POST,
				AppConfig.INSTANCE.SERVER + Constants.URL_MERCHANT_LIST, this,
				this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				
				Map<String, String> params = new HashMap<String, String>();
				params.put("loc_type", "2");//只取未被领取的店铺
				params.put("cate_id", "");
				params.put("city_id", "1");
				params.put("user_id", MApp.get().getUser() == null || MApp.get().getUser().user == null ? "" : MApp.get().getUser().user.user_id);
				params.put("xpoint", x);
				params.put("ypoint", y);
				params.put("page_index", String.valueOf(pageIndex));
				params.put("page_size", String.valueOf(pageSize));
				return params;
			}

		};
		try {
			D.info(TAG, "body: "+new String(sr.getBody()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		executeRequest(sr);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		isFetchingData =false;
		dismissLoadingDialog();
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		isFetchingData =false;
		D.error(TAG, arg0);
		dismissLoadingDialog();
		if (TextUtils.isEmpty(arg0)){
			return;
		}
		try {
			MerchantListBean bean = new Gson().fromJson(arg0, MerchantListBean.class);
			if (bean.op_code.equals(Constants.SUCCESS)){
				mAdapter.addDatas(bean.sa_list);
				pageIndex ++ ;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	} 

}
