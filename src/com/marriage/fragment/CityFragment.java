package com.marriage.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.OnCityChoiceListener;
import com.marriage.R;
import com.marriage.bean.City;
import com.marriage.bean.CityInfo;
import com.marriage.network.WillCancelDelegate;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.SideBar;

public class CityFragment extends BaseFragment implements OnChildClickListener, Listener<String>, ErrorListener {

	public static final String TAG = "CityFragment";
	
	List<City> cityDatas;
	SideBar bar;
	private SearchModelAdapter searchAdapter;
	private ExpandableListView searchLvContent;
	// 所有数据 分组的数据
	private ArrayList<GroupModelBean> groupDataList;
	private OnCityChoiceListener onChoice;
	private Context mExtraContext;
	private Request<String> mCityRequest;
	public boolean hasSameCity;
	private City allCity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View root = inflater.inflate(R.layout.fragment_city_list, container, false);

		groupDataList = new ArrayList<GroupModelBean>();
		cityDatas = new ArrayList<City>();
		searchLvContent = (ExpandableListView) root
				.findViewById(R.id.search_lv_content);
		searchAdapter = new SearchModelAdapter(inflater);
		searchLvContent.setAdapter(searchAdapter);
		bar = (SideBar) root.findViewById(R.id.sideBar);
		bar.setListView(searchLvContent, searchAdapter);
		
		if(hasSameCity){
			bar.init(new char[] { '全','同','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
					'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
					'X', 'Y', 'Z' });
		} else {
			bar.init(new char[] { '全', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
					'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
					'X', 'Y', 'Z' });
		}
		container.post(new Runnable() {
			
			@Override
			public void run() {
				getCity();
			}
		});
		searchLvContent.setOnChildClickListener(this);
		return root;
	}
	
	public void setExtraContext(Context cxt) {
		this.mExtraContext = cxt;
	}

	public void setOnCityChoiceListener(OnCityChoiceListener l) {
		onChoice = l;
	}
	
	class GroupHolder {
		TextView textView;
	}

	class ChildHolder {
		TextView textName;
	}

	/**
	 * parseDataMap(将查询到集合转换成 一个分组 结婚)
	 * 
	 * @param allDataList
	 *            得到的 所有还未分组的数据 void
	 * @exception
	 * @since 1.0.0
	 */
	private void parseDataMap(List<City> allDataList) {
		groupDataList.clear();
		String[] alphatable = { "A", "B", "C", "D", "E", "F", "G", "H", "I",
				"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
				"V", "W", "X", "Y", "Z" };

		ArrayList<GroupModelBean> tempDataList = new ArrayList<GroupModelBean>();

		for (String str : alphatable) {
			GroupModelBean groupBean = new GroupModelBean();
			groupBean.setGroup(str);
			groupBean.setGroupList(new ArrayList<City>());
			tempDataList.add(groupBean);
		}

		for (GroupModelBean groupBean : tempDataList) {
			String str = groupBean.getGroup();
			if (allDataList.size() == 0)
				return;
			for (City bean : allDataList) {
				if (bean.uname.subSequence(0, 1).toString().toUpperCase()
						.equalsIgnoreCase(str)) {
					groupBean.getGroupList().add(bean);
				}
			}
		}
		for (GroupModelBean groupBean : tempDataList) {
			if (groupBean.getGroupList().size() > 0) {
				groupDataList.add(groupBean);
			}
		}
		
		GroupModelBean allCityGroup = new GroupModelBean();
		allCityGroup.group = "全国";
		allCityGroup.groupList = new ArrayList<City>();
		allCityGroup.groupList.add(allCity);
		groupDataList.add(0, allCityGroup);
		
		if(hasSameCity) {
			GroupModelBean sameCity = new GroupModelBean();
			sameCity.group = "同城";
			sameCity.groupList = new ArrayList<City>();
			City city = new City();
			city.id = "-1";
			city.name = "同城";
			city.uname = "tongcheng";
			sameCity.groupList.add(city);
			groupDataList.add(1, sameCity);
		}
	}

	private static char[] copyArray(char original[]) {
		int length = original.length;
		char newArray[] = new char[length + 1];
		System.arraycopy(original, 0, newArray, 0, length);
		return newArray;
	}

	private class GroupModelBean {
		private String group;
		private ArrayList<City> groupList;

		public String getGroup() {
			return group;
		}

		public void setGroup(String group) {
			this.group = group;
		}

		public ArrayList<City> getGroupList() {
			return groupList;
		}

		public void setGroupList(ArrayList<City> groupList) {
			this.groupList = groupList;
		}

	}

	public void getCity() {
		if (null == searchLvContent)
			return;
		searchLvContent.setOnGroupClickListener(new OnGroupClickListener() {
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				return true;
			}
		});
		
		showLoadingDialog();
		
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_CITY;
		mCityRequest = new CityRequest(Method.POST, url, this, this);
		getRequestQueue().add(mCityRequest);
	}
	
	public void cancelRequest() {
		if(mCityRequest != null) {
			mCityRequest.cancel();
		}
	}
	
	public static class CityRequest extends StringRequest implements WillCancelDelegate {

		public CityRequest(int method, String url, Listener<String> listener,
				ErrorListener errorListener) {
			super(method, url, listener, errorListener);
		}
		
		@Override
		protected Map<String, String> getParams() throws AuthFailureError {
			Map<String, String> params = new HashMap<String, String>();
			params.put("city_id", "");
			return params;
		}

		@Override
		public boolean willCancelWhenOnDestroy() {
			return true;
		}

		@Override
		protected Response<String> parseNetworkResponse(NetworkResponse response) {
			Map<String, String> headers = response.headers;
			headers.put("Cache-Control", "max-age=" + Constants.MAX_AGE_CITY);
			return super.parseNetworkResponse(response);
		}
	}

	/**
	 * 按照指定属性，对ArrayList集合进行排序。
	 * 
	 * @param list
	 *            bean集合
	 */
	public static void sort(List<City> list) {
		if (list == null || list.isEmpty())
			return;
		Collections.sort(list, new Comparator<City>() {
			public int compare(City lhs, City rhs) {
				return lhs.uname.compareTo(rhs.uname);
			}
		});
	}


	class SearchModelAdapter extends BaseExpandableListAdapter implements
			SectionIndexer {

		private LayoutInflater inflater;

		public SearchModelAdapter(LayoutInflater inflater) {
			this.inflater = inflater;
		}

		public Object getChild(int groupPosition, int childPosition) {
			return groupDataList.get(groupPosition).getGroupList()
					.get(childPosition);
		}

		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		public View getChildView(final int groupPosition,
				final int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			ChildHolder childHolder = null;
			if (convertView == null) {
				childHolder = new ChildHolder();
				convertView = inflater.inflate(R.layout.item_list_city_child,
						null);
				childHolder.textName = (TextView) convertView
						.findViewById(R.id.item);
				convertView.setTag(childHolder);
			} else {
				childHolder = (ChildHolder) convertView.getTag();
			}
			childHolder.textName.setText(groupDataList.get(groupPosition)
					.getGroupList().get(childPosition).name);
			return convertView;
		}

		public int getChildrenCount(int groupPosition) {
			return groupDataList.get(groupPosition).getGroupList().size();
		}

		public Object getGroup(int groupPosition) {
			return groupDataList.get(groupPosition);
		}

		public int getGroupCount() {
			return groupDataList.size();
		}

		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			GroupHolder groupHolder = null;
			if (convertView == null) {
				groupHolder = new GroupHolder();
				convertView = inflater.inflate(R.layout.item_list_city_group,
						null);
				groupHolder.textView = (TextView) convertView
						.findViewById(R.id.group);
				convertView.setTag(groupHolder);
			} else {
				groupHolder = (GroupHolder) convertView.getTag();
			}
			groupHolder.textView.setText(groupDataList.get(groupPosition)
					.getGroup());
			return convertView;
		}

		public boolean hasStableIds() {
			return true;
		}

		public Object[] getSections() {
			return null;
		}

		public int getPositionForSection(int section) {
			if (section == 35) {
				return 0;
			}

			for (int i = 0; i < groupDataList.size(); i++) {
				String l = groupDataList.get(i).getGroup();
				char firstChar = l.toUpperCase().charAt(0);
				if (firstChar == section) {
					return i;
				}
			}
			return -1;
		}

		public int getSectionForPosition(int position) {
			return 0;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		if(onChoice != null) {
			SearchModelAdapter adapter = (SearchModelAdapter) parent.getExpandableListAdapter();
			City bean = (City) adapter.getChild(groupPosition, childPosition);
			onChoice.onChoice(bean);
			return true;
		}
		return false;
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		dismissLoadingDialog();
		ToastHelper.showShortToast("网络异常");
	}

	@Override
	public void onResponse(String arg0) {
		dismissLoadingDialog();
		D.debug("MARTIN", arg0);
		CityInfo info = null;
		try {
			info = new Gson().fromJson(arg0, CityInfo.class);
		} catch (JsonSyntaxException e) {
			return ;
		}
		
		if(Constants.SUCCESS.equals(info.op_code)) {
			cityDatas = info.cities;
			
			//第一个为全国，没有意义，删除之
			allCity = cityDatas.remove(0);
			sort(cityDatas);
			parseDataMap(cityDatas);
			searchAdapter.notifyDataSetChanged();
			for (int i = 0; i < groupDataList.size(); i++) {
				searchLvContent.expandGroup(i);
			}
		} else {
			ToastHelper.showShortToast(info.op_info);
		}
	
	}

}
