package com.marriage.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.AboutActivity;
import com.marriage.activity.FeedbackActivity;
import com.marriage.activity.MerchantJoinDesActivity;
import com.marriage.delegate.LoginDelegate;
import com.marriage.delegate.LoginDelegate.LoginDelegateListener;
import com.marriage.utility.D;
import com.marriage.utility.ToastHelper;

public class LoginFragment extends BaseFragment implements OnClickListener,
		LoginDelegateListener {

	private EditText emailInput, pwdInput;

	private LoginDelegate delegate;

	private String pwd;
	private String email;

	public static LoginFragment newInstance(LoginDelegate d) {
		LoginFragment instance = new LoginFragment();
		instance.delegate = d;
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		delegate.setLoginDelegateListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_login, container,
				false);

		initViews(mainView);

		return mainView;
	}

	private void initViews(View v) {
		v.findViewById(R.id.back_btn).setOnClickListener(this);
		v.findViewById(R.id.register).setOnClickListener(this);
		v.findViewById(R.id.login).setOnClickListener(this);
		v.findViewById(R.id.forget_pwd).setOnClickListener(this);
		v.findViewById(R.id.login_by_qq).setOnClickListener(this);
		v.findViewById(R.id.login_by_sina_weibo).setOnClickListener(this);
		v.findViewById(R.id.merchant_join_des).setOnClickListener(this);
		v.findViewById(R.id.cooperation_feedback).setOnClickListener(this);
		v.findViewById(R.id.about).setOnClickListener(this);
		emailInput = (EditText) v.findViewById(R.id.login_email_input);
		pwdInput = (EditText) v.findViewById(R.id.login_pwd_input);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			popBack();
			break;
		case R.id.register:
			goRegister();
			break;
		case R.id.login:
			goLogin();
			break;
		case R.id.forget_pwd:
			goForgetPwd();
			break;
		case R.id.login_by_qq:
			goLoginByQQ();
			break;
		case R.id.login_by_sina_weibo:
			goLoginBySinaWeibo();
			break;
		case R.id.merchant_join_des:
			goMerchantJoinDes();
			break;
		case R.id.cooperation_feedback:
			goCooperationFeedback();
			break;
		case R.id.about:
			goAbout();
			break;
		}
	}

	private void popBack() {
		getActivity().onBackPressed();
	}

	private void goRegister() {
		RegisterFragment rf = new RegisterFragment();
		FragmentTransaction ft = getActivity().getSupportFragmentManager()
				.beginTransaction();
		ft.addToBackStack(this.getClass().getSimpleName());
		ft.add(android.R.id.content, rf, RegisterFragment.class.getSimpleName())
				.commit();
	}

	private void goLogin() {
		if (checkEmainInput() && checkPwdInput()) {
			delegate.login(email, pwd);
		}
	}

	private void goLoginByQQ() {
		showLoadingDialog();
		delegate.loginByQQ(this);
	}

	private void goLoginBySinaWeibo() {
		showLoadingDialog();
		Activity a = getActivity();
		D.error("LOGIN", a==null?"getActivity is null":a.toString());
		delegate.loginBySinaWeibo(a);
	}

	private void goForgetPwd() {
		FindpwdFragment ff = new FindpwdFragment();
		FragmentTransaction ft = getActivity().getSupportFragmentManager()
				.beginTransaction();
		ft.addToBackStack(this.getClass().getSimpleName());
		ft.add(android.R.id.content, ff, FindpwdFragment.class.getSimpleName())
				.commit();
	}

	private void goMerchantJoinDes() {
		Intent goMerchantJoinDes = new Intent(MApp.get(),
				MerchantJoinDesActivity.class);
		startActivity(goMerchantJoinDes);
	}

	private void goCooperationFeedback() {
		Intent goFeedback = new Intent(MApp.get(), FeedbackActivity.class);
		startActivity(goFeedback);
	}

	private void goAbout() {
		Intent goAbout = new Intent(MApp.get(), AboutActivity.class);
		startActivity(goAbout);
	}

	private boolean checkEmainInput() {
		email = emailInput.getText().toString();
		if (TextUtils.isEmpty(email)) {
			ToastHelper.showShortToast(R.string.pls_input_your_email_first);
			return false;
		}
		/*
		 * if (!CommonUtils.isEmailAddress(email)){
		 * ToastHelper.showShortToast(R.string.pls_input_correct_email); return
		 * false; }
		 */
		return true;
	}

	private boolean checkPwdInput() {
		pwd = pwdInput.getText().toString().trim();
		if (TextUtils.isEmpty(pwd)) {
			ToastHelper.showShortToast(R.string.pls_input_pwd);
			return false;
		}
		return true;
	}

	@Override
	public void onLoginStart() {
		// TODO Auto-generated method stub
		showLoadingDialog();
	}

	@Override
	public void onLoginFailure(String msg) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		showAlert(msg);
	}

	@Override
	public void onLoginCancel() {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
	}

	@Override
	public void onLoginSuccess() {
		// TODO Auto-generated method stub
		popBack();
		ToastHelper.showShortToast(R.string.login_success);
	}

}
