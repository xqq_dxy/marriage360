package com.marriage.fragment;

import com.marriage.R;
import com.marriage.utility.Constants;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class WebFragment extends BaseFragment implements OnClickListener{

	private String titleName;
	private String sourceUrl;
	
	private TextView titleView;
	private WebView webView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle data = getArguments();
		if (data != null){
			titleName = data.getString(Constants.BUNDLE_WEBVIEW_TITLE);
			sourceUrl = data.getString(Constants.BUNDLE_WEBVIEW_URL);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_webview, container, false);
		initViews(mainView);
		return mainView;
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	private void initViews(View v){
		titleView = (TextView) v.findViewById(R.id.web_title_view);
		if (!TextUtils.isEmpty(titleName)){
			titleView.setText(titleName);
		}
		webView = (WebView) v.findViewById(R.id.web_view);
		
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient(){

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub
				super.onPageStarted(view, url, favicon);
				showLoadingDialog();
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				// TODO Auto-generated method stub
				super.onPageFinished(view, url);
				dismissLoadingDialog();
			}
			
		});
		if (!TextUtils.isEmpty(sourceUrl)){
			webView.loadUrl(sourceUrl);
		}
		v.findViewById(R.id.back_btn).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;

		default:
			break;
		}
	}
	
	@Override
	public boolean onBackPressed() {
		// TODO Auto-generated method stub
		if (webView.canGoBack()){
			webView.goBack();
			return true;
		}
		return super.onBackPressed();
	}

}
