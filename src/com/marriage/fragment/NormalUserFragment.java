package com.marriage.fragment;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.WeddingDetailActivity;
import com.marriage.bean.NormalUserInfoBean;
import com.marriage.bean.Topics;
import com.marriage.bean.TopicsInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImageUtil;
import com.marriage.widget.adapter.MerchantPublishListAdapter;
import com.marriage.widget.lib.pla.PLA_AbsListView;
import com.marriage.widget.lib.pla.PLA_AbsListView.OnScrollListener;
import com.marriage.widget.lib.pla.PLA_AdapterView;
import com.marriage.widget.lib.pla.PLA_AdapterView.OnItemClickListener;
import com.marriage.widget.lib.pla.XListView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class NormalUserFragment extends BaseFragment implements
		OnClickListener, Listener<String>, ErrorListener, OnScrollListener,
		OnItemClickListener {

	public static final String TAG = "NormalUserFragment";

	private ImageView userAvatar;
	private TextView userNameView, userCityView, userPublisNumber,
			userPriseNumberView, userFocusNumberView;
	private XListView mPublishList;

	private MerchantPublishListAdapter mAdapter;

	private int pageIndex = 1;
	private int pageSize = 10;
	private boolean canNext = true;
	private boolean isLoading = false;
	private String userId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mAdapter = new MerchantPublishListAdapter(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_normal_userinfo,
				container, false);
		initViews(mainView);
		getUserInfo();
		fillData();
		return mainView;
	}

	private void initViews(View v) {
		userAvatar = (ImageView) v.findViewById(R.id.userinfo_avatar);
		userNameView = (TextView) v.findViewById(R.id.userinfo_name);
		userCityView = (TextView) v.findViewById(R.id.userinfo_city);
		userPublisNumber = (TextView) v
				.findViewById(R.id.userinfo_publish_number);
		userPriseNumberView = (TextView) v
				.findViewById(R.id.userinfo_prise_number);
		userFocusNumberView = (TextView) v
				.findViewById(R.id.userinfo_focus_number);
		v.findViewById(R.id.back_btn).setOnClickListener(this);
		mPublishList = (XListView) v.findViewById(R.id.user_info_publish_list);
		mPublishList.setPullLoadEnable(false);
		mPublishList.setPullRefreshEnable(false);
		mPublishList.setOnScrollListener(this);
		mPublishList.setOnItemClickListener(this);
		mPublishList.setAdapter(mAdapter);
	}

	private void fillData() {
		Bundle data = getArguments();
		if (data == null) {
			showAlert(MApp.get().getString(R.string.net_err));
			return;
		}
		String avatarImg = data.getString(Constants.BUNDLE_USER_AVATAR);
		String name = data.getString(Constants.BUNDLE_USER_NAME);
		userId = data.getString(Constants.BUNDLE_USER_ID);
		String city = data.getString(Constants.BUNDLE_USER_CITY);
		ImageLoader.getInstance().displayImage(avatarImg, userAvatar,
				ImageUtil.getCircleDisplayImageOptions());
		userNameView.setText(name);
		userCityView.setText(city);

		if (mAdapter.getCount() == 0) {
			fetchData(true);
		}
	}

	private void getUserInfo() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_USER_INFO;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						D.error(TAG, "user ret: " + arg0);
						try {
							NormalUserInfoBean bean = new Gson().fromJson(arg0,
									NormalUserInfoBean.class);
							if (Constants.SUCCESS.equals(bean.op_code)) {
								userPriseNumberView
										.setText(bean.user.faved_count);
								userFocusNumberView
										.setText(bean.user.focus_count);
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub

					}
				}) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("user_id", userId);
				return params;
			}
		};
		executeRequest(sr);
	}

	private void fetchData(boolean isFirst) {
		isLoading = true;
		if (isFirst) {
			showLoadingDialog();
		}

		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPICS;
		StringRequest sr = new StringRequest(Method.POST, url, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("fun_page", "3");
				params.put("user_id", userId);
				params.put("page_index", String.valueOf(pageIndex));
				params.put("page_size", String.valueOf(pageSize));
				params.put("city_id", "1");
				params.put("cate_id", "");
				params.put("sort", "");
				return params;
			}

		};

		executeRequest(sr);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		isLoading = false;
		dismissLoadingDialog();
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		isLoading = false;
		dismissLoadingDialog();
		TopicsInfo tops = null;
		try {
			tops = new Gson().fromJson(arg0, TopicsInfo.class);
		} catch (Exception e) {
		}

		if (Constants.SUCCESS.equals(tops.op_code)) {
			userPublisNumber.setText(MApp.get().getString(
					R.string.merchant_publish_number,
					new Object[] { tops.topic_list_total }));
			if (tops.topic_list != null) {
				mAdapter.addDatas(tops.topic_list);
				if (tops.topic_list.size() == pageSize) {
					pageIndex++;
				} else {
					canNext = false;
				}
			}
		}
	}

	@Override
	public void onItemClick(PLA_AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Topics data = (Topics) parent.getAdapter().getItem(position);
		Intent intent = new Intent(getActivity(), WeddingDetailActivity.class);
		intent.putExtra("topic_id", data.topic_id);
		startActivity(intent);
	}

	@Override
	public void onScrollStateChanged(PLA_AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(PLA_AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		if (!isLoading && canNext && totalItemCount > visibleItemCount
				&& totalItemCount - (firstVisibleItem + visibleItemCount) < 3) {
			fetchData(false);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;

		default:
			break;
		}
	}
}
