package com.marriage.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.MApp.LoginChangedListener;
import com.marriage.R;
import com.marriage.activity.LoginActivity;
import com.marriage.activity.WeddingDetailActivity;
import com.marriage.activity.WeddingPublishActivity;
import com.marriage.bean.AdvInfo;
import com.marriage.bean.AdvList;
import com.marriage.bean.City;
import com.marriage.bean.TopicCategory;
import com.marriage.bean.TopicCategoryInfo;
import com.marriage.bean.TopicTagCat;
import com.marriage.bean.TopicTagCatInfo;
import com.marriage.bean.Topics;
import com.marriage.bean.UserInfo;
import com.marriage.dialog.WeddingShareAddDialog;
import com.marriage.fragment.CityFragment.CityRequest;
import com.marriage.network.WillCancelDelegate;
import com.marriage.utility.AppConfig;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImagePicker;
import com.marriage.utility.ImagePicker.RequestParams;
import com.marriage.utility.ImagePicker.StartActivityForResultCompt;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.PreferenceHelper;
import com.marriage.utility.ToastHelper;
import com.marriage.utility.TopicsRequestWrapper;
import com.marriage.utility.TopicsRequestWrapper.TopicReponse;
import com.marriage.widget.ScaleImageView;
import com.marriage.widget.TagLayout2;
import com.marriage.widget.adapter.CommonBaseAdapter;
import com.marriage.widget.lib.pla.PLA_AbsListView;
import com.marriage.widget.lib.pla.PLA_AbsListView.OnScrollListener;
import com.marriage.widget.lib.pla.PLA_AdapterView;
import com.marriage.widget.lib.pla.PLA_AdapterView.OnItemClickListener;
import com.marriage.widget.lib.pla.XListView;
import com.marriage.widget.lib.vpi.CirclePageIndicator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;

public class WeddingShareFragment extends BaseFragment implements
		View.OnClickListener, OnItemClickListener, StartActivityForResultCompt,
		TagLayout2.OnCityChoiceListener, TopicReponse {

	public static final String KEY_RESULT_REQUEST = "key_result_request";
	
	public static String cityId;
	public static String cityName;
	private String categoryId, categoryTagId;
	private String categoryName, categoryTagName;
	private String sortId;
	private String sortName;

	private ViewPager mPager;
	private CirclePageIndicator mPagerIndicator;
	private XListView mListView;
	private MyAdapter mPagerAdapter;
	private PLAAdapter mListAdapter;
	private TagLayout2 mTagLayout;
	private ListView mCategoryList, mCategoryListChild;
	private WeddingShareAddDialog mDialog;
	private final static int CODE_REQUEST_TAKE_PHOTO = 0x07171;
	private final static int CODE_REQUEST_PICKER_IMAGE = 0x07172;
	private final static int CODE_REQUEST_DETAIL_PUBLISH = 0x07173;
	private final static int CODE_REQUEST_TO_PUBLISH = 0x07174;
	private RequestParams mRequest;
	private List<AdvList> mAdvPics;
	private List<TopicCategory> mTopCategory = new ArrayList<TopicCategory>();
	private CategoryAdapter mTopCategoryAdapter;
	private CategoryChildAdapter mTopCategoryChildAdapter;
	private TopicsRequestWrapper mTopicRequest;
	static List<String> sortData;
	private SparseArray<List<TopicTagCat>> mTagData = new SparseArray<List<TopicTagCat>>();
	private int mList1ChoicedPosition = -1, mList2ChoicedPosition = -1;
	private boolean mFollow;
	private City mDefaultCity;
	private View publishBtn;
	
	static {
		sortData = new ArrayList<String>();
		sortData.add("全部商家");
		sortData.add("关注的商家");
//		sortData.add("点击量");
//		sortData.add("发布时间");
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		D.error("MARTIN", "WeddingShareFragment.... onCreate savedInstanceState " + savedInstanceState);
		if(savedInstanceState != null) {
			mRequest = savedInstanceState.getParcelable(KEY_RESULT_REQUEST);
			if(mRequest != null) {
				mRequest.fillContext(getActivity());
			}
		}
		MApp.get().setLoginChanged(new LoginChangedListener() {
			
			@Override
			public void onLoginChanged(boolean isLogin) {
				if (MApp.get().isLogin()) {
					if (isMerchantUser()){
						publishBtn.setVisibility(View.VISIBLE);
					}
				} else {
					publishBtn.setVisibility(View.GONE);
				}
				
				if(mTopicRequest != null) {
					mTopicRequest.clear();
				}
			}
		});
		
		mDefaultCity = MApp.get().getCityFromServer(new Listener<City>() {

			@Override
			public void onResponse(City response) {
				mDefaultCity = response;
				ToastHelper.showShortToast("当前城市 "+mDefaultCity.city_name);
				onCityChoice(0, mDefaultCity);
			}
		});
		if(mDefaultCity != null) {
			ToastHelper.showShortToast("当前城市 "+mDefaultCity.city_name);
		}
		initData();
	}

	private void initData() {
		if(mDefaultCity != null) {
			cityId = mDefaultCity.city_id;
			cityName = mDefaultCity.city_name;
		} else {
			cityId = PreferenceHelper.getString(
					PreferenceHelper.PreferenceKeys.CHOICED_CITY_ID, "1");
			cityName = PreferenceHelper.getString(
					PreferenceHelper.PreferenceKeys.CHOICED_CITY_NAME, "城市");
		}
		categoryId = PreferenceHelper.getString(
				PreferenceHelper.PreferenceKeys.CHOICED_CATEGORY_ID, "");
		categoryName = PreferenceHelper.getString(
				PreferenceHelper.PreferenceKeys.CHOICED_CATEGORY_NAME, "婚礼图片");
		categoryTagId = categoryTagName = "";
		sortId = PreferenceHelper.getString(
				PreferenceHelper.PreferenceKeys.CHOICED_SORT_ID, "");
		sortName = PreferenceHelper.getString(
				PreferenceHelper.PreferenceKeys.CHOICED_SORT_NAME, "全部商家");
		mTopicRequest = new TopicsRequestWrapper(getRequestQueue(), this,
				cityId, categoryId, categoryTagName, sortId);

		// 城市数据
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_CITY;
		getRequestQueue().add(
				new CityRequest(Method.POST, url, new Listener<String>() {
					@Override
					public void onResponse(String reponse) {
						D.debug("城市", reponse);
					}
				}, null));

		// 轮播图
		url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_ADVLS;
		StringRequest request = new AdvPicRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String reponse) {
						D.debug("轮播图", reponse);
						AdvInfo info = null;
						try {
							info = new Gson().fromJson(reponse, AdvInfo.class);
						} catch (JsonSyntaxException e) {
							return;
						}

						if (Constants.SUCCESS.equals(info.op_code)) {
							mAdvPics = info.adv_list;
							if (mPagerAdapter != null) {
								mPagerAdapter.swapData(mAdvPics);
							}
						}
					}

				}, null);
		getRequestQueue().add(request);

		// 分类数据
		url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPIC_CATEGORY;
		request = new CategoryRequest(Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String reponse) {
				D.debug("分类", reponse);
				TopicCategoryInfo info = null;
				try {
					info = new Gson()
							.fromJson(reponse, TopicCategoryInfo.class);
				} catch (JsonSyntaxException e) {
					return;
				}
				mTopCategory = info.topic_cate_tag_ls;
				if (Constants.SUCCESS.equals(info.op_code)) {
					if (mTopCategoryAdapter != null) {
						if(mTopCategory != null && mTopCategory.size() > 0) {
							TopicCategory category = new TopicCategory();
							category.tag_cate_id = "-1";
							category.tag_cate_name = "全部";
							mTopCategory.add(0, category);
						}
						mTopCategoryAdapter.setDatas(mTopCategory);
					}
				}
			}
		}, null);
		getRequestQueue().add(request);

	}

	AdapterView.OnItemClickListener mCategoryListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
			if(parent.getId() == R.id.merchant_category_list) {	//大类别
				if(position == 0) {
					categoryId = categoryTagName = "";
					categoryName = "全部";
					mTagLayout.setTagName(1, categoryName);

					if(mTopicRequest.onCategoryChanged(categoryId, categoryTagName)) {
						mListView.setSelectionFromTop(0, 0);
						showLoadingDialog();
					}
					mCategoryList.clearChoices();
					mTopCategoryChildAdapter.clear();
					mList1ChoicedPosition = -1;
					mTagLayout.dismissPopWindow();
					return;
				}
				
				if(mList1ChoicedPosition == position) {
					categoryId = mTopCategory.get(mList1ChoicedPosition).tag_cate_id;
					categoryTagName = "";
					categoryName = mTopCategory.get(mList1ChoicedPosition).tag_cate_name;
					mTagLayout.setTagName(1, categoryName);

					if(mTopicRequest.onCategoryChanged(categoryId, categoryTagName)) {
						mListView.setSelectionFromTop(0, 0);
						showLoadingDialog();
					}
					mTagLayout.dismissPopWindow();
					return;
				}
				
				mList1ChoicedPosition = position;
				mCategoryList.setItemChecked(position, true);
				showList2(position);
			} else if(parent.getId() == R.id.merchant_catergory_child_list) {	//小类别
				mList2ChoicedPosition = position;
				mTagLayout.dismissPopWindow();
				TopicCategory category = mTopCategory.get(mList1ChoicedPosition);
				categoryId = category.tag_cate_id;
				categoryName = category.tag_cate_name;
				
				List<TopicTagCat> tagCategory = mTagData.get(mList1ChoicedPosition);
				if(!tagCategory.isEmpty()) {
					categoryTagId = tagCategory.get(mList2ChoicedPosition).tag_tag_id;
					categoryTagName = tagCategory.get(mList2ChoicedPosition).tag_tag_name;
				}
				String showTag = categoryName;
				if(!TextUtils.isEmpty(categoryTagName)) {
					showTag = categoryTagName;
				}
				
				mTagLayout.setTagName(1, showTag);

				if(mTopicRequest.onCategoryChanged(categoryId, categoryTagName)) {
					mListView.setSelectionFromTop(0, 0);
					showLoadingDialog();
				}

//				PreferenceHelper
//						.setString(
//								PreferenceHelper.PreferenceKeys.CHOICED_CATEGORY_ID,
//								categoryId);
//				PreferenceHelper
//						.setString(
//								PreferenceHelper.PreferenceKeys.CHOICED_CATEGORY_NAME,
//								categoryName);
			}
		}
	};
	
	private void showList2(final int position) {
		List<TopicTagCat> tag = mTagData.get(position);
		
		if(tag == null) {
			String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPIC_TAG_CATEGORY;;
			StringRequest request = new StringRequest(Method.POST, url, new TagListener(position), null) {

				@Override
				protected Map<String, String> getParams()
						throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("topic_cate_id", mTopCategory.get(position).tag_cate_id);
					return params;
				}
			};
			BaseFragment.getRequestQueue().add(request);
		} else {
			mTopCategoryChildAdapter.setDatas(tag);
		}
	}
	
	class TagListener implements Listener<String> {

		int cPosition;
		
		public TagListener(int position) {
			cPosition = position;
		}
		
		@Override
		public void onResponse(String response) {

			D.debug("分类", response);
			TopicTagCatInfo info;
			try {
				info = new Gson()
					.fromJson(response, TopicTagCatInfo.class);
			} catch (JsonSyntaxException e) {
				return;
			}
			if (Constants.SUCCESS.equals(info.op_code)) {
				if(info.topic_tag_ls == null) {
					info.topic_tag_ls = new ArrayList<TopicTagCat>();
				}
				
				mTagData.put(cPosition, info.topic_tag_ls);
				if(mList1ChoicedPosition == cPosition) {
					mTopCategoryChildAdapter.setDatas(info.topic_tag_ls);
				}
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_share_wedding_layout,
				container, false);
		publishBtn = view.findViewById(R.id.right_btn);
		publishBtn.setOnClickListener(this);
		
		
		if (MApp.get().isLogin() && isMerchantUser()) {
			publishBtn.setVisibility(View.VISIBLE);
		} else {
			publishBtn.setVisibility(View.GONE);
		}
		
		
		mTagLayout = (TagLayout2) view.findViewById(R.id.tag_layout);

		View categoryView = inflater.inflate(
					R.layout.filter_pop_category2, null);
		
		mCategoryList = (ListView) categoryView.findViewById(R.id.merchant_category_list);
		mTopCategoryAdapter = new CategoryAdapter(getActivity(), mTopCategory);
		mCategoryList.setAdapter(mTopCategoryAdapter);
		mCategoryList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		mCategoryList.setOnItemClickListener(mCategoryListener);
		
		mCategoryListChild = (ListView) categoryView.findViewById(R.id.merchant_catergory_child_list);
		mTopCategoryChildAdapter = new CategoryChildAdapter();
		mCategoryListChild.setAdapter(mTopCategoryChildAdapter);
		mCategoryListChild.setOnItemClickListener(mCategoryListener);
		
		View ciy = inflater.inflate(R.layout.filter_pop_city2, null);
		
		ListView sortList = (ListView) inflater.inflate(
				R.layout.filter_pop_sort, null);
		sortList.setBackgroundColor(Color.WHITE);
		sortList.setAdapter(new SortAdapter(getActivity(), sortData));
		mTagLayout.addTag(0, ciy, cityName);
		mTagLayout.addTag(1, categoryView, categoryName);
		mTagLayout.addTag(2, sortList, sortName);
		mTagLayout.setOnCityChoiceListener(this);
		
		sortList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				mTagLayout.dismissPopWindow();

				if(position == 1) {
					if(!MApp.get().isLogin()) {
						Intent intent = new Intent(getActivity(), LoginActivity.class);
						startActivity(intent);
						return;
					}
				}
				
				if(position == 0 ){
					sortId = "";
					sortName = sortData.get(position);
					mFollow = false;
				} else if(position == 1) {
					sortId = "1";
					sortName = sortData.get(position);
					mFollow = true;
				}
				
				mTagLayout.setTagName(2, sortName);
				
				if (mTopicRequest.onSortChanged(sortId)) {
					mListView.setSelectionFromTop(0, 0);
					showLoadingDialog();
				}

//				PreferenceHelper.setString(
//						PreferenceHelper.PreferenceKeys.CHOICED_SORT_ID,
//						String.valueOf(position + 1));
//				PreferenceHelper.setString(
//						PreferenceHelper.PreferenceKeys.CHOICED_SORT_NAME,
//						sortData.get(position));
			}
		});
		
		
//		mCategoryList
//				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//					@Override
//					public void onItemClick(AdapterView<?> parent, View view,
//							int position, long id) {
//						mTagLayout.dismissPopWindow();
//						TopicCategory category = mTopCategory.get(position);
//						categoryId = category.tag_cate_id;
//						categoryName = category.tag_cate_name;
//
//						mTagLayout.setTagName(0, categoryName);
//
//						if(mTopicRequest.onCategoryChanged(categoryId)) {
//							showLoadingDialog();
//						}
//
//						PreferenceHelper
//								.setString(
//										PreferenceHelper.PreferenceKeys.CHOICED_CATEGORY_ID,
//										categoryId);
//						PreferenceHelper
//								.setString(
//										PreferenceHelper.PreferenceKeys.CHOICED_CATEGORY_NAME,
//										categoryName);
//					}
//				});

		mListView = (XListView) view.findViewById(R.id.list);
		mListView.setPullLoadEnable(false);
		mListView.setPullRefreshEnable(false);
		mListView.addHeaderView(newHeaderView());
		mListView.setAdapter(mListAdapter = new PLAAdapter(mTopicRequest
				.getTopicsList()));
		mListView.setOnItemClickListener(this);
		mListView.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(PLA_AbsListView view,
					int scrollState) {
				switch (scrollState) {
				case OnScrollListener.SCROLL_STATE_IDLE:
					if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
						if(mTopicRequest.addQueue()) {
							D.error("MARTIN", ">>>>>>>>>>>>加载更多<<<<<<<<<<<");
							showLoadingDialog();
						}
					}
					break;
				}
			}
			
			@Override
			public void onScroll(PLA_AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {}
		});
		return view;
	}

	private View newHeaderView() {
		final float density = getActivity().getResources().getDisplayMetrics().density;
		FrameLayout ll = new FrameLayout(getActivity());
		ll.setLayoutParams(new PLA_AbsListView.LayoutParams(
				LayoutParams.MATCH_PARENT, (int) (165 * density + .5f)));

		mPager = new ViewPager(getActivity());
		mPager.setAdapter(mPagerAdapter = new MyAdapter());
		mPagerAdapter.swapData(mAdvPics);
		mPagerIndicator = new CirclePageIndicator(getActivity());
		ll.addView(mPager, new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
				Gravity.BOTTOM);
		lp.bottomMargin = (int) (density * 9 + 0.5f);
		mPagerIndicator.setFillColor(Color.parseColor("#e366c3"));
		mPagerIndicator.setPageColor(Color.parseColor("#F9F9F9"));
		mPagerIndicator.setSnap(true);
		mPagerIndicator.setStrokeWidth(0);
		mPagerIndicator.setViewPager(mPager);
		ll.addView(mPagerIndicator, lp);

		return ll;
	}

	@Override
	public void onResume() {
		if(mTopicRequest.emptyAddQueue()) {
			showLoadingDialog();
			mListAdapter.notifyDataSetChanged();
		}
		super.onResume();
	}

	@Override
	public boolean onBackPressed() {
		return false;
	}

	class PLAAdapter extends BaseAdapter implements BitmapDisplayer {

		public PLAAdapter(List<Topics> data) {
			this.data = data;
		}

		DisplayImageOptions option = new DisplayImageOptions.Builder()
				.cacheInMemory(true).cacheOnDisk(true)/*.displayer(this).showImageOnLoading(R.drawable.default_iphone)*/
				.bitmapConfig(Config.RGB_565).build();

		List<Topics> data;

		public void setData(List<Topics> data) {
			this.data = data;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Topics getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.item_list_home_pinterest, parent, false);
				holder = new ViewHolder();
				holder.img = (ScaleImageView) convertView
						.findViewById(R.id.item_iv);
				holder.content = (TextView) convertView
						.findViewById(R.id.item_content);
				holder.iconNum = (TextView) convertView
						.findViewById(R.id.item_images_num);
				holder.likeNum = (TextView) convertView
						.findViewById(R.id.item_likes_num);
				holder.uName = (TextView) convertView
						.findViewById(R.id.item_name);
				holder.avatar = (ImageView) convertView.findViewById(R.id.user_avatar);
//				holder.vIcon = (ImageView) convertView.findViewById(R.id.is_v);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			Topics topics = getItem(position);
			holder.img.setImageBitmap(null);
			ImageLoader.getInstance().displayImage(topics.cover_img, holder.img,
					option);
			
			holder.img.setImageWidth(topics.cover_img_w);
			holder.img.setImageHeight(topics.cover_img_h);
			
			holder.content.setText(topics.topic_content);
			holder.likeNum.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_icon, 0, 0, 0);
//			if("0".equals(topics.fav_count)) {
//			} else {
//				holder.likeNum.setCompoundDrawablesWithIntrinsicBounds(R.drawable.liked_icon, 0, 0, 0);
//			}
			holder.likeNum.setText(topics.fav_count);
			holder.iconNum.setText(topics.images_count);
			
			if(!TextUtils.isEmpty(topics.sl_name)) {
				holder.uName.setText(topics.sl_name);
			} else {
				holder.uName.setText(topics.user_name);
			}
			
			ImageLoader.getInstance().displayImage(topics.user_head_img, holder.avatar,
					ImageUtil.getCircleDisplayImageOptions());
			
			/*if(topics.is_sa) {
				holder.vIcon.setImageResource(R.drawable.v_icon_small);
			} else {
				holder.vIcon.setImageResource(R.drawable.v_icon_grey_small);
			}*/
			
			return convertView;
		}

		class ViewHolder {
			ScaleImageView img;
			TextView content, iconNum, likeNum, uName;
			ImageView avatar/*, vIcon*/;
		}

		@Override
		public void display(Bitmap bitmap, ImageAware imageAware,
				LoadedFrom arg2) {
			// TODO Auto-generated method stub
//			if (!imageAware.isCollected()) {
//				ScaleImageView image = (ScaleImageView) imageAware
//						.getWrappedView();
//				image.setImageWidth(bitmap.getWidth());
//				image.setImageHeight(bitmap.getHeight());
//				image.setImageBitmap(bitmap);
//			}
		}

	}

	static class MyAdapter extends PagerAdapter {

		private List<AdvList> mData;

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		@Override
		public int getCount() {
			return mData != null ? mData.size() : 0;
		}

		public void swapData(List<AdvList> data) {
			mData = data;
			notifyDataSetChanged();
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ImageView iv = new ImageView(container.getContext());
//			iv.setBackgroundResource(R.drawable.loading_failure);
			iv.setScaleType(ScaleType.CENTER_CROP);
			ImageLoader.getInstance().displayImage(
					mData.get(position).image_path, iv);
			container.addView(iv);
			return iv;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.share_weixin: {
			mDialog.dismissAllowingStateLoss();
			mRequest = new ImagePicker(getActivity()).startActivityForResult(
					ImagePicker.FROM_CAMERA, CODE_REQUEST_TAKE_PHOTO, this);
			break;
		}
		case R.id.share_weixin_friends: {
			mDialog.dismissAllowingStateLoss();
			mRequest = new ImagePicker(getActivity()).startActivityForResult(
					ImagePicker.FROM_GALLERY, CODE_REQUEST_PICKER_IMAGE, this);
			break;
		}
		case R.id.share_cancle:
			mDialog.dismissAllowingStateLoss();
			break;
		case R.id.right_btn: {
			if (MApp.get().isLogin()) {
				if (isMerchantUser()){
					if (mDialog == null) {
						mDialog = new WeddingShareAddDialog(this);
					}
					mDialog.show(getFragmentManager(), "ADD_DIALOG");
				}else{
					ToastHelper.showShortToast(R.string.sorry_you_are_not_merchant);
				}
				
			} else {
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
			}
			break;
		}
		case R.id.item_pinterest_root: {
			Integer p = (Integer) v.getTag(R.id.item_pinterest_root);

			Log.e("Martin", p + "");

			break;
		}
		}

	}

	private boolean isMerchantUser(){
		UserInfo user = MApp.get().getUser();
		return "1".equals(user.user_type);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		D.error("MARTIN", "onActivityResult.....");
		
		if(CODE_REQUEST_DETAIL_PUBLISH == requestCode && resultCode == Activity.RESULT_OK) {
			String likeNum = data.getStringExtra("likeNum");
			if(likeNum != null) {
				int position = data.getIntExtra("position", -1);
				if(position != -1 && mTopicRequest != null && mTopicRequest.getTopicsList() != null && mTopicRequest.getTopicsList().size() > position) {
					mTopicRequest.getTopicsList().get(position).fav_count = likeNum;
					mListAdapter.notifyDataSetChanged();
				}
			}
			
		} else if(CODE_REQUEST_TO_PUBLISH == requestCode && resultCode == Activity.RESULT_OK) {
			boolean reload = data.getBooleanExtra("reload", false);
			if(reload) {
				mTopicRequest.clear();
			}
		} else if(CODE_REQUEST_TAKE_PHOTO == requestCode || requestCode == CODE_REQUEST_PICKER_IMAGE){
			/**
			 * 其他请求自行处理
			 */
			Uri uri = mRequest.parseResult(requestCode, resultCode, data);
			if (uri != null) {
				Intent intent = new Intent(getActivity(),
						WeddingPublishActivity.class);
				intent.putExtra(WeddingPublishActivity.EXTRA_INPUT_URI, uri);
				startActivityForResult(intent, CODE_REQUEST_TO_PUBLISH);
			}
		}
	}

	@Override
	public void onItemClick(PLA_AdapterView<?> parent, View view, int position,
			long id) {

		Intent intent = new Intent(getActivity(), WeddingDetailActivity.class);
		intent.putExtra("topic_id", mTopicRequest.getTopicsList().get(position-2).topic_id);	//这里好坑啊，position 尽然是从2开始的，可能是设置Heade view的原因，减2就是正确了
		intent.putExtra("sl_id", mTopicRequest.getTopicsList().get(position-2).sl_id);
		intent.putExtra("sl_name", mTopicRequest.getTopicsList().get(position-2).sl_name);
		intent.putExtra("position", position-2);
		
		startActivityForResult(intent, CODE_REQUEST_DETAIL_PUBLISH);

	}

	@Override
	public void startActivityForResultCompt(Intent intent, int requestCode) {
		startActivityForResult(intent, requestCode);
	}

	@Override
	public void onCityChoice(int type, City data) {
//		if (type == 0) { // 全国
//			cityId = "1";
//			cityName = "全国";
//			mTagLayout.setTagName(1, cityName);
//			if (mTopicRequest.onCityChanged(cityId)) {
//				showLoadingDialog();
//			}
//		} else if (type == 1) { // 同城
//			cityId = "-1";
//			cityName = "同城";
//			mTagLayout.setTagName(1, cityName);
//			if (mTopicRequest.onCityChanged(cityId)) {
//				showLoadingDialog();
//			}
//		} else { // 城市
//			cityId = data.id;
//			cityName = data.name;
//			mTagLayout.setTagName(1, cityName);
//			if (mTopicRequest.onCityChanged(cityId)) {
//				showLoadingDialog();
//			}
//		}
		
		cityId = data.id;
		cityName = data.name;
		mTagLayout.setTagName(0, cityName);
		if (mTopicRequest.onCityChanged(cityId)) {
			showLoadingDialog();
			mListView.setSelectionFromTop(0, 0);
		}
		
		
//		PreferenceHelper.setString(
//				PreferenceHelper.PreferenceKeys.CHOICED_CITY_ID, cityId);
//		PreferenceHelper.setString(
//				PreferenceHelper.PreferenceKeys.CHOICED_CITY_NAME, cityName);
	}

	public static class CategoryRequest extends StringRequest implements
			WillCancelDelegate {

		public CategoryRequest(int method, String url,
				Listener<String> listener, ErrorListener errorListener) {
			super(method, url, listener, errorListener);
			// TODO Auto-generated constructor stub
		}

		@Override
		public boolean willCancelWhenOnDestroy() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		protected Response<String> parseNetworkResponse(NetworkResponse response) {
			Map<String, String> headers = response.headers;
			headers.put("Cache-Control", "max-age="
					+ Constants.MAX_AGE_TOPIC_CATEGORY);
			return super.parseNetworkResponse(response);
		}
	}

	static class AdvPicRequest extends StringRequest implements
			WillCancelDelegate {

		public AdvPicRequest(int method, String url, Listener<String> listener,
				ErrorListener errorListener) {
			super(method, url, listener, errorListener);
			// TODO Auto-generated constructor stub
		}

		@Override
		public boolean willCancelWhenOnDestroy() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		protected Response<String> parseNetworkResponse(NetworkResponse response) {
			return super.parseNetworkResponse(response);
		}
	}

	static class CategoryAdapter extends CommonBaseAdapter<TopicCategory> {

		public CategoryAdapter(Context context, List<TopicCategory> _datas) {
			super(context, _datas);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			CheckedTextView v = (CheckedTextView) convertView;
			if (v == null) {
				v = new CheckedTextView(context);
				v.setPadding(CommonUtils.dip2Px(context, 15),
						CommonUtils.dip2Px(context, 10),
						CommonUtils.dip2Px(context, 5),
						CommonUtils.dip2Px(context, 10));
				v.setTextSize(15);
				v.setTextColor(context.getResources().getColor(
						R.color.edit_text_color));
				v.setBackgroundResource(R.drawable.selector_filter_list_bg);
			}
			v.setText(getItem(position).toString());
			return v;
		}

	}
	
	static class CategoryChildAdapter extends BaseAdapter {
		
		private List<TopicTagCat> data;
		
		@Override
		public int getCount() {
			return data != null ? data.size() : 0;
		}

		public void setDatas(List<TopicTagCat> arrayList) {
			data = arrayList;
			notifyDataSetChanged();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
		
		public void clear() {
			data = new ArrayList<TopicTagCat>();
			notifyDataSetChanged();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			CheckedTextView v = (CheckedTextView) convertView;
			if (v == null) {
				v = new CheckedTextView(parent.getContext());
				v.setPadding(CommonUtils.dip2Px(parent.getContext(), 15),
						CommonUtils.dip2Px(parent.getContext(), 10),
						CommonUtils.dip2Px(parent.getContext(), 5),
						CommonUtils.dip2Px(parent.getContext(), 10));
				v.setTextSize(15);
				v.setTextColor(parent.getContext().getResources().getColor(
						R.color.edit_text_color));
			}
			v.setText(getItem(position).toString());
			return v;
		}
		
	}
	
	static class SortAdapter extends CommonBaseAdapter<String> {

		public SortAdapter(Context context, List<String> _datas) {
			super(context, _datas);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView v = (TextView) convertView;
			if (v == null) {
				v = new TextView(context);
				v.setPadding(CommonUtils.dip2Px(context, 15),
						CommonUtils.dip2Px(context, 10),
						CommonUtils.dip2Px(context, 5),
						CommonUtils.dip2Px(context, 10));
				v.setBackgroundColor(Color.WHITE);
				v.setTextSize(15);
				v.setTextColor(context.getResources().getColor(
						R.color.edit_text_color));
			}
			v.setText(getItem(position).toString());
			return v;
		}

	}

	@Override
	public void onReponse(List<Topics> topics) {
		dismissLoadingDialog();
		mListAdapter.setData(topics);
		if(topics.isEmpty()) {
			if(mFollow) {
				mFollow = false;
				ToastHelper.showShortToast("您还没有关注其他用户，快去关注吧！");
			} else {
				ToastHelper.showShortToast("没找到数据:(");
			}
		}
	}

	@Override
	public void onErrorReponse(VolleyError error) {
		dismissLoadingDialog();
		mListAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDestroy() {
		D.error("MARTIN", "WeddingShareFragment........destory");
		super.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		D.error("MARTIN", "WeddingShareFragment........onSaveInstanceState");
		outState.putParcelable(KEY_RESULT_REQUEST, mRequest);
		super.onSaveInstanceState(outState);
	}

}
