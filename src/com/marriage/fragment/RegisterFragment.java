package com.marriage.fragment;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.CityActivity;
import com.marriage.bean.UserInfo;
import com.marriage.delegate.RegisterDelegate;
import com.marriage.dialog.WeddingDialog;
import com.marriage.dialog.WeddingShareAddDialog;
import com.marriage.utility.AppConfig;
import com.marriage.utility.AvatarUtils;
import com.marriage.utility.BitmapFileUtil;
import com.marriage.utility.CircleBitmapMaker;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.FileUtil;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.NetWorkUtil;
import com.marriage.utility.ToastHelper;

public class RegisterFragment extends BaseFragment implements OnClickListener,
		Listener<String>, ErrorListener, OnDateSetListener {

	public static final String TAG = "RegisterFragment";

	private EditText emailInput, userNameInput, pwdInput, mobileInput;
	private ImageView avatar;
	private String userName, email, pwd, mobile, countDownTime, cityId,
			cityName, headPicStream;
	private TextView countDownDayView, cityView;

	private RegisterDelegate delegate;

	private WeddingShareAddDialog dialog;

	private static final int REQUEST_OPEN_CAMERA = 0x0001;
	private static final int REQUEST_OPEN_GALLERY = 0x0002;
	private static final int REQUEST_CITY = 0x0003;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		delegate = new RegisterDelegate();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_register, container,
				false);
		initViews(mainView);

		return mainView;
	}

	private void initViews(View v) {
		v.findViewById(R.id.register_submit).setOnClickListener(this);
		avatar = (ImageView) v.findViewById(R.id.personal_avatar);
		avatar.setOnClickListener(this);
		countDownDayView = (TextView) v
				.findViewById(R.id.register_weddingday_set);
		countDownDayView.setOnClickListener(this);
		cityView = (TextView) v.findViewById(R.id.register_city_input);
		cityView.setOnClickListener(this);

		userNameInput = (EditText) v.findViewById(R.id.register_user_input);
		emailInput = (EditText) v.findViewById(R.id.register_email_input);
		pwdInput = (EditText) v.findViewById(R.id.register_pwd_input);
		mobileInput = (EditText) v.findViewById(R.id.register_mobile_input);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.register_weddingday_set:
			goSetWeddingDay();
			break;

		case R.id.register_submit:
			goRegisterSubmit();
			break;
		case R.id.personal_avatar:
			goSetPersonalAvatar();
			break;
		case R.id.share_weixin:
			goPickPhotoByCamera();
			dialog.dismiss();
			break;
		case R.id.share_weixin_friends:
			goPickPhotoFromGallery();
			dialog.dismiss();
			break;
		case R.id.share_cancle:
			dialog.dismiss();
			break;
		case R.id.register_city_input:
			goChoseCity();
			break;
		}
	}

	private void goChoseCity() {
		Intent goCity = new Intent(MApp.get(), CityActivity.class);
		startActivityForResult(goCity, REQUEST_CITY);
	}

	private void goSetWeddingDay() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, year,
				month, day);
		dpd.show();
	}

	private void goRegisterSubmit() {
		if (checkUserNameInput() && checkMailInput() && checkPwdInput()
				&& checkCityInput()) {
			mobile = mobileInput.getText().toString().trim();
			doRegister();
		}
	}

	private void goSetPersonalAvatar() {
		if (dialog == null) {
			dialog = new WeddingShareAddDialog(this);
		}
		dialog.show(getFragmentManager(), "SET_AVATAR");

	}

	private void goPickPhotoFromGallery() {
		AvatarUtils.openGallery(this, REQUEST_OPEN_GALLERY);
	}

	private void goPickPhotoByCamera() {
		AvatarUtils.openCamera(this, REQUEST_OPEN_CAMERA);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_OPEN_GALLERY:
			onOpenGalleryBack(resultCode, data);
			break;

		case REQUEST_OPEN_CAMERA:
			onOpenCameraBack(resultCode, data);
			break;
		case REQUEST_CITY:
			if (resultCode == Activity.RESULT_OK) {
				onCityChosed(data);
			}
			break;
		}
	}

	private void onCityChosed(Intent data) {
		if (data == null) {
			return;
		}
		cityId = data.getStringExtra(CityActivity.KEY_CITY_ID);
		cityName = data.getStringExtra(CityActivity.KEY_CITY_NAME);
		if (TextUtils.isEmpty(cityId) || TextUtils.isEmpty(cityName)) {
			return;
		}
		cityView.setText(cityName);
	}

	private void onOpenGalleryBack(int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		if (data != null) {
			onPhotoTaken(data);
		}
	}

	private void onOpenCameraBack(int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		AvatarUtils.openGallery(this, REQUEST_OPEN_GALLERY, data == null ? null
				: data.getData());
	}

	private void onPhotoTaken(Intent data) {
		if (!NetWorkUtil.isNetworkAvailable(MApp.get())) {
			new WeddingDialog.Builder(getActivity())
					.setMessage(R.string.net_err)
					.setPositiveButton(R.string.confirm, null).show();
			return;
		}
		try {
			Bitmap bm = null;
			bm = data.getParcelableExtra("data");
			if (bm == null) {
				Uri uri = data.getData();
				DisplayMetrics dm = this.getResources().getDisplayMetrics();
				bm = ImageUtil.loadRotatedBitmapWithSizeLimitation(MApp.get(),
						dm.widthPixels * dm.heightPixels / 4, uri);
			}
			if (bm == null) {
				return;
			}
			avatar.setImageBitmap(CircleBitmapMaker.getCircleBitmap(MApp.get(),
					bm));
			uploadPersonPhoto(bm);

		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		}
	}

	private void doRegister() {
		showLoadingDialog();
		String reqUrl = AppConfig.INSTANCE.SERVER + Constants.URL_REGISTER;
		StringRequest sr = new StringRequest(Method.POST, reqUrl, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("user_name", userName);
				params.put("user_pwd", pwd);
				params.put("email", email);
				params.put("city_id", cityId);
				params.put("mobile", mobile == null ? "" : mobile);
				params.put("countdown", countDownTime == null ? ""
						: countDownTime);
				params.put("head_pic", headPicStream == null ? ""
						: headPicStream);
				params.put("head_pic_type", headPicStream == null ? "" : "jpg");
				return params;
			}

		};
		executeRequest(sr);
	}

	private void uploadPersonPhoto(Bitmap bm) {
		File bitmapFile = BitmapFileUtil.saveTempBmp(bm, MApp.get());
		String savedPath = BitmapFileUtil.compress(bitmapFile);
		headPicStream = FileUtil.fileToString(savedPath);
	}

	@Override
	public void onResponse(String result) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		if (result == null) {
			return;
		}
		try {
			UserInfo userInfo = new Gson().fromJson(result, UserInfo.class);
			if (userInfo == null) {
				Toast.makeText(MApp.get(), "出错啦~~~", Toast.LENGTH_SHORT).show();
				return;
			}
			if (Constants.SUCCESS.equals(userInfo.op_code)) {
				MApp.get().setUser(userInfo);
				ToastHelper.showShortToast(R.string.register_success);
				Activity a = getActivity();
				if (a != null) {
					a.finish();
				}
			} else {
				showAlert(userInfo.op_info);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		D.debug(TAG, "result: " + result);
		
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		showAlert(MApp.get().getString(R.string.net_err));
	}

	private boolean checkUserNameInput() {
		userName = userNameInput.getText().toString().trim();
		if (TextUtils.isEmpty(userName)) {
			ToastHelper.showShortToast(R.string.pls_input_your_user_name_first);
			return false;
		}
		return true;
	}

	private boolean checkMailInput() {
		email = emailInput.getText().toString().trim();
		if (TextUtils.isEmpty(email)) {
			ToastHelper.showShortToast(R.string.pls_input_your_email_first);
			return false;
		}
		if (!CommonUtils.isEmailAddress(email)) {
			ToastHelper.showShortToast(R.string.pls_input_correct_email);
			return false;
		}
		return true;
	}

	private boolean checkPwdInput() {
		pwd = pwdInput.getText().toString().trim();
		if (TextUtils.isEmpty(pwd)) {
			ToastHelper.showShortToast(R.string.pls_input_your_pwd);
			return false;
		}
		if (pwd.length() < 6) {
			ToastHelper.showShortToast(R.string.pls_input_correct_pwd);
			return false;
		}
		return true;
	}

	private boolean checkCityInput() {
		if (TextUtils.isEmpty(cityId)) {
			ToastHelper.showShortToast(R.string.pls_chose_city);
			return false;
		}
		return true;
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		countDownTime = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
		countDownDayView.setText(countDownTime);
	}

}
