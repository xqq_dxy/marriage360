package com.marriage.fragment;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.utility.AppConfig;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ToastHelper;

public class FeedbackFragment extends BaseFragment implements OnClickListener,
		Listener<String>, ErrorListener {

	private EditText contentInput, emailInput;

	private String content, email;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_feedback, container,
				false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(View v) {
		v.findViewById(R.id.back_btn).setOnClickListener(this);
		v.findViewById(R.id.feedback_submit).setOnClickListener(this);

		contentInput = (EditText) v.findViewById(R.id.feedback_content_input);
		emailInput = (EditText) v.findViewById(R.id.feedback_email_input);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;

		case R.id.feedback_submit:
			goSubmitFeedback();
			break;
		}
	}

	private void goSubmitFeedback() {
		if (checkContentInput() && checkEmailInput()) {
			doSubmitFeedback();
		}
	}

	private boolean checkContentInput() {
		content = contentInput.getText().toString();
		if (TextUtils.isEmpty(content)) {
			Toast.makeText(MApp.get(), R.string.fb_pls_input_feedback,
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	private boolean checkEmailInput() {
		email = emailInput.getText().toString();
		if (TextUtils.isEmpty(email)) {
			Toast.makeText(MApp.get(), R.string.fb_pls_input_email,
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!CommonUtils.isEmailAddress(email)){
			Toast.makeText(MApp.get(), R.string.fb_pls_input_correct_email,
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	private void doSubmitFeedback() {
		showLoadingDialog();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_FEED_BACK;
		StringRequest sr = new StringRequest(Method.POST, url, this, this){

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("user_id", MApp.get().isLogin()?MApp.get().getUser().user.user_id:"");
				params.put("content", content);
				params.put("email", email);
 				return params;
			}
			
		};
		executeRequest(sr);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		finish();
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		D.info("FeedbackFragment", "feedback response: "+arg0);
		dismissLoadingDialog();
		finish();
	}

	private void finish(){
		ToastHelper.showShortToast(R.string.thank_feedback);
		getActivity().onBackPressed();
	}
}
