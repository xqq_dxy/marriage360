package com.marriage.fragment;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.LoginActivity;
import com.marriage.activity.MineCommentActivity;
import com.marriage.activity.MineFansActivity;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.MerchantDetail;
import com.marriage.bean.MerchantDetailBean;
import com.marriage.bean.UserInfo;
import com.marriage.dialog.CommentDialog;
import com.marriage.dialog.CommentDialog.CommentSendExecutor;
import com.marriage.dialog.WeddingDialog;
import com.marriage.utility.AppConfig;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.ToastHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class MerchantDetailFragment extends BaseFragment implements
		OnClickListener, Listener<String>, ErrorListener, CommentSendExecutor {

	public static final String TAG = "MerchantDetailFragment";

	private ImageView mMerchantAvatar, mFocusTag, mMerchantDesImage;
	private TextView mMerchantName, mMerchantType, mFansNumber, mFocusState,
			mMerchantDesText, mMerchantPhone, mMerchantAddress;
	private View mGetThisMerchant,focusButton;
	private LinearLayout desLayout;
	private String merchantName;
	private String sa_loc_id;
	private MerchantDetail detailData;
	
	private int mMerchantAddressViewMaxLength;
	
	private String phoneNumbers[];

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle data = getArguments();
		if (data != null) {
			merchantName = data.getString(Constants.BUNDLE_MERCHANT_NAME);
			sa_loc_id = data.getString(Constants.BUNDLE_MERCHANT_LOC_ID);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_merchant_info,
				container, false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(View v) {
		mMerchantAvatar = (ImageView) v
				.findViewById(R.id.merchant_detail_avatar);
		mFocusTag = (ImageView) v.findViewById(R.id.merchant_detail_focus_tag);
		mMerchantDesImage = (ImageView) v
				.findViewById(R.id.merchant_detail_des_img);

		mMerchantName = (TextView) v.findViewById(R.id.merchant_detail_name);
		mMerchantType = (TextView) v.findViewById(R.id.merchant_detail_type);
		mFansNumber = (TextView) v.findViewById(R.id.merchant_detail_fans);
		mFansNumber.setOnClickListener(this);
		mFocusState = (TextView) v
				.findViewById(R.id.merchant_detail_focus_state);
		mMerchantDesText = (TextView) v
				.findViewById(R.id.merchant_detail_des_text);
		mMerchantPhone = (TextView) v
				.findViewById(R.id.merchant_detail_tel_number);
		mMerchantAddress = (TextView) v
				.findViewById(R.id.merchant_detail_adress);
		desLayout = (LinearLayout) v
				.findViewById(R.id.merchant_detail_des_layout);
		mGetThisMerchant = v.findViewById(R.id.get_this_merchant);

		v.findViewById(R.id.merchant_detail_go_comment)
				.setOnClickListener(this);
		focusButton = v.findViewById(R.id.merchant_detail_focous);
		focusButton.setOnClickListener(this);
		v.findViewById(R.id.merchant_detail_call).setOnClickListener(this);
		v.findViewById(R.id.back_btn).setOnClickListener(this);
		mGetThisMerchant.setOnClickListener(this);
		v.findViewById(R.id.merchant_detail_bottom_call).setOnClickListener(
				this);
		v.findViewById(R.id.merchant_detail_bottom_comment).setOnClickListener(
				this);
		v.findViewById(R.id.merchant_detail_bottom_publish).setOnClickListener(
				this);
		v.findViewById(R.id.merchant_detail_fans_name).setOnClickListener(this);
		if (isSelf()){
			focusButton.setVisibility(View.GONE);
		}
		mergeViews(v);
	}
	
	private boolean isSelf(){
		if (!MApp.get().isLogin()){
			return false;
		}
		if (MApp.get().getUser().supplier_account==null){
			return false;
		}
		try {
			if (sa_loc_id.equals(MApp.get().getUser().supplier_account.sa_loc_id)){
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return false;
	}
	
	private void mergeViews(View v){
		int screenWidth = MApp.get().getResources().getDisplayMetrics().widthPixels;
		TextView addressName = (TextView) v.findViewById(R.id.merchant_detail_address_name);
		float addressNameLength = addressName.getPaint().measureText(MApp.get().getString(R.string.merchant_detail_address_name));
		mMerchantAddressViewMaxLength = screenWidth - (int)addressNameLength - CommonUtils.dip2Px(MApp.get(), 5+35+10+13);
		mMerchantAddress.setMaxWidth(mMerchantAddressViewMaxLength);
		
	}

	/*private String mergeAddressText(String originalString){
		if (TextUtils.isEmpty(originalString)){
			return "";
		}
		String ret = originalString.replace(" ", "");
		Paint p = mMerchantAddress.getPaint();
		int length = (int) p.measureText(ret)+1;
		if (length < mMerchantAddressViewMaxLength){
			return ret;
		}
		ret.
	}*/
	
	private String mergePhoneText(String origPhone){
		if (TextUtils.isEmpty(origPhone)){
//			return origPhone;
		}
		boolean lastIsEmpty = false;
		char[] chars = origPhone.toCharArray();
		for(int i=0;i<chars.length;i++){
			if (chars[i] == ' '){
				if(lastIsEmpty){
					chars[i]='\0';
				}else{
					lastIsEmpty = true;
				}
			}else{
				lastIsEmpty = false;
			}
		}
		String merged = new String(chars);
		phoneNumbers = merged.split(" ");
		return merged.replace(' ', '\n');
		
	}
	
	private void bindData() {
		ImageLoader.getInstance().displayImage(detailData.img_small,
				mMerchantAvatar, ImageUtil.getCircleDisplayImageOptions());
		mMerchantName.setText(merchantName);
		mMerchantType.setText(detailData.sa_type_name);
		mFansNumber.setText(detailData.focused_count);
		mMerchantPhone.setText(mergePhoneText(detailData.tel));
		mMerchantAddress.setText(detailData.address);
		if (!TextUtils.isEmpty(detailData.focus_state)
				&& detailData.focus_state.toUpperCase(Locale.ENGLISH).equals(
						"TRUE")) {
			mFocusTag.setImageResource(R.drawable.details_added_follow);
			mFocusState.setText(R.string.merchant_detail_focused);
		} else {
			mFocusTag.setImageResource(R.drawable.details_add_follow);
			mFocusState.setText(R.string.merchant_detail_focus);
		}
		if (TextUtils.isEmpty(detailData.mobile_brief)
				|| "null".equals(detailData.mobile_brief)) {
			mMerchantDesText.setVisibility(View.GONE);
		} else {
			mMerchantDesText.setText(detailData.mobile_brief);
		}
		if (TextUtils.isEmpty(detailData.preview)
				|| "null".equals(detailData.preview)) {
			mMerchantDesImage.setVisibility(View.GONE);
		} else {
			ImageLoader.getInstance().displayImage(detailData.preview,
					mMerchantDesImage, new ImageLoadingListener() {

						@Override
						public void onLoadingStarted(String arg0, View arg1) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onLoadingFailed(String arg0, View arg1,
								FailReason arg2) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onLoadingComplete(String arg0, View arg1,
								Bitmap arg2) {
							// TODO Auto-generated method stub
							if (arg2 == null) {
								return;
							}
							int w = arg2.getWidth();
							int h = arg2.getHeight();
							int width = getResources().getDisplayMetrics().widthPixels
									- CommonUtils.dip2Px(getActivity(), 16 * 2);
							int height = width * h / w;
							mMerchantDesImage.getLayoutParams().height = height;
							mMerchantDesImage.invalidate();
						}

						@Override
						public void onLoadingCancelled(String arg0, View arg1) {
							// TODO Auto-generated method stub

						}
					});
		}
		if (mMerchantDesText.getVisibility() == View.GONE
				&& mMerchantDesImage.getVisibility() == View.GONE) {
			desLayout.setVisibility(View.GONE);
		}
		
		if (detailData.is_claim.equals("0")){
			mGetThisMerchant.setVisibility(View.VISIBLE);
		}else{
			mGetThisMerchant.setVisibility(View.GONE);
		}

	}

	private void fetchData() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_MERCHANT_DETAIL;
		StringRequest sr = new StringRequest(Method.POST, url, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				UserInfo userInfo = MApp.get().getUser();
				String userId = userInfo == null || userInfo.user == null ? ""
						: userInfo.user.user_id;
				params.put("user_id",userId);
				params.put("sl_id", sa_loc_id == null ? "" : sa_loc_id);
				
				D.info(TAG, "user_id: "+userId);
				D.info(TAG, "sl_id: "+sa_loc_id);
				
				return params;
			}

		};
		executeRequest(sr);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		fetchData();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;

		case R.id.merchant_detail_go_comment:
			goComment();
			break;
		case R.id.merchant_detail_focous:
			goFocus();
			break;
		case R.id.merchant_detail_call:
			goCall();
			break;
		case R.id.get_this_merchant:
			goGetThisMerchant();
			break;
		case R.id.merchant_detail_bottom_call:
			goCall();
			break;
		case R.id.merchant_detail_bottom_comment:
			goCommentList();
			break;
		case R.id.merchant_detail_bottom_publish:
			goPublishList();
			break;
		case R.id.merchant_detail_fans_name:
			goMerchantFans();
			break;
		case R.id.merchant_detail_fans:
			goMerchantFans();
			break;
		}
	}

	private void goMerchantFans(){
		if (detailData == null){
			return;
		}
		if ("0".equals(detailData.is_claim)){
			ToastHelper.showShortToast(R.string.not_claim_no_fans);
			return;
		}
		
		if (TextUtils.isEmpty(detailData.user_id)){
			ToastHelper.showShortToast(R.string.merchant_data_err_no_fans);
			return;
		}
		Intent goMerchantFans = new Intent(MApp.get(), MineFansActivity.class);
		goMerchantFans.putExtra(Constants.BUNDLE_FANS_FROM_MERCHANT, true);
		goMerchantFans.putExtra(Constants.BUNDLE_USER_ID, detailData.user_id);
		goMerchantFans.putExtra(Constants.BUNDLE_USER_NAME, merchantName);
		startActivity(goMerchantFans);
		
	}
	
	private void goComment() {
		if (!MApp.get().isLogin()) {
			goLogin();
			return;
		}
		CommentDialog commentDialog = new CommentDialog();
		commentDialog.setCommentSendExecutor(this);
		commentDialog.show(getFragmentManager(), TAG);
	}
	
	private void goCommentList() {
		Intent intent = new Intent(getActivity(), MineCommentActivity.class);
		intent.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, sa_loc_id);
		startActivity(intent);
	}

	private void goFocus() {
		if (detailData == null){
			return;
		}
		if (!TextUtils.isEmpty(detailData.focus_state)
				&& detailData.focus_state.toUpperCase(Locale.ENGLISH).equals(
						"TRUE")) {
			return;
		}
		if (!MApp.get().isLogin()) {
			goLogin();
			return;
		}
		if ("0".equals(detailData.is_claim)){
			ToastHelper.showShortToast(R.string.not_claim);
			return;
		}
		
		if (TextUtils.isEmpty(detailData.user_id)){
			ToastHelper.showShortToast(R.string.merchant_data_err);
			return;
		}
		// TODO do focus
		doFoucus();
	}

	private void goLogin() {
		Intent i = new Intent(MApp.get(), LoginActivity.class);
		startActivity(i);
	}

	private void goCall() {
		if (detailData == null || TextUtils.isEmpty(detailData.tel)) {
			return;
		}
		if (phoneNumbers.length > 1){
			showCallNumberPickDialog();
		}else{
			String number = detailData.tel;
			call(number);
		}
	}

	private void call(String number){
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" + number));
		startActivity(intent);
		saveCallClick();
	}
	
	private void showCallNumberPickDialog(){
		final String[] items = new String[phoneNumbers.length + 1];
		items[0] = detailData.tel;
		for(int i=0;i<phoneNumbers.length;i++){
			items[i+1] = phoneNumbers[i];
		}
		WeddingDialog dialog = new WeddingDialog.Builder(getActivity()).setTitle(R.string.pls_chose_your_call_number).setItems(items, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				call(items[which]);
			}
		}).create();
		dialog.show();
	}
	
	private void saveCallClick() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CALL_CLICK;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						D.info(TAG, "call click response: " + arg0);
					}
				}, null) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("sl_id", sa_loc_id);
				return params;
			}

		};
		executeRequest(sr);
	}

	private void goGetThisMerchant() {
		if (!MApp.get().isLogin()) {
			Intent goLogin = new Intent(MApp.get(), LoginActivity.class);
			startActivity(goLogin);
			return;
		}
		showLoadingDialog();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CLAIM_MERCHANT;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						D.info(TAG, "CLAIM merchant: " + arg0);
						if (TextUtils.isEmpty(arg0)) {
							showAlert(MApp.get().getString(R.string.net_err));
							return;
						}
						try {
							BaseApiResult result = new Gson().fromJson(arg0,
									BaseApiResult.class);
							showAlert(result.op_info);
						} catch (Exception e) {
							// TODO: handle exception
							showAlert(MApp.get().getString(R.string.net_err));
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						showAlert(MApp.get().getString(R.string.net_err));
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("user_id", MApp.get().getUser().user.user_id);
				params.put("sl_id", sa_loc_id);
				return params;
			}

		};
		executeRequest(sr);
	}

	private void goPublishList() {
		if (detailData == null) {
			return;
		}
		MerchantDetailPublishFragment mdpf = MerchantDetailPublishFragment
				.newInstance(detailData);
		Bundle args = new Bundle();
		args.putString(Constants.BUNDLE_MERCHANT_NAME, merchantName);
		args.putString(Constants.BUNDLE_MERCHANT_LOC_ID, sa_loc_id);
		mdpf.setArguments(args);
		FragmentTransaction ft = getActivity().getSupportFragmentManager()
				.beginTransaction();
		ft.addToBackStack(TAG);
		ft.add(android.R.id.content, mdpf, MerchantDetailPublishFragment.TAG)
				.commit();
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		showAlert(MApp.get().getString(R.string.net_err));
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		D.debug(TAG, "get merchant detail: " + arg0);
		if (TextUtils.isEmpty(arg0)) {
			showAlert(MApp.get().getString(R.string.net_err));
			return;
		}
		try {
			MerchantDetailBean bean = new Gson().fromJson(arg0,
					MerchantDetailBean.class);
			if (Constants.SUCCESS.equals(bean.op_code)) {
				detailData = bean.sa_dtldtl;
				bindData();
			} else {
				showAlert(bean.op_info);
			}

		} catch (Exception e) {
			// TODO: handle exception
			showAlert(MApp.get().getString(R.string.net_err));
		}
	}

	@Override
	public void sendComment(final String comment) {
		// TODO Auto-generated method stub
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_SEND_COMMENT;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						if (TextUtils.isEmpty(arg0)) {
							ToastHelper.showShortToast(R.string.net_err);
							return;
						}
						try {
							BaseApiResult result = new Gson().fromJson(arg0,
									BaseApiResult.class);
							if (Constants.SUCCESS.equals(result.op_code)) {
								ToastHelper
										.showShortToast(R.string.comment_success);
							} else {
								ToastHelper.showShortToast(result.op_info);
							}
						} catch (Exception e) {
							// TODO: handle exception
							ToastHelper.showShortToast(R.string.net_err);
						}

					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						ToastHelper.showShortToast(R.string.net_err);
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("content", comment);
				params.put("user_id", MApp.get().getUser().user.user_id);
				params.put("sa_loc_id", sa_loc_id);
				return params;
			}

		};
		try {
			D.info(TAG, "body: "+new String(sr.getBody()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		showLoadingDialog();
		executeRequest(sr);
	}

	private void doFoucus() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_ADD_FOLLOW;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String response) {
						dismissLoadingDialog();
						BaseApiResult info = null;
						try {
							info = new Gson().fromJson(response,
									BaseApiResult.class);
						} catch (JsonSyntaxException e) {
							e.printStackTrace();
						}
						if (Constants.SUCCESS.equals(info.op_code)) {
							ToastHelper
									.showShortToast(R.string.string_has_followed);
							mFocusTag
									.setImageResource(R.drawable.details_added_follow);
							mFocusState
									.setText(R.string.merchant_detail_focused);
							detailData.focus_state = "true";
						} else {
							ToastHelper.showShortToast(info.op_info);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						dismissLoadingDialog();
						ToastHelper.showShortToast(R.string.net_err);
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> map = new HashMap<String, String>();
				UserInfo user = MApp.get().getUser();
				map.put("focus_user_id", user.user.user_id);
				map.put("focus_user_name", user.user.user_name);
				map.put("focused_user_id", detailData.user_id);
				map.put("focused_user_name", merchantName);
				map.put("focus_flag", "1");
				return map;
			}

		};
		try {
			D.info(TAG, "body: "+new String(sr.getBody()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		showLoadingDialog();
		executeRequest(sr);
	}
}
