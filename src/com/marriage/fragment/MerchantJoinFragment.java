package com.marriage.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.CityActivity;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.MerchantCategory;
import com.marriage.bean.MerchantCategoryBean;
import com.marriage.bean.MerchantSubCategory;
import com.marriage.bean.MerchantSubCategoryBean;
import com.marriage.dialog.WeddingDialog;
import com.marriage.dialog.WeddingShareAddDialog;
import com.marriage.utility.AppConfig;
import com.marriage.utility.BitmapFileUtil;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.FileUtil;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.adapter.MerchantCategoryAdapter;

public class MerchantJoinFragment extends BaseFragment implements
		OnClickListener, Listener<String>, ErrorListener {

	public static final String TAG = "MerchantJoinFragment";

	private static final int REQUEST_CODE_CITY = 0x0001;
	private static final int REQUEST_CODE_LOGO_CAMERA = 0x0002;
	private static final int REQUEST_CODE_LOGO_GALLERY = 0x0003;
	private static final int REQUEST_CODE_INFO_CAMERA = 0x0004;
	private static final int REQUEST_CODE_INFO_GALLERY = 0x0005;
	private static final int REQUEST_CODE_LOGO_CROP = 0x0006;
	private static final int LOGO_WIDTH = 160;
	private static final int LOGO_HEIGHT = 160;

	private EditText nameInput, addressInput, telInput, openTimeInput;
	private TextView categoryShow, subCategoryShow, cityShow;
	private ImageView merchantLogoIpload, merchantIdUpload;

	private String cityName, cityId;

	private MerchantCategoryBean mCategory;
	private MerchantCategory mCurrentCategory;

	private MerchantSubCategoryBean mSubCategory;
	private MerchantSubCategory mCurrentSubCategory;

	private MerchantCategoryAdapter mCateoryAdapter;
	private MerchantCategoryAdapter mSubCateoryAdapter;

	private WeddingShareAddDialog mLogoDialog;
	private WeddingShareAddDialog mIdInfoDialog;

	private Bitmap mLogo;
	private File mImageFile;

	private String mSa_name;
	private String mAddress;
	private String mTel;
	private String mOpen_time;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mCateoryAdapter = new MerchantCategoryAdapter(MApp.get());
		mSubCateoryAdapter = new MerchantCategoryAdapter(MApp.get());
		String cachedCategory = getFromCache(Constants.CACHE_MERCHANT_CATEGORY);
		if (TextUtils.isEmpty(cachedCategory)) {
			getCategoryFromServer();
		} else {
			mCategory = new Gson().fromJson(cachedCategory,
					MerchantCategoryBean.class);
			mCateoryAdapter.setDatas(mergeCategory2String());
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_merchant_join,
				container, false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(View v) {
		nameInput = (EditText) v.findViewById(R.id.join_merchant_name_input);
		addressInput = (EditText) v
				.findViewById(R.id.join_merchant_address_input);
		telInput = (EditText) v.findViewById(R.id.join_merchant_tel_input);
		openTimeInput = (EditText) v
				.findViewById(R.id.join_merchant_open_time_input);
		categoryShow = (TextView) v
				.findViewById(R.id.join_merchant_category_show);
		subCategoryShow = (TextView) v
				.findViewById(R.id.join_merchant_sub_category_show);
		cityShow = (TextView) v.findViewById(R.id.join_merchant_city_show);
		merchantLogoIpload = (ImageView) v
				.findViewById(R.id.upload_merchant_logo);
		merchantIdUpload = (ImageView) v.findViewById(R.id.id_info_upload);

		v.findViewById(R.id.back_btn).setOnClickListener(this);
		v.findViewById(R.id.confirm_submit).setOnClickListener(this);
		v.findViewById(R.id.join_merchant_category_chose).setOnClickListener(
				this);
		v.findViewById(R.id.join_merchant_sub_category_chose)
				.setOnClickListener(this);
		v.findViewById(R.id.join_merchant_city_chose).setOnClickListener(this);
		merchantLogoIpload.setOnClickListener(this);
		merchantIdUpload.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			popBack();
			break;
		case R.id.confirm_submit:
			joinConfirm();
			break;
		case R.id.join_merchant_category_chose:
			categoryChose();
			break;
		case R.id.join_merchant_sub_category_chose:
			subCategoryChose();
			break;
		case R.id.join_merchant_city_chose:
			cityChose();
			break;
		case R.id.upload_merchant_logo:
			uploadMerchantLogo();
			break;
		case R.id.id_info_upload:
			idInfoUpload();
			break;
		}

	}

	private void popBack() {
		getActivity().onBackPressed();
	}

	private void joinConfirm() {
		if (checkInfoInput()) {
			doJoinConfirm();
		}
	}

	private void categoryChose() {
		showCategoryPickDialog();
	}

	private void subCategoryChose() {
		showSubCategoryPickDialog();
	}

	private void cityChose() {
		Intent goCity = new Intent(MApp.get(), CityActivity.class);
		startActivityForResult(goCity, REQUEST_CODE_CITY);
	}

	private void doJoinConfirm() {
		showLoadingDialog();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_MERCHANT_JOIN;
		StringRequest sr = new StringRequest(Method.POST, url, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				File logoF = BitmapFileUtil.saveTempBmp(mLogo, MApp.get());
				String savedPath = BitmapFileUtil.compress(logoF);
				String logoString = FileUtil.fileToString(savedPath);

				Map<String, String> params = new HashMap<String, String>();
				params.put("user_id", MApp.get().getUser().user.user_id);
				params.put("sa_name", mSa_name);
				params.put("deal_cate_id", mCurrentCategory.id);
				params.put("deal_cate_type_id", mCurrentSubCategory.type_id);
				params.put("city_id", cityId);
				params.put("address", mAddress);
				params.put("tel", mTel);
				params.put("open_time", mOpen_time);
				params.put("sa_logo", logoString);
				return params;
			}

		};
		executeRequest(sr);
	}

	private boolean checkInfoInput() {
		mSa_name = nameInput.getText().toString().trim();
		if (TextUtils.isEmpty(mSa_name)) {
			ToastHelper.showShortToast(R.string.pls_input_merchant_name);
			return false;
		}

		if (mCurrentCategory == null) {
			ToastHelper.showShortToast(R.string.pls_chose_category);
			return false;
		}

		if (mCurrentSubCategory == null) {
			ToastHelper.showShortToast(R.string.pls_chose_sub_category);
			return false;
		}

		if (TextUtils.isEmpty(cityId)) {
			ToastHelper.showShortToast(R.string.pls_chose_merchant_city);
			return false;
		}

		mAddress = addressInput.getText().toString().trim();
		if (TextUtils.isEmpty(mAddress)) {
			ToastHelper.showShortToast(R.string.pls_input_address);
			return false;
		}

		mTel = telInput.getText().toString().trim();
		if (TextUtils.isEmpty(mTel)) {
			ToastHelper.showShortToast(R.string.pls_input_tel);
			return false;
		}

		mOpen_time = openTimeInput.getText().toString().trim();
		if (TextUtils.isEmpty(mOpen_time)) {
			ToastHelper.showShortToast(R.string.pls_input_open_time);
			return false;
		}

		if (mLogo == null) {
			ToastHelper.showShortToast(R.string.pls_input_logo);
			return false;
		}

		return true;
	}

	private void uploadMerchantLogo() {
		if (mLogoDialog == null) {
			mLogoDialog = new WeddingShareAddDialog(logoListener);
		}
		mLogoDialog.show(getFragmentManager(), "LOGO_DIALOG");
	}

	private void idInfoUpload() {
		if (mIdInfoDialog == null) {
			mIdInfoDialog = new WeddingShareAddDialog(idInfoListener);
		}
		mIdInfoDialog.show(getFragmentManager(), "ID_INFO_DIALOG");
	}

	private void onCityChosed(Intent data) {
		cityName = data.getStringExtra(CityActivity.KEY_CITY_NAME);
		cityId = data.getStringExtra(CityActivity.KEY_CITY_ID);
		if (!TextUtils.isEmpty(cityId) && !TextUtils.isEmpty(cityName)) {
			cityShow.setText(cityName);
		}
	}

	private void onCategoryChosed(MerchantCategory c) {
		if (mCurrentCategory != null && c.id.equals(mCurrentCategory.id)) {
			return;
		}
		mCurrentCategory = c;
		categoryShow.setText(c.name);
		// reset sub category
		mCurrentSubCategory = null;
		subCategoryShow.setText("");

		String cachedSubCategory = getFromCache(Constants.CACHE_MERCHANT_SUB_CATEGORY
				+ "_" + c.id);
		if (TextUtils.isEmpty(cachedSubCategory)) {
			getSubCategoryFromServer(c.id);
		} else {
			mSubCategory = new Gson().fromJson(cachedSubCategory,
					MerchantSubCategoryBean.class);
			mSubCateoryAdapter.setDatas(mergeSubCategory2String());
		}
	}

	private void onSubCategoryChosed(MerchantSubCategory sc) {
		mCurrentSubCategory = sc;
		subCategoryShow.setText(sc.type_name);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case REQUEST_CODE_CITY:
			if (resultCode == Activity.RESULT_OK && data != null) {
				onCityChosed(data);
			}
			break;
		case REQUEST_CODE_LOGO_CAMERA:
		case REQUEST_CODE_LOGO_GALLERY:
			if (resultCode == Activity.RESULT_OK) {
				onPhotoTaken(data);
			}
			break;
		case REQUEST_CODE_INFO_CAMERA:
		case REQUEST_CODE_INFO_GALLERY:
			if (resultCode == Activity.RESULT_OK && data != null) {
				/*mIdInfo = onPhotoTaken(data);
				if (mIdInfo == null) {
					ToastHelper.showShortToast(R.string.chose_pic_failure);
				} else {

				}*/
			}
			break;
		case REQUEST_CODE_LOGO_CROP:
			if (resultCode == Activity.RESULT_OK && data != null) {
				Uri imgUri = data.getData();
				try {
					mLogo = ImageUtil.loadRotatedBitmapWithSizeLimitation(
							MApp.get(), LOGO_WIDTH, LOGO_HEIGHT, imgUri);
				} catch (Exception e) {
					// TODO: handle exception
				}

				if (mLogo == null) {
					ToastHelper.showShortToast(R.string.chose_logo_failure);
				} else {
					onLogoTaked(mLogo);
				}
			}
			break;
		}
	}

	private void onLogoTaked(Bitmap logo) {
		int bw = logo.getWidth();
		int bh = logo.getHeight();
		int imgWidth = merchantLogoIpload.getWidth();
		int imgHeight = imgWidth * bh / bw;
		merchantLogoIpload.getLayoutParams().height = imgHeight;
		merchantLogoIpload.setImageBitmap(logo);
	}

	private void getCategoryFromServer() {
		showLoadingDialog();
		final String serverUrl = AppConfig.INSTANCE.SERVER
				+ Constants.URL_MERCHANT_CATEGORY;
		StringRequest sr = new StringRequest(Method.POST, serverUrl,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						if (TextUtils.isEmpty(arg0)) {
							return;
						}
						D.error(TAG, serverUrl);
						D.error(TAG, arg0);
						try {
							mCategory = new Gson().fromJson(arg0,
									MerchantCategoryBean.class);

							if (mCategory != null) { // 异步滴 重新设一下没错的
								mCateoryAdapter
										.setDatas(mergeCategory2String());
							}

							if (mCategory != null) {
								saveToCache(Constants.CACHE_MERCHANT_CATEGORY,
										arg0);
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("cate_id", "");
				return params;
			}

		};
		executeRequest(sr);
	}

	private void getSubCategoryFromServer(final String cateId) {
		showLoadingDialog();
		String url = AppConfig.INSTANCE.SERVER
				+ Constants.URL_MERCHANT_SUB_CATEGORY;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						if (TextUtils.isEmpty(arg0)) {
							return;
						}
						D.error(TAG, arg0);
						try {
							mSubCategory = new Gson().fromJson(arg0,
									MerchantSubCategoryBean.class);

							if (mSubCategory != null) { // 异步滴 重新设一下没错的
								mSubCateoryAdapter
										.setDatas(mergeSubCategory2String());
							}

							if (mCategory != null) {
								saveToCache(
										Constants.CACHE_MERCHANT_SUB_CATEGORY
												+ "_" + cateId, arg0);
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("cate_id", cateId);
				params.put("type_id", "");
				return params;
			}

		};
		executeRequest(sr);
	}

	private List<String> mergeCategory2String() {
		List<String> data = new ArrayList<String>();
		if (mCategory != null && mCategory.cates != null) {
			for (MerchantCategory category : mCategory.cates) {
				data.add(category.name);
			}
		}
		return data;

	}

	private List<String> mergeSubCategory2String() {
		List<String> data = new ArrayList<String>();
		if (mSubCategory != null && mSubCategory.type_list != null) {
			for (MerchantSubCategory subCategory : mSubCategory.type_list) {
				data.add(subCategory.type_name);
			}
		}
		return data;
	}

	private void showCategoryPickDialog() {
		List<String> list = mergeCategory2String();
		String[] data = new String[list.size()];
		data = list.toArray(data);
		new WeddingDialog.Builder(getActivity()).setItems(data,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						onCategoryChosed(mCategory.cates.get(which));
					}
				}).show();
	}

	private void showSubCategoryPickDialog() {
		List<String> list = mergeSubCategory2String();
		if (list.size() == 0) {
			list.add(MApp.get().getString(R.string.merchant_sub_category_all));
		}
		String[] data = new String[list.size()];
		data = list.toArray(data);
		new WeddingDialog.Builder(getActivity()).setItems(data,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if (mSubCategory == null
								|| mSubCategory.type_list == null
								|| mSubCategory.type_list.size() == 0) {
							MerchantSubCategory msc = new MerchantSubCategory();
							msc.type_id = "";
							msc.type_name = MApp.get().getString(
									R.string.merchant_sub_category_all);
							onSubCategoryChosed(msc);
						} else {
							onSubCategoryChosed(mSubCategory.type_list
									.get(which));
						}

					}
				}).show();
	}

	private OnClickListener logoListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.share_weixin:
				getPicByCamera(REQUEST_CODE_LOGO_CAMERA);
				break;

			case R.id.share_weixin_friends:
				getPicByGallery(REQUEST_CODE_LOGO_GALLERY);
				break;
			}
			mLogoDialog.dismissAllowingStateLoss();
		}
	};

	private OnClickListener idInfoListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int id = v.getId();
			switch (id) {
			case R.id.share_weixin:
				getPicByCamera(REQUEST_CODE_INFO_CAMERA);
				break;

			case R.id.share_weixin_friends:
				getPicByGallery(REQUEST_CODE_INFO_GALLERY);
				break;
			}
			mIdInfoDialog.dismissAllowingStateLoss();
		}
	};

	private void getPicByCamera(int requestCode) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		mImageFile = new File(FileUtil.createPhotoPath());
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mImageFile));
		startActivityForResult(intent, requestCode);
	}

	private void getPicByGallery(int requestCode) {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_PICK);
		startActivityForResult(intent, requestCode);
	}

	private void cropImageLogo(int requestCode, Uri url) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		Uri uri = url;
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", LOGO_WIDTH);
		intent.putExtra("outputY", LOGO_HEIGHT);
		intent.putExtra("return-data", false);
		try {
			startActivityForResult(intent, requestCode);
		} catch (ActivityNotFoundException e) {

		}
	}

	private void onPhotoTaken(Intent data) {
		Uri uri = null;
		if (data != null) {
			uri = data.getData();
		}
		if (uri == null) {
			uri = Uri.fromFile(mImageFile);
		}
		cropImageLogo(REQUEST_CODE_LOGO_CROP, uri);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		showAlert(MApp.get().getString(R.string.net_err));
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		D.error(TAG, "join response: " + arg0);
		dismissLoadingDialog();
		try {
			BaseApiResult result = new Gson().fromJson(arg0,
					BaseApiResult.class);
			if (Constants.SUCCESS.equals(result.op_code)) {
				showSuccessAlert(result.op_info);
			} else {
				showAlert(result.op_info);
			}
		} catch (Exception e) {
			// TODO: handle exception
			showAlert(MApp.get().getString(R.string.net_err));
		}

	}

	private void showSuccessAlert(String msg) {
		FragmentActivity a = getActivity();
		if (a == null) {
			return;
		}
		WeddingDialog dialog = new WeddingDialog.Builder(a)
				.setTitle(R.string.prompt)
				.setMessage(msg)
				.setPositiveButton(R.string.confirm,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								getActivity().finish();
							}
						}).create();
		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				getActivity().finish();
			}
		});
		dialog.show();

	}

}
