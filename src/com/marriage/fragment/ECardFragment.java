package com.marriage.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.MApp.LoginChangedListener;
import com.marriage.R;
import com.marriage.activity.LoginActivity;
import com.marriage.activity.MakeCardActivity;
import com.marriage.activity.MakeExplainActivity;
import com.marriage.activity.MerchantCollaborateActivity;
import com.marriage.activity.SendCardActivity;
import com.marriage.activity.WeddingWebViewActivity;
import com.marriage.bean.XTAdv;
import com.marriage.bean.XTAdvInfo;
import com.marriage.bean.XTInfo;
import com.marriage.bean.XTList;
import com.marriage.delegate.StepMakeCard;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.PageRecord;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.lib.vpi.CirclePageIndicator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ECardFragment extends BaseFragment implements
		View.OnClickListener, Listener<String>, ErrorListener, OnScrollListener, LoginChangedListener, OnItemClickListener {

	private View mHeaderView;
	private ListView mList;
	private ViewPager mPager;
	private CirclePageIndicator mIndicator;
	private WeddingCardAdapter mAdapter;
	private Request<String> mCardReq, mBannerReq;
	private PageRecord mPage;
	private MyAdapter mAdvAdapter;
	private List<XTList> mXTList = new ArrayList<XTList>();
	private List<XTAdv> mAdvList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCardReq = new StringRequest(Method.POST, AppConfig.INSTANCE.SERVER
				+ Constants.URL_GET_XITIE_LIST, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				String userId = MApp.get().isLogin() ? MApp.get().getUser().user.user_id
						: "";
				params.put("user_id", userId);
				params.put("page_index",
						String.valueOf(mPage.currentPageIndex()));
				params.put("page_size", String.valueOf(mPage.getPageSize()));

				return params;
			}

			@Override
			public boolean isCanceled() {
				// TODO Auto-generated method stub
				return false;
			}

		};
		mPage = new PageRecord(1, 20);

		mBannerReq = new StringRequest(Method.POST, AppConfig.INSTANCE.SERVER
				+ Constants.URL_CARD_GET_ADV, new Listener<String>() {

			@Override
			public void onResponse(String response) {
				XTAdvInfo info;
				try {
					info = new Gson().fromJson(response, XTAdvInfo.class);
				} catch (JsonSyntaxException e) {
					e.printStackTrace();
					return;
				}
				
				if(Constants.SUCCESS.equals(info.op_code)) {
					mAdvAdapter.setData(mAdvList = info.adv_list);
				}

			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {

			}
		}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("adv_type", "xitie");
				return params;
			}

		};
		MApp.get().getRequestQueue().add(mBannerReq);
		MApp.get().addLoginChangedListener(this);
	}

	@Override
	public void onDestroy() {
		MApp.get().removeLoginChangedListener(this);
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootList = inflater.inflate(R.layout.fragment_e_card_list,
				container, false);
		mHeaderView = inflater.inflate(R.layout.fragment_e_wedding_card,
				(ViewGroup) rootList.findViewById(android.R.id.list), false);
		nextPage();
		return rootList;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		mPager = (ViewPager) mHeaderView.findViewById(R.id.pager);
		mPager.setAdapter(mAdvAdapter = new MyAdapter());
		mAdvAdapter.setData(mAdvList);
		mIndicator = (CirclePageIndicator) mHeaderView
				.findViewById(R.id.pager_indicator);
		mIndicator.setViewPager(mPager);
		mList = (ListView) view.findViewById(android.R.id.list);
		mList.addHeaderView(mHeaderView, null, false);
		mList.setOnScrollListener(this);
		mList.setAdapter(mAdapter = new WeddingCardAdapter(this, mXTList));
		mList.setOnItemClickListener(this);
		mHeaderView.findViewById(R.id.btn_share_to_friends_cycle)
				.setOnClickListener(this);
		mHeaderView.findViewById(R.id.btn_make_explain)
				.setOnClickListener(this);
		mHeaderView.findViewById(R.id.btn_merchant_cooperation)
				.setOnClickListener(this);
		mHeaderView.findViewById(R.id.btn_make_card).setOnClickListener(this);

		super.onViewCreated(view, savedInstanceState);
	}

	private boolean nextPage() {
		if (mPage.hasNextPager() && MApp.get().isLogin()) {
			getRequestQueue().add(mCardReq);
			return true;
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_share_to_friends_cycle:
			Log.d("MARTIN", "share_to_friends_cycle");
			Intent goTemplate = new Intent(MApp.get(),
					WeddingWebViewActivity.class);
			goTemplate.putExtra(Constants.BUNDLE_WEBVIEW_TITLE, MApp.get()
					.getString(R.string.title_xitie_template));
			goTemplate.putExtra(Constants.BUNDLE_WEBVIEW_URL,
					Constants.XITIE_TEMPLATE);
			startActivity(goTemplate);
			break;
		case R.id.btn_make_explain: {
			Intent intent = new Intent(getActivity(), MakeExplainActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.btn_merchant_cooperation: {
			Intent intent = new Intent(getActivity(),
					MerchantCollaborateActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.btn_make_card: {
			if (!MApp.get().isLogin()) {
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
				return;
			}

			Intent intent = new Intent(getActivity(), MakeCardActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.btn_modify: {
			Integer pos = (Integer) v.getTag();
			if(pos != null) {
				XTList xt = mXTList.get(pos.intValue());
				if(xt != null && !TextUtils.isEmpty(xt.id)) {
					Intent intent = new Intent(getActivity(), MakeCardActivity.class);
					intent.putExtra(StepMakeCard.EXTRA_MAKE_CARD_STEP, StepMakeCard.MAKE_CARD_STEP_INPUT_INFO);
					intent.putExtra(StepMakeCard.EXTRA_CARD_ID, xt.id);
					startActivity(intent);
				}
			}

			break;
		}
		case R.id.btn_send: {
			Integer pos = (Integer) v.getTag();
			if(pos != null && mXTList.get(pos.intValue()) != null ) {
				Intent intent = new Intent(getActivity(), SendCardActivity.class);
				intent.putExtra("XT", mXTList.get(pos.intValue()));
				startActivity(intent);
			}
			break;
		}
		case R.id.btn_relative_msg: {
			Integer pos = (Integer) v.getTag();
			Log.d("MARTIN", pos + "");
			break;
		}
		}
	}

	static class MyAdapter extends PagerAdapter {
		List<XTAdv> data;

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		public void setData(List<XTAdv> data) {
			this.data = data;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return data != null ? data.size() : 0;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ImageView iv = new ImageView(container.getContext());
			iv.setScaleType(ScaleType.CENTER_CROP);
			ImageLoader.getInstance().displayImage(data.get(position).img, iv);
			container.addView(iv);
			return iv;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

	}

	static class WeddingCardAdapter extends BaseAdapter {

		DisplayImageOptions option = new DisplayImageOptions.Builder()
				.cacheInMemory(true).cacheOnDisk(true)
//				.showImageOnLoading(R.drawable.default_iphone)
//				.showImageForEmptyUri(R.drawable.default_iphone)
//				.showImageOnFail(R.drawable.default_iphone)
				.bitmapConfig(Config.RGB_565).build();

		OnClickListener onclick;
		List<XTList> data;

		public WeddingCardAdapter(OnClickListener onclick, List<XTList> data) {
			this.onclick = onclick;
			this.data = data;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			HolderView holder;
			if (convertView == null) {
				holder = new HolderView();
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.item_list_wedding_card, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.iv_wedding_card_pic);
				holder.name = (TextView) convertView
						.findViewById(R.id.tv_mr_ms);
				holder.date = (TextView) convertView
						.findViewById(R.id.tv_wedding_date);
				holder.msg = (TextView) convertView
						.findViewById(R.id.tv_wedding_notify_msg);
				holder.btnModify = (TextView) convertView
						.findViewById(R.id.btn_modify);
				holder.btnLeaverMsg = (TextView) convertView
						.findViewById(R.id.btn_relative_msg);
				holder.btnSend = (TextView) convertView
						.findViewById(R.id.btn_send);
				convertView.setTag(holder);
			} else {
				holder = (HolderView) convertView.getTag();
			}
			ImageLoader.getInstance().displayImage(
					data.get(position).index_img, holder.icon, option);

			holder.name.setText(MApp.get().getString(
					R.string.string_placeholder_bride_bridegroom,
					data.get(position).mname, data.get(position).fname));
			holder.date.setText(data.get(position).marry_date);
			holder.msg.setText(data.get(position).title);

			holder.btnModify.setTag(position);
			holder.btnModify.setOnClickListener(onclick);

			holder.btnLeaverMsg.setTag(position);
			holder.btnLeaverMsg.setOnClickListener(onclick);

			holder.btnSend.setTag(position);
			holder.btnSend.setOnClickListener(onclick);

			return convertView;
		}

		static class HolderView {
			TextView name, date, msg, btnModify, btnLeaverMsg, btnSend;
			ImageView icon;
		}

	}

	@Override
	public void onErrorResponse(VolleyError error) {
		dismissLoadingDialog();
	}

	@Override
	public void onResponse(String response) {
		dismissLoadingDialog();
		D.debug("MARTIN", response);
		XTInfo info;
		try {
			info = new Gson().fromJson(response, XTInfo.class);
		} catch (JsonSyntaxException e) {
			return;
		}
		if (info != null && info.xt_list != null) {
			try {
				int count = Integer.valueOf(info.xt_count);
				mPage.preNextPager();
				mPage.setTotalSize(count);
				mXTList.addAll(info.xt_list);
				mAdapter.notifyDataSetChanged();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		switch (scrollState) {
		case OnScrollListener.SCROLL_STATE_IDLE:
			if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
				if (nextPage()) {
					D.error("MARTIN", ">>>>>>>>>>>>加载更多<<<<<<<<<<<");
				}
			}
			break;
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if ((Intent.FLAG_ACTIVITY_CLEAR_TOP & intent.getFlags()) != 0) {
			mPage = new PageRecord(1, 20);
			nextPage();
			mXTList.clear();
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onLoginChanged(boolean isLogin) {
		mXTList.clear();
		mPage.clear();
		if(!isDetached()) {
			nextPage();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if(mXTList != null && mXTList.size() > position) {
			String url = mXTList.get(position).url;
			Intent goTemplate = new Intent(MApp.get(),
					WeddingWebViewActivity.class);
			goTemplate.putExtra(Constants.BUNDLE_WEBVIEW_TITLE, MApp.get()
					.getString(R.string.string_card_preview));
			goTemplate.putExtra(Constants.BUNDLE_WEBVIEW_URL, url);
			startActivity(goTemplate);
		}
		
	}
	
}
