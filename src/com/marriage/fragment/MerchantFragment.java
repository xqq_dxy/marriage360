package com.marriage.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.MerchantDetailActivity;
import com.marriage.bean.City;
import com.marriage.bean.Merchant;
import com.marriage.bean.MerchantCategory;
import com.marriage.bean.MerchantCategoryBean;
import com.marriage.bean.MerchantListBean;
import com.marriage.fragment.WeddingShareFragment.SortAdapter;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.PreferenceHelper;
import com.marriage.widget.LoadingView;
import com.marriage.widget.TagLayout;
import com.marriage.widget.adapter.MerchantCategoryAdapter;
import com.marriage.widget.adapter.MerchantListAdapter;

public class MerchantFragment extends BaseFragment implements
		TagLayout.OnCityChoiceListener, OnScrollListener, OnItemClickListener,
		Listener<String>, ErrorListener {

	public static final String TAG = "MerchantFragment";

	private ListView mMerchantList, mCategoryList;
	private TagLayout mTagViewLayout;
	private MerchantListAdapter mAdapter;
	private LoadingView mFooter;

	private String cityId;
	private String cityName;
	private String sort = "0";
	private int pageIndex = 1;
	private int pageSize = 10;

	private MerchantCategoryBean mCategory;
	private MerchantCategory mCurrentCategory;

	private boolean isFetchingData;
	private boolean canFetchNext = true;
	
	private MerchantCategoryAdapter mCateoryAdapter;
	
	static List<String> sortData;
	static {
		sortData = new ArrayList<String>();
		sortData.add("全部商家");
		sortData.add("热门商家");
//		sortData.add("点击量");
//		sortData.add("发布时间");
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mAdapter = new MerchantListAdapter(MApp.get());
		cityId = "1";
		if (!checkCategoryCache()) {
			getCategoryFromServer();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_merchant_list,
				container, false);
		initViews(mainView, inflater);
		return mainView;
	}

	private void initViews(View v, LayoutInflater inflater) {
		mTagViewLayout = (TagLayout) v.findViewById(R.id.merchant_tag_layout);
		mMerchantList = (ListView) v.findViewById(R.id.merchant_list);
		mFooter = new LoadingView(getActivity());
		mFooter.dismiss();
		mMerchantList.addFooterView(mFooter);
		mMerchantList.setAdapter(mAdapter);
		mMerchantList.setOnItemClickListener(this);
		mMerchantList.setOnScrollListener(this);
		View ciy = inflater.inflate(R.layout.filter_pop_city, null);
		mCategoryList = (ListView)inflater.inflate(R.layout.filter_pop_category, null);
		
		ListView sortList = (ListView) inflater.inflate(
				R.layout.filter_pop_sort, null);
		sortList.setBackgroundColor(Color.WHITE);
		sortList.setAdapter(new SortAdapter(getActivity(), sortData));
		sortList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				mTagViewLayout.dismissPopWindow();
				//TODO
				if(String.valueOf(position).equals(sort)) {
					return;
				}
				
				mTagViewLayout.setTagName(2, sortData.get(position));
				if (position == 0){
					sort = "0";
				}else if(position == 1){
					sort = "1";
				}
				refereshData();
			}
		});
		
		mTagViewLayout.addTag(0, ciy, getString(R.string.string_city));
		mTagViewLayout.addTag(1, mCategoryList, getString(R.string.category));
		mTagViewLayout.addTag(2, sortList, sortData.get(0));
		mTagViewLayout.setOnCityChoiceListener(this);
		
		mCategoryList.setAdapter(mCateoryAdapter = new MerchantCategoryAdapter(MApp.get(),
				mergeData2String()));
		mCategoryList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				mTagViewLayout.dismissPopWindow();
				MerchantCategory temp = null;
				if (position == 0){
					temp = new MerchantCategory();
					temp.id = "";
					temp.name = MApp.get().getString(R.string.category);
				}else{
					temp = mCategory.cates.get(position-1);
				}
				if (mCurrentCategory != null && temp.id.equals(mCurrentCategory.id)){
					return;
				}
				mCurrentCategory = temp;
				saveCateCache();
				mTagViewLayout.setTagName(1, mCurrentCategory.name);
				refereshData();
			}
		});
		
		if (!TextUtils.isEmpty(WeddingShareFragment.cityId) && !TextUtils.isEmpty(WeddingShareFragment.cityName)){
			this.cityId = WeddingShareFragment.cityId;
			this.cityName = WeddingShareFragment.cityName;
			mTagViewLayout.setTagName(0, cityName);
		}
		getCateCache();
	}

	private void saveCateCache(){
		if (mCurrentCategory == null){
			return;
		}
		PreferenceHelper.setString(Constants.REFERENCE_CATEGORY_ID, mCurrentCategory.id);
		PreferenceHelper.setString(Constants.REFERENCE_CATEGORY_NAME, mCurrentCategory.name);
	}
	
	private void getCateCache(){
		String cateName = PreferenceHelper.getString(Constants.REFERENCE_CATEGORY_NAME, "");
		String cateId = PreferenceHelper.getString(Constants.REFERENCE_CATEGORY_ID, "");
		if (TextUtils.isEmpty(cateName)||TextUtils.isEmpty(cateId)){
			return;
		}
		mCurrentCategory = new MerchantCategory();
		mCurrentCategory.id = cateId;
		mCurrentCategory.name = cateName;
		mTagViewLayout.setTagName(1, mCurrentCategory.name);
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mAdapter.getCount() == 0){
			fetchData(true);
		}
	}
	
	private boolean checkCategoryCache() {
		String cache = getFromCache(Constants.CACHE_MERCHANT_CATEGORY);
		D.debug(TAG, "cache:" + cache);
		if (!TextUtils.isEmpty(cache)) {
			try {
				mCategory = new Gson().fromJson(cache, MerchantCategoryBean.class);
				return true;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return false;
			}

		}
		return false;
	}

	@Override
	public void onCityChoice(int type, City data) {
		// TODO Auto-generated method stub
//		if (type == 0) { // 全国
//			onCity("1", MApp.get().getString(R.string.string_country));
//		} else if (type == 1) { // 同城
//			onCity("-1", MApp.get().getString(R.string.string_same_city));
//		} else if (type == 2 && data != null) { // 选择城市
//			onCity(data.id, data.name);
//		}
		onCity(data.id, data.name);
	}
	
	private void onCity(String id, String name){
		cityName = name;
		mTagViewLayout.setTagName(0, cityName);
		if (id.equals(cityId)){
			return;
		} else {
			cityId = id;
			refereshData();
		}
	}

	private void fetchData(boolean isFirst) {
		isFetchingData = true;
		if (isFirst){
			showLoadingDialog();
		}
		Location l = MApp.get().getCurrentLocation();
		String _x = "";
		String _y = "";
		if (l != null){
			_x = String.valueOf(l.getLongitude());
			_y = String.valueOf(l.getLatitude());
		}
		final String x = _x;
		final String y = _y;
		StringRequest sr = new StringRequest(Method.POST,
				AppConfig.INSTANCE.SERVER + Constants.URL_MERCHANT_LIST, this,
				this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				
				Map<String, String> params = new HashMap<String, String>();
				params.put("loc_type", "0");
				params.put("cate_id", mCurrentCategory == null ? ""
						: mCurrentCategory.id);
				params.put("city_id", cityId == null ? "1" : cityId);
				params.put("sort", sort == null?"0":sort);
				params.put("user_id", MApp.get().getUser() == null || MApp.get().getUser().user == null ? "" : MApp.get().getUser().user.user_id);
				params.put("xpoint", x);
				params.put("ypoint", y);
				params.put("page_index", String.valueOf(pageIndex));
				params.put("page_size", String.valueOf(pageSize));
				return params;
			}

		};
		try {
			D.info(TAG, "body: "+new String(sr.getBody()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		sr.setRetryPolicy(new DefaultRetryPolicy(Constants.SOCKET_TIMEOUT, 
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		executeRequest(sr);
	}

	private void goMerchantDetail(Merchant merchant) {
		Intent goMerchantDetail = new Intent(MApp.get(), MerchantDetailActivity.class);
		goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_NAME, merchant.sl_name);
		goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, merchant.sl_id);
		startActivity(goMerchantDetail);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		if (!isFetchingData && totalItemCount-1 > visibleItemCount
				&& totalItemCount - (firstVisibleItem + visibleItemCount) < 3 && canFetchNext) {
			
			fetchData(false);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Merchant merchant = (Merchant) parent.getAdapter().getItem(position);
		goMerchantDetail(merchant);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		isFetchingData =false;
		D.error(TAG, arg0.toString());
		dismissLoadingDialog();
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		isFetchingData =false;
		D.error(TAG, arg0);
		dismissLoadingDialog();
		if (TextUtils.isEmpty(arg0)){
			return;
		}
		try {
			MerchantListBean bean = new Gson().fromJson(arg0, MerchantListBean.class);
			if (bean.op_code.equals(Constants.SUCCESS)){
				mAdapter.addDatas(bean.sa_list);
				if (bean.sa_list.size() < pageIndex){
					mMerchantList.removeFooterView(mFooter);
					canFetchNext = false;
				}else{
					canFetchNext = true;
					pageIndex ++ ;
				}
				
				if(!mFooter.isShowing()){
					mFooter.show();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void getCategoryFromServer() {
//		showLoadingDialog();
		final String serverUrl = AppConfig.INSTANCE.SERVER
				+ Constants.URL_MERCHANT_CATEGORY;
		StringRequest sr = new StringRequest(Method.POST, serverUrl,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
//						dismissLoadingDialog();
						if (TextUtils.isEmpty(arg0)) {
							return;
						}
						D.error(TAG, serverUrl);
						D.error(TAG, arg0);
						try {
							mCategory = new Gson().fromJson(arg0,
									MerchantCategoryBean.class);
							
							if(mCategory != null) {		//异步滴 重新设一下没错的
								mCateoryAdapter.setDatas(mergeData2String());
							}
							
							if (mCategory != null) {
								saveToCache(Constants.CACHE_MERCHANT_CATEGORY,
										arg0);
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
//						dismissLoadingDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("cate_id", "");
				return params;
			}

		};
		executeRequest(sr);
	}

	private List<String> mergeData2String() {
		List<String> data = new ArrayList<String>();
		data.add(MApp.get().getString(R.string.merchant_sub_category_all));
		if (mCategory != null && mCategory.cates != null) {
			for (MerchantCategory category : mCategory.cates) {
				data.add(category.name);
			}
		}
		return data;

	}

	private void refereshData() {
		mAdapter.clear();
		pageIndex = 1;
		fetchData(true);
		mFooter.setVisibility(View.GONE);
	}
}
