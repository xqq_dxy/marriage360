package com.marriage.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.marriage.R;
import com.marriage.utility.PreferenceHelper;
import com.marriage.utility.ToastHelper;

public class GeneralSettingsFragment extends BaseFragment implements View.OnClickListener, OnCheckedChangeListener {

	private CheckBox cbEnableMobile;
	private CheckBox cbEnablePush;
	
	boolean enableMobile, enablePush;
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		PreferenceHelper.getBoolean(PreferenceHelper.PreferenceKeys.SETTING_ENABLE_MOBILE_NET_DOWNLOAD_IMAGE, true);
		PreferenceHelper.getBoolean(PreferenceHelper.PreferenceKeys.SETTING_ENABLE_PUSH, true);
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_general_settings, container, false);
		view.findViewById(R.id.back_btn).setOnClickListener(this);
		view.findViewById(R.id.clear_cache).setOnClickListener(this);
		cbEnableMobile = (CheckBox)view.findViewById(R.id.enable_mobile_network);
		cbEnablePush = (CheckBox)view.findViewById(R.id.enable_push);
		
		cbEnableMobile.setChecked(enableMobile);
		cbEnablePush.setChecked(enablePush);
		
		cbEnableMobile.setOnCheckedChangeListener(this);
		cbEnablePush.setOnCheckedChangeListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.back_btn:
			getActivity().finish();
			break;
		case R.id.clear_cache:
			clearCache();
			break;
		}
		
	}
	
	private void clearCache() {
		ToastHelper.showShortToast("已清空缓存");
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch(buttonView.getId()) {
		case R.id.enable_mobile_network:
			PreferenceHelper.setBoolean(PreferenceHelper.PreferenceKeys.SETTING_ENABLE_MOBILE_NET_DOWNLOAD_IMAGE, isChecked);
			break;
		case R.id.enable_push:
			PreferenceHelper.setBoolean(PreferenceHelper.PreferenceKeys.SETTING_ENABLE_PUSH, isChecked);
			break;
		}
	}

}
