package com.marriage.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.marriage.R;

/**
 * 我的点赞
 * 
 * @author Martin
 * 
 */
public class MinePraiseFragment extends BaseFragment {

	ListView listView;
	PraiseAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_mine_praise, container,
				false);
		listView = (ListView) view.findViewById(android.R.id.list);
		view.findViewById(R.id.back_btn).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						getActivity().finish();
					}
				});
		
		mAdapter = new PraiseAdapter();
		listView.setAdapter(mAdapter);
		return view;
	}

	static class PraiseAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 20;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.item_list_mine_praise, parent, false);
				holder.userAvatar = (ImageView) convertView.findViewById(R.id.user_avatar);
				holder.image = (ImageView) convertView.findViewById(R.id.image);
				holder.uName = (TextView) convertView.findViewById(R.id.user_name);
				holder.time = (TextView) convertView.findViewById(R.id.user_time);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			return convertView;
		}

		static class ViewHolder {
			TextView uName, time;
			ImageView userAvatar, image;

		}

	}

}
