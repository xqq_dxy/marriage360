package com.marriage.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.marriage.R;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.ToastHelper;

public class FindpwdFragment extends BaseFragment implements OnClickListener{

	private EditText emailInput;
	
	private String email;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_findpwd, container, false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(View v){
		v.findViewById(R.id.back_btn).setOnClickListener(this);
		v.findViewById(R.id.findpwd_submit).setOnClickListener(this);
		
		emailInput = (EditText) v.findViewById(R.id.findpwd_email_input);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;

		case R.id.findpwd_submit:
			goFindpwd();
			break;
		}
	}
	
	private void goFindpwd(){
		if (checkEmailInput()){
			doFindpwd();
		}
	}

	private boolean checkEmailInput(){
		email = emailInput.getText().toString();
		if (TextUtils.isEmpty(email)){
			ToastHelper.showShortToast(R.string.pls_input_your_email_first);
			return false;
		}
		if(!CommonUtils.isEmailAddress(email)){
			ToastHelper.showShortToast(R.string.pls_input_correct_email);
			return false;
		}
		return true;
	}
	
	private void doFindpwd(){
		
	}
}
