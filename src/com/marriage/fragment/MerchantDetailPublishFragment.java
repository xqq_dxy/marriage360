package com.marriage.fragment;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.LoginActivity;
import com.marriage.activity.WeddingDetailActivity;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.MerchantDetail;
import com.marriage.bean.Topics;
import com.marriage.bean.TopicsInfo;
import com.marriage.bean.UserInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.adapter.MerchantPublishListAdapter;
import com.marriage.widget.lib.pla.PLA_AbsListView;
import com.marriage.widget.lib.pla.PLA_AdapterView;
import com.marriage.widget.lib.pla.PLA_AdapterView.OnItemClickListener;
import com.marriage.widget.lib.pla.XListView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MerchantDetailPublishFragment extends BaseFragment implements
		OnClickListener, Listener<String>, ErrorListener,
		com.marriage.widget.lib.pla.PLA_AbsListView.OnScrollListener, OnItemClickListener {

	public static final String TAG = "MerchantDetailPublishFragment";

	private ImageView mMerchantAvatar, mFocusTag;
	private TextView mMerchantName, mMerchantType, mFansNumber, mFocusState,
			mPublishNumber;
	private View mGetThisMerchant;
	private XListView mPublishList;
	private MerchantDetail mDetail;
	private String merchantName;
	private String sa_loc_id;
	private MerchantPublishListAdapter mAdapter;
	private int pageIndex = 1;
	private int pageSize = 10;
	private boolean canNext = true;
	private boolean isLoading = false;

	public static MerchantDetailPublishFragment newInstance(
			MerchantDetail detail) {
		MerchantDetailPublishFragment f = new MerchantDetailPublishFragment();
		f.setMerchantDetail(detail);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle data = getArguments();
		if (data != null) {
			merchantName = data.getString(Constants.BUNDLE_MERCHANT_NAME);
			sa_loc_id = data.getString(Constants.BUNDLE_MERCHANT_LOC_ID);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_merchant_publish,
				container, false);
		initViews(mainView);
		mainView.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				fetchData(true);
			}
		});
		return mainView;
	}

	private void initViews(View v) {
		mMerchantAvatar = (ImageView) v
				.findViewById(R.id.merchant_detail_avatar);
		mFocusTag = (ImageView) v.findViewById(R.id.merchant_detail_focus_tag);

		mMerchantName = (TextView) v.findViewById(R.id.merchant_detail_name);
		mMerchantType = (TextView) v.findViewById(R.id.merchant_detail_type);
		mFansNumber = (TextView) v.findViewById(R.id.merchant_detail_fans);
		mFocusState = (TextView) v
				.findViewById(R.id.merchant_detail_focus_state);
		mPublishNumber = (TextView) v
				.findViewById(R.id.merchant_detail_publish_number);
		mGetThisMerchant = v.findViewById(R.id.get_this_merchant_list);
		mPublishList = (XListView) v.findViewById(R.id.merchant_publish_list);
		mPublishList.setPullLoadEnable(false);
		mPublishList.setPullRefreshEnable(false);
		mPublishList.setOnScrollListener(this);
		mPublishList.setOnItemClickListener(this);
		mAdapter = new MerchantPublishListAdapter(getActivity());
		mPublishList.setAdapter(mAdapter);

		v.findViewById(R.id.merchant_detail_focous).setOnClickListener(this);
		v.findViewById(R.id.merchant_detail_call).setOnClickListener(this);
		v.findViewById(R.id.back_btn).setOnClickListener(this);
		mGetThisMerchant.setOnClickListener(this);

		bindData();
	}

	private void bindData() {
		ImageLoader.getInstance().displayImage(mDetail.img_small,
				mMerchantAvatar, ImageUtil.getCircleDisplayImageOptions());
		mMerchantName.setText(merchantName);
		mMerchantType.setText(mDetail.sa_type_name);
		mFansNumber.setText(mDetail.focused_count);

		if (!TextUtils.isEmpty(mDetail.focus_state)
				&& mDetail.focus_state.toUpperCase(Locale.ENGLISH).equals(
						"TRUE")) {
			mFocusTag.setImageResource(R.drawable.details_added_follow);
			mFocusState.setText(R.string.merchant_detail_focused);
		} else {
			mFocusTag.setImageResource(R.drawable.details_add_follow);
			mFocusState.setText(R.string.merchant_detail_focus);
		}
		
		if ("0".equals(mDetail.is_claim)){
			mGetThisMerchant.setVisibility(View.VISIBLE);
		}else{
			mGetThisMerchant.setVisibility(View.GONE);
		}

	}

	public void setMerchantDetail(MerchantDetail detail) {
		mDetail = detail;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {

		case R.id.merchant_detail_focous:
			goFocus();
			break;
		case R.id.merchant_detail_call:
			goCall();
			break;
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;
		case R.id.get_this_merchant_list:
			goGetThisMerchant();
			break;
		}
	}

	private void goFocus() {
		if (!TextUtils.isEmpty(mDetail.focus_state)
				&& mDetail.focus_state.toUpperCase(Locale.ENGLISH).equals(
						"TRUE")) {
			return;
		}
		if (!MApp.get().isLogin()) {
			Intent goLogin = new Intent(MApp.get(), LoginActivity.class);
			startActivity(goLogin);
			return;
		}
		
		if ("0".equals(mDetail.is_claim)){
			ToastHelper.showShortToast(R.string.not_claim);
			return;
		}
		
		if (TextUtils.isEmpty(mDetail.user_id)){
			ToastHelper.showShortToast(R.string.merchant_data_err);
			return;
		}
		
		// TODO do focus
		doFoucus();
	}
	
	private void doFoucus() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_ADD_FOLLOW;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String response) {
						dismissLoadingDialog();
						BaseApiResult info = null;
						try {
							info = new Gson().fromJson(response,
									BaseApiResult.class);
						} catch (JsonSyntaxException e) {
							e.printStackTrace();
						}
						if (Constants.SUCCESS.equals(info.op_code)) {
							ToastHelper
									.showShortToast(R.string.string_has_followed);
							mFocusTag
									.setImageResource(R.drawable.details_added_follow);
							mFocusState
									.setText(R.string.merchant_detail_focused);
							mDetail.focus_state = "true";
						} else {
							ToastHelper.showShortToast(info.op_info);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						dismissLoadingDialog();
						ToastHelper.showShortToast(R.string.net_err);
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> map = new HashMap<String, String>();
				UserInfo user = MApp.get().getUser();
				map.put("focus_user_id", user.user.user_id);
				map.put("focus_user_name", user.user.user_name);
				map.put("focused_user_id", mDetail.user_id);
				map.put("focused_user_name", merchantName);
				map.put("focus_flag", "1");
				return map;
			}

		};
		try {
			D.info(TAG, "body: "+new String(sr.getBody()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		showLoadingDialog();
		executeRequest(sr);
	}
	
	private void goCall() {
		if (mDetail == null || TextUtils.isEmpty(mDetail.tel)) {
			return;
		}
		String number = mDetail.tel;
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" + number));
		startActivity(intent);
		saveCallClick();
	}

	private void saveCallClick() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CALL_CLICK;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						D.info(TAG, "call click response: " + arg0);
					}
				}, null) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("sl_id", sa_loc_id);
				return params;
			}

		};
		executeRequest(sr);
	}
	
	private void fetchData(boolean isFirst) {
		if (mDetail == null || TextUtils.isEmpty(mDetail.user_id)) {
			return;
		}
		isLoading = true;
		if (isFirst) {
			showLoadingDialog();
		}
		
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPICS;
		StringRequest sr = new StringRequest(Method.POST, url, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("fun_page", "3");
				params.put("user_id", mDetail.user_id/*""*/);
				params.put("page_index", String.valueOf(pageIndex));
				params.put("page_size", String.valueOf(pageSize));
				params.put("city_id", "1");
				params.put("cate_id", "");
				params.put("sort", "");
				return params;
			}

		};

		executeRequest(sr);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		isLoading = false;
		dismissLoadingDialog();
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		isLoading = false;
		dismissLoadingDialog();
		TopicsInfo tops = null;
		try {
			tops = new Gson().fromJson(arg0, TopicsInfo.class);
		} catch (Exception e) {
		}

		if (Constants.SUCCESS.equals(tops.op_code)) {
			mPublishNumber.setText(MApp.get().getString(
					R.string.merchant_publish_number,
					new Object[] { tops.topic_list_total }));
			if (tops.topic_list != null) {
				mAdapter.addDatas(tops.topic_list);
				if (tops.topic_list.size() == pageSize){
					pageIndex++;
				}else {
					canNext = false;
				}
			} 
		}
	}

	private void goGetThisMerchant() {
		if (!MApp.get().isLogin()) {
			Intent goLogin = new Intent(MApp.get(), LoginActivity.class);
			startActivity(goLogin);
			return;
		}
		showLoadingDialog();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CLAIM_MERCHANT;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						D.info(TAG, "CLAIM merchant: " + arg0);
						if (TextUtils.isEmpty(arg0)) {
							showAlert(MApp.get().getString(R.string.net_err));
							return;
						}
						try {
							BaseApiResult result = new Gson().fromJson(arg0,
									BaseApiResult.class);
							showAlert(result.op_info);
						} catch (Exception e) {
							// TODO: handle exception
							showAlert(MApp.get().getString(R.string.net_err));
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						showAlert(MApp.get().getString(R.string.net_err));
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("user_id", MApp.get().getUser().user.user_id);
				params.put("sl_id", sa_loc_id);
				return params;
			}

		};
		executeRequest(sr);
	}
	
	@Override
	public void onScrollStateChanged(PLA_AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(PLA_AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		if (!isLoading && canNext && totalItemCount > visibleItemCount
				&& totalItemCount - (firstVisibleItem + visibleItemCount) < 3) {
			fetchData(false);
		}

	}

	@Override
	public void onItemClick(PLA_AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Topics data = (Topics) parent.getAdapter().getItem(position);
		Intent intent = new Intent(getActivity(), WeddingDetailActivity.class);
		intent.putExtra("topic_id", data.topic_id);
		startActivity(intent);
	}

}
