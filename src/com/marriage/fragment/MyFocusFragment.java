package com.marriage.fragment;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.MerchantDetailActivity;
import com.marriage.activity.NormalUserInfoActivity;
import com.marriage.bean.Focus;
import com.marriage.bean.FocusBean;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.widget.adapter.FocusAdapter;

public class MyFocusFragment extends BaseFragment implements OnScrollListener,
		Listener<String>, ErrorListener, OnClickListener, OnItemClickListener {
	public static final String TAG = "MineFansFragment";

	private ListView mFansListView;

	private int pageSize = 15;
	private int pageIndex = 1;

	private boolean isLoading = false;
	private boolean canNext = true;

	private FocusAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAdapter = new FocusAdapter(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View fansRoot = inflater.inflate(R.layout.fragment_mine_focus,
				container, false);
		mFansListView = (ListView) fansRoot.findViewById(android.R.id.list);
		fansRoot.findViewById(R.id.back_btn).setOnClickListener(this);

		mFansListView.setAdapter(mAdapter);
		mFansListView.setOnScrollListener(this);
		mFansListView.setOnItemClickListener(this);
		fansRoot.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				fetchData(true);
			}
		});

		return fansRoot;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			getActivity().finish();
			break;
		}
	}

	private void fetchData(boolean isFirst) {
		isLoading = true;
		if (isFirst) {
			showLoadingDialog();
		}
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_FOCUS;
		StringRequest sr = new StringRequest(Method.POST, url, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("user_id", MApp.get().getUser().user.user_id);
				params.put("page_index", String.valueOf(pageIndex));
				params.put("page_size", String.valueOf(pageSize));
				return params;
			}

		};
		try {
			D.info(TAG, new String(sr.getBody()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		executeRequest(sr);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		isLoading = false;
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		D.info(TAG, "focus: " + arg0);
		dismissLoadingDialog();
		try {
			FocusBean bean = new Gson().fromJson(arg0, FocusBean.class);
			if (Constants.SUCCESS.equals(bean.op_code)
					&& bean.fans_list != null) {
				mAdapter.addDatas(bean.fans_list);
				if (bean.fans_list.size() == pageSize) {
					canNext = true;
				} else {
					canNext = false;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		isLoading = false;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		switch (scrollState) {
		case OnScrollListener.SCROLL_STATE_IDLE:
			if (view.getLastVisiblePosition() == (view.getCount() - 2)
					&& !isLoading && canNext) {
				D.info(TAG, "==============next page==============");
				fetchData(false);
			}
			break;
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		D.info(TAG, "onItemClick");
		Focus data = (Focus) parent.getAdapter().getItem(position);
		if (data == null) {
			return;
		}
		if ("TRUE".equals(data.is_sa.toUpperCase(Locale.ENGLISH))) {
			goMerchantDetail(data.sl_id, data.sl_name);
		} else {
			goNormalUser(data.focused_user_id,data.focused_user_name,data.user_head_img,data.focused_user_city_name);
		}
	}

	private void goMerchantDetail(String slId, String slName) {
		Intent goMerchantDetail = new Intent(MApp.get(),
				MerchantDetailActivity.class);
		goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_NAME, slName);
		goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, slId);
		startActivity(goMerchantDetail);
	}

	private void goNormalUser(String userId,String userName,String img,String cityName) {
		Intent goNormalUser = new Intent(MApp.get(),NormalUserInfoActivity.class);
		goNormalUser.putExtra(Constants.BUNDLE_USER_ID, userId);
		goNormalUser.putExtra(Constants.BUNDLE_USER_AVATAR, img);
		goNormalUser.putExtra(Constants.BUNDLE_USER_NAME, userName);
		goNormalUser.putExtra(Constants.BUNDLE_USER_CITY, cityName);
		startActivity(goNormalUser);
	}

}
