package com.marriage.fragment;

import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.AboutActivity;
import com.marriage.activity.BaseActivity;
import com.marriage.activity.FeedbackActivity;
import com.marriage.activity.GeneralSettingsActivity;
import com.marriage.activity.MainActivity;
import com.marriage.activity.MerchantJoinActivity;
import com.marriage.activity.MineFansActivity;
import com.marriage.activity.MyFocusActivity;
import com.marriage.activity.MyPublishActivity;
import com.marriage.activity.SearchMerchantActivity;
import com.marriage.bean.MerchantInfo;
import com.marriage.bean.UserInfo;
import com.marriage.dialog.ShareDialog;
import com.marriage.dialog.ThreeButtonDialog;
import com.marriage.dialog.ThreeButtonDialog.OnButtonClickListener;
import com.marriage.update.UpdateService;
import com.marriage.utility.AppConfig;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.ShareUtils;
import com.marriage.widget.MerchantPersonalInfoView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MineFragment extends BaseFragment implements OnClickListener,
		OnButtonClickListener {

	public static final String TAG = "MineFragment";

	private MerchantPersonalInfoView merchantInfoView;
	private LinearLayout visitorsLayout;
	private ImageView avatar;
	private TextView nameView, typeView, cityView;
	private View mVisitorsGroup;
	private ShareDialog mShareDialog;
	private ThreeButtonDialog mMerchantJoinDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_personal, container,
				false);

		initViews(mainView);
		bindData();
		return mainView;
	}

	private void initViews(View v) {
		merchantInfoView = (MerchantPersonalInfoView) v
				.findViewById(R.id.merchant_personal_info);
		visitorsLayout = (LinearLayout) v
				.findViewById(R.id.personal_visitors_layout);
		visitorsLayout.setGravity(Gravity.CENTER_VERTICAL);
		avatar = (ImageView) v.findViewById(R.id.personal_avatar);
		nameView = (TextView) v.findViewById(R.id.personal_name);
		typeView = (TextView) v.findViewById(R.id.personal_type);
		cityView = (TextView) v.findViewById(R.id.personal_city);

		v.findViewById(R.id.personal_my_focus).setOnClickListener(this);
		v.findViewById(R.id.personal_my_publish).setOnClickListener(this);
		mVisitorsGroup = v.findViewById(R.id.personal_my_visitors);
		mVisitorsGroup.setOnClickListener(this);
		v.findViewById(R.id.personal_my_good).setOnClickListener(this);
		v.findViewById(R.id.personal_common_setting).setOnClickListener(this);
		v.findViewById(R.id.personal_wedding_card).setOnClickListener(this);
		v.findViewById(R.id.personal_merchant_register_get).setOnClickListener(
				this);
		v.findViewById(R.id.personal_about_us).setOnClickListener(this);
		v.findViewById(R.id.personal_feedback).setOnClickListener(this);
		v.findViewById(R.id.personal_share).setOnClickListener(this);
		v.findViewById(R.id.personal_logout).setOnClickListener(this);
		v.findViewById(R.id.personal_updatecheck).setOnClickListener(this);
	}

	private void bindData() {
		UserInfo user = MApp.get().getUser();
		if (user.user_type.equals("1")) {
			merchantInfoView.setVisibility(View.VISIBLE);
			mVisitorsGroup.setVisibility(View.VISIBLE);
			// typeView.setText(user.supplier_account.)
			getMerchantInfo();
		} else {
			merchantInfoView.setVisibility(View.GONE);
			mVisitorsGroup.setVisibility(View.GONE);
			if (!TextUtils.isEmpty(user.left_days)
					&& !user.left_days.equals("-1")) {
				typeView.setText(getResources().getString(
						R.string.my_count_down, user.left_days));
			} else {
				typeView.setText("");
			}
		}
		ImageLoader.getInstance().displayImage(user.head_pics.small, avatar,
				ImageUtil.getCircleDisplayImageOptions());
		nameView.setText(user.user.user_name);
		cityView.setText(user.user.city_name);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.personal_my_focus:
			goFocus();
			break;
		case R.id.personal_my_publish:
			goPublish();
			break;
		case R.id.personal_my_visitors:
			goVisters();
			break;
		case R.id.personal_my_good:
			goGoods();
			break;
		case R.id.personal_common_setting:
			goCommonSettings();
			break;
		case R.id.personal_wedding_card:
			goWeddingCard();
			break;
		case R.id.personal_merchant_register_get:
			goRegisterOrGet();
			break;
		case R.id.personal_about_us:
			goAboutUs();
			break;
		case R.id.personal_feedback:
			goFeedback();
			break;
		case R.id.personal_share:
			goShare();
			break;
		case R.id.personal_logout:
			logout();
			break;

		case R.id.share_sms:
			ShareUtils.shareToSms(getActivity(), Constants.SHARE_SMS_CONTENT,
					"");
			mShareDialog.dismissAllowingStateLoss();
			break;
		case R.id.share_mail:
			ShareUtils.shareToEmail(getActivity(), "",
					Constants.SHARE_EMAIL_SUBJECT,
					Constants.SHARE_EMAIL_CONTENT);
			mShareDialog.dismissAllowingStateLoss();
			break;
		case R.id.share_weixin:
			ShareUtils.shareToWeixin(getActivity(), Constants.SHARE_CONTENT,
					"", "", Constants.SHARE_TITLE, false);
			mShareDialog.dismissAllowingStateLoss();
			break;
		case R.id.share_weixin_cycle:
			ShareUtils.shareToWeixin(getActivity(), Constants.SHARE_CONTENT,
					"", "", Constants.SHARE_TITLE, true);
			mShareDialog.dismissAllowingStateLoss();
			break;
		case R.id.share_weibo:
			ShareUtils.shareToWeibo(getActivity(), Constants.SHARE_TITLE,
					Constants.SHARE_CONTENT);
			mShareDialog.dismissAllowingStateLoss();
			break;
		case R.id.share_qq:
			ShareUtils.shareToQQ(getActivity(), Constants.SHARE_TITLE,
					Constants.SHARE_CONTENT);
			mShareDialog.dismissAllowingStateLoss();
			break;
		case R.id.share_cancle:
			if (mShareDialog != null) {
				mShareDialog.dismissAllowingStateLoss();
			}
			break;
		case R.id.personal_updatecheck:
			checkUpdate();
			break;
		}

	}

	private void checkUpdate(){
		((BaseActivity)getActivity()).showLoading();
		registerNoUpdateReceiver();
		Intent startUpdateCheck = new Intent(MApp.get(), UpdateService.class);
		getActivity().startService(startUpdateCheck);
	}
	
	private void registerNoUpdateReceiver(){
		IntentFilter filter = new IntentFilter(Constants.ACTION_NO_UPDATE);
		getActivity().registerReceiver(noUpdateReceiver, filter);
	}
	
	private void unRegisterNoUpdateReceiver(){
		try {
			getActivity().unregisterReceiver(noUpdateReceiver);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		unRegisterNoUpdateReceiver();
	}
	
	@SuppressWarnings("unused")
	private void goFansList() {
		Intent intent = new Intent(getActivity(), MineFansActivity.class);
		startActivity(intent);
	}

	private void goFocus() {
		Intent intent = new Intent(getActivity(), MyFocusActivity.class);
		startActivity(intent);
	}

	private void goPublish() {
		Intent goMyPublish = new Intent(getActivity(), MyPublishActivity.class);
		goMyPublish.putExtra(Constants.BUNDLE_LIST_TYPE,
				MyPublishFragment.TYPE_MY_PUBLISH);
		startActivity(goMyPublish);
	}

	private void goVisters() {
		Intent intent = new Intent(getActivity(), MineFansActivity.class);
		startActivity(intent);
	}

	private void goGoods() {
		Intent goGoods = new Intent(getActivity(), MyPublishActivity.class);
		goGoods.putExtra(Constants.BUNDLE_LIST_TYPE,
				MyPublishFragment.TYPE_MY_PRAISE);
		startActivity(goGoods);
	}

	private void goCommonSettings() {
		Intent intent = new Intent(getActivity(), GeneralSettingsActivity.class);
		startActivity(intent);
	}

	private void goWeddingCard() {
		((MainActivity)getActivity()).setCurrentTab(2);
	}

	private void goRegisterOrGet() {
		if (mMerchantJoinDialog == null) {
			mMerchantJoinDialog = ThreeButtonDialog.newInstance(
					R.string.merchant_direct_join, R.string.search_merchant,
					R.string.cancel, this);
		}
		mMerchantJoinDialog.show(getFragmentManager(), "MERCHANT_JOIN");
	}

	private void goAboutUs() {
		Intent intent = new Intent(getActivity(), AboutActivity.class);
		startActivity(intent);
	}

	private void goFeedback() {
		Intent intent = new Intent(getActivity(), FeedbackActivity.class);
		startActivity(intent);
	}

	private void logout() {
		MApp.get().logout();
		((MainActivity) getActivity()).onLogout();
	}

	private void goShare() {
		if (mShareDialog == null) {
			mShareDialog = new ShareDialog(this);
		}
		mShareDialog.show(getFragmentManager(), "SHARE_DIALOG");
	}

	private void getMerchantInfo() {
		showLoadingDialog();
		String url = AppConfig.INSTANCE.SERVER
				+ Constants.URL_MERCHANT_PERSONAL_INFO;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
						D.info(TAG, "response: " + arg0);
						try {
							MerchantInfo info = new Gson().fromJson(arg0,
									MerchantInfo.class);
							if (info != null
									&& Constants.SUCCESS.equals(info.op_code)) {
								bindMerchantData(info);
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						dismissLoadingDialog();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("sa_id", MApp.get().getUser().supplier_account.sa_id);
				return params;
			}

		};
		executeRequest(sr);
	}

	private void bindMerchantData(MerchantInfo info) {
		merchantInfoView.setCommentCount(info.sa_personal_info.dp_count);
		merchantInfoView.setPraiseCount(info.sa_personal_info.faved_count);
		merchantInfoView.setFansCount(info.sa_personal_info.focused_count);
		merchantInfoView.setCallCount(info.sa_personal_info.phoned_count);

		typeView.setText(info.sa_personal_info.sa_type_name);
		int visitorCount = 0;
		if (info.sa_user_part_visits != null) {
			visitorCount = info.sa_user_part_visits.size();
		}
		int showCount = visitorCount;
		if (showCount > 5) {
			showCount = 5;
		}
		for (int i = 0; i < showCount; i++) {
			String img = info.sa_user_part_visits.get(i).visitor_head_img;
			visitorsLayout.addView(createVisitor(img));
		}
		if (showCount < visitorCount) {
			visitorsLayout.addView(createVisitorCount(visitorCount));
		}
	}

	private ImageView createVisitor(String imgUrl) {
		ImageView icon = new ImageView(MApp.get());
		LinearLayout.LayoutParams iconLayoutParams = new LinearLayout.LayoutParams(
				CommonUtils.dip2Px(MApp.get(), 28), CommonUtils.dip2Px(
						MApp.get(), 28));
		iconLayoutParams.leftMargin = CommonUtils.dip2Px(MApp.get(), 14);
		icon.setLayoutParams(iconLayoutParams);
		ImageLoader.getInstance().displayImage(imgUrl, icon,
				ImageUtil.getCircleDisplayImageOptions());
		return icon;
	}

	private View createVisitorCount(int count) {
		LinearLayout v = new LinearLayout(MApp.get());
		LinearLayout.LayoutParams vLayoutParams = new LinearLayout.LayoutParams(
				0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
		v.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		v.setPadding(0, 0, CommonUtils.dip2Px(MApp.get(), 10), 0);
		v.setLayoutParams(vLayoutParams);

		TextView countView = new TextView(MApp.get());
		LinearLayout.LayoutParams countViewLayoutParams = new LinearLayout.LayoutParams(
				CommonUtils.dip2Px(MApp.get(), 24), CommonUtils.dip2Px(
						MApp.get(), 24));
		countView.setLayoutParams(countViewLayoutParams);
		countView.setText(String.valueOf(count));
		countView.setTextColor(Color.WHITE);
		countView.setTextSize(16);
		countView.setGravity(Gravity.CENTER);
		v.addView(countView);

		return v;
	}

	@Override
	public void onClick(int position, View v) {
		// TODO Auto-generated method stub
		switch (position) {
		case 0:
			Intent goJoin = new Intent(MApp.get(), MerchantJoinActivity.class);
			startActivity(goJoin);
			mMerchantJoinDialog.dismiss();
			break;

		case 1:
			Intent goSearch = new Intent(MApp.get(),
					SearchMerchantActivity.class);
			startActivity(goSearch);
			mMerchantJoinDialog.dismiss();
			break;
		case 2:
			mMerchantJoinDialog.dismiss();
			break;
		}
	}
	
	private void showNoUpdateDialog(){
		showAlert(MApp.get().getString(R.string.no_update));
	}
	
	private BroadcastReceiver noUpdateReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (getActivity() != null){
				((BaseActivity)getActivity()).endLoading();
			}
			if (intent == null){
				return;
			}
			String action = intent.getAction();
			if (Constants.ACTION_NO_UPDATE.equals(action)){
				showNoUpdateDialog();
			}
		}
	};

}
