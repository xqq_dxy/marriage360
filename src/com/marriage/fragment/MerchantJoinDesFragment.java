package com.marriage.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.LoginActivity;
import com.marriage.activity.MainActivity;
import com.marriage.activity.MerchantJoinActivity;
import com.marriage.activity.SearchMerchantActivity;
import com.marriage.utility.Constants;
import com.marriage.utility.D;

public class MerchantJoinDesFragment extends BaseFragment implements OnClickListener{

	public static final String TAG = "MerchantJoinDesFragment";
	
	private TextView joinGuide;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_merchant_join_des, container, false);
		initViews(mainView);
		return mainView;
	}
	
	private void initViews(View v){
		v.findViewById(R.id.merchant_join).setOnClickListener(this);
		v.findViewById(R.id.search_merchant).setOnClickListener(this);
		v.findViewById(R.id.back_btn).setOnClickListener(this);
		
		joinGuide = (TextView) v.findViewById(R.id.merchant_join_guide);
		
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		String str1 = getString(R.string.join_step_2);
		String str2 = getString(R.string.join_step_22);
		String all = str1 + str2;
		SpannableString ss = new SpannableString(all);
		int start = str1.length();
		int end = all.length();
		ss.setSpan(new ClickableSpan() {
			
			@Override
			public void onClick(View widget) {
				// TODO Auto-generated method stub
				D.error(TAG, "ClickableSpan onclick");
				goCreateMerchant();
			}
		}, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		ss.setSpan(new ForegroundColorSpan(0xffeb63c4), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		joinGuide.setText(ss);
		joinGuide.setMovementMethod(LinkMovementMethod.getInstance());
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.merchant_join:
			goMerchantJoin();
			break;

		case R.id.search_merchant:
			goSearchMerchant();
			break;
		case R.id.back_btn:
			getActivity().onBackPressed();
			break;
		}
	}
	
	private void goCreateMerchant(){
		goMerchantJoin();
	}

	private void goMerchantJoin(){
		if (MApp.get().isLogin()){
			Intent goJoin = new Intent(MApp.get(), MerchantJoinActivity.class);
			startActivity(goJoin);
		}else{
			Intent goLogin = new Intent(MApp.get(), LoginActivity.class);
			goLogin.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(goLogin);
		}
	}
	
	private void goSearchMerchant(){
		Intent goMerchantList = new Intent(MApp.get(), SearchMerchantActivity.class);
		startActivity(goMerchantList);
	}
}
