package com.marriage.fragment;

import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.WeddingDetailActivity;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.Topics;
import com.marriage.bean.TopicsInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.LoadingView;
import com.marriage.widget.adapter.MerchantPublishListAdapter;
import com.marriage.widget.lib.pla.PLA_AbsListView;
import com.marriage.widget.lib.pla.PLA_AbsListView.OnScrollListener;
import com.marriage.widget.lib.pla.PLA_AdapterView;
import com.marriage.widget.lib.pla.PLA_AdapterView.OnItemClickListener;
import com.marriage.widget.lib.pla.XListView;

public class MyPublishFragment extends BaseFragment implements
		OnItemClickListener, OnScrollListener, Listener<String>, ErrorListener, OnClickListener{

	public static final String TAG = "MyPublishFragment";

	private XListView mListView;

	private LoadingView mFooter;
	
	private MerchantPublishListAdapter mAdapter;

	private int pageIndex = 1;
	private int pageSize = 10;

	private boolean isLoading = false;
	private boolean canNext = true;
	
	private String titleString;
	private String type = "3";
	
	public static final String TYPE_MY_PUBLISH = "3";
	public static final String TYPE_MY_PRAISE = "2";
	public static final String TYPE_PRAISE = "4";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		registerCancelReceiver();
		Bundle args = getArguments();
		if (args != null){
			type = args.getString(Constants.BUNDLE_LIST_TYPE);
		}
		if (TYPE_MY_PRAISE.equals(type)){
			titleString = getResources().getString(R.string.title_my_good);
		} else if (TYPE_MY_PUBLISH.equals(type)){
			titleString = getResources().getString(R.string.title_my_publish);
		} else if (TYPE_PRAISE.equals(type)){
			titleString = getResources().getString(R.string.title_good);
		}
		mAdapter = new MerchantPublishListAdapter(getActivity());
		mAdapter.setOnClickListener(this);
		mAdapter.setOnCreateContextMenuListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View mainView = inflater.inflate(R.layout.fragment_mypublish,
				container, false);
		initViews(mainView);
		fetchData();
		return mainView;
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterCancelReceiver();
	}

	private void initViews(View v) {
		mFooter = new LoadingView(getActivity());
		mFooter.dismiss();
		mListView = (XListView) v.findViewById(R.id.my_publish_list);
		mListView.addFooterView(mFooter);
		mListView.setPullLoadEnable(false);
		mListView.setPullRefreshEnable(false);
		mListView.setOnScrollListener(this);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(mAdapter);
		TextView title = (TextView) v.findViewById(R.id.title);
		title.setText(titleString);
		v.findViewById(R.id.back_btn).setOnClickListener(this);
	}

	private void fetchData() {
		isLoading = true;
		showLoadingDialog();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPICS;
		StringRequest sr = new StringRequest(Method.POST, url, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("fun_page", type);
				params.put("user_id", MApp.get().getUser().user.user_id);
				params.put("city_id", "1");
				params.put("cate_id", "0");
				params.put("sort", "3");
				params.put("page_index", String.valueOf(pageIndex));
				params.put("page_size", String.valueOf(pageSize));
				return params;
			}
			
		};
		
		try {
			D.info(TAG, "postParams: "+new String(sr.getBody()));
		} catch (AuthFailureError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		executeRequest(sr);
	}

	private void popBack(){
		getActivity().onBackPressed();
	}
	
	@Override
	public void onScrollStateChanged(PLA_AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onScroll(PLA_AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		if (totalItemCount > 0 && view.getLastVisiblePosition() == (view.getCount() - 3) && !isLoading && canNext) {
			D.info(TAG, "==============next page==============");
			fetchData();
		}
	}

	@Override
	public void onItemClick(PLA_AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Topics data = (Topics) parent.getAdapter().getItem(position);
		Intent intent = new Intent(getActivity(), WeddingDetailActivity.class);
		intent.putExtra("topic_id", data.topic_id);
		startActivity(intent);
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		isLoading = false;
	}

	@Override
	public void onResponse(String arg0) {
		// TODO Auto-generated method stub
		dismissLoadingDialog();
		isLoading = false;
		D.info(TAG, "response: "+arg0);
		try {
			TopicsInfo topicsInfos = new Gson().fromJson(arg0, TopicsInfo.class);
			if (Constants.SUCCESS.equals(topicsInfos.op_code)) {
				
				if (topicsInfos.topic_list != null) {
					mAdapter.addDatas(topicsInfos.topic_list);
					D.info(TAG, "fetch data size: "+topicsInfos.topic_list.size());
					if (topicsInfos.topic_list.size() == pageSize){
						pageIndex++;
						if (!mFooter.isShowing()){
							mFooter.show();
						}
					}else {
						canNext = false;
						mListView.removeFooterView(mFooter);
					}
				} 
				
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			popBack();
			break;
			
		default: {
			Integer position = (Integer) v.getTag(v.getId());
			if (position != null) {
				Topics data = (Topics) mAdapter.getItem(position);
				Intent intent = new Intent(getActivity(),
						WeddingDetailActivity.class);
				intent.putExtra("topic_id", data.topic_id);
				startActivity(intent);
			}
		}
			break;
		}
	}

//	@Override
//	public boolean onItemLongClick(final PLA_AdapterView<?> parent, View view,
//			final int position, long id) {
//		new AlertDialog.Builder(parent.getContext())
//				.setTitle("对Item进行操作")
//				.setItems(new String[] { "删除" },
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {}
//						}).setNegativeButton("取消", null).show();
//		return true;
//	}
	
	private void deleteItem(final String topic_id, final int position) {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_DEL_TOPIC;

		StringRequest request = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String response) {
						dismissLoadingDialog();
						BaseApiResult topicsInfos = new Gson().fromJson(
								response, BaseApiResult.class);
						if(topicsInfos != null && Constants.SUCCESS.equals(topicsInfos.op_code)) {
							mAdapter.removeData(position);
						} else {
							ToastHelper.showShortToast("删除失败.");
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						dismissLoadingDialog();
						ToastHelper.showShortToast("删除失败");
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("topic_id", topic_id);
				return params;
			}

		};
		executeRequest(request);
		showLoadingDialog();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (TYPE_MY_PUBLISH.equals(type)){
			Integer position = (Integer)v.getTag(v.getId());
			if(position != null) {
				menu.add(0, position, Menu.NONE, "删除");
			}
		}
		
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (TYPE_MY_PUBLISH.equals(type)){
			int position = item.getItemId();
			Topics topic = (Topics)mAdapter.getItem(position);
			if(topic != null) {
				deleteItem(topic.topic_id, position);
			}
		}
		return super.onContextItemSelected(item);
	}
	
	private void tryDeleteTopic(String topicId){
		if (mAdapter != null){
			mAdapter.onTopicDelete(topicId);
		}
	}
	
	private void registerCancelReceiver(){
		if (getActivity() != null){
			IntentFilter filter = new IntentFilter(Constants.ACTION_CANCEL_PRAISE);
			getActivity().registerReceiver(cancelPraiseReceiver, filter);
		}
		
	}
	
	private void unregisterCancelReceiver(){
		try {
			if (getActivity() != null){
				getActivity().unregisterReceiver(cancelPraiseReceiver);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private BroadcastReceiver cancelPraiseReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (intent == null){
				return;
			}
			String action = intent.getAction();
			if (action == null){
				return;
			}
			if (!action.equals(Constants.ACTION_CANCEL_PRAISE)){
				return;
			}
			String topicId = intent.getStringExtra(Constants.BUNDLE_TOPIC_ID);
			tryDeleteTopic(topicId);
		}
	};

}
