package com.marriage.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.R;
import com.marriage.activity.WeddingDetailActivity;
import com.marriage.bean.MerCommentInfo;
import com.marriage.bean.MerchantComment;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImageUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;



public class MineCommentFragment extends BaseFragment implements Listener<String>, ErrorListener, OnScrollListener {

	ListView listView;
	CommentAdapter mAdapter;
	String sa_loc_id;
	CommentListRequest request;
	List<MerchantComment> mCommentList = new ArrayList<MerchantComment>();
	static final int PAGE_SIZE = 30;
	int page_index = 1;
	boolean hasMore = true;
	static DisplayImageOptions options = ImageUtil.getCircleDisplayImageOptions();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle data = getArguments();
		if(data != null) {
			sa_loc_id = data.getString(Constants.BUNDLE_MERCHANT_LOC_ID);
		}
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_MERCHANT_COMMENT_LIST;
		request = new CommentListRequest(url, this, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_mine_comment, container,
				false);
		listView = (ListView) view.findViewById(android.R.id.list);
		view.findViewById(R.id.back_btn).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						getActivity().finish();
					}
				});
		
		mAdapter = new CommentAdapter();
		listView.setAdapter(mAdapter);
		listView.setEmptyView(view.findViewById(android.R.id.empty));
		listView.setOnScrollListener(this);
		container.post(new Runnable() {
			@Override
			public void run() {
				showLoadingDialog();
				getRequestQueue().add(request);
			}
		});
		return view;
	}

	class CommentAdapter extends BaseAdapter {
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mCommentList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.item_list_mine_praise, parent, false);
				holder.userAvatar = (ImageView) convertView.findViewById(R.id.user_avatar);
				holder.image = (ImageView) convertView.findViewById(R.id.image);
				holder.uName = (TextView) convertView.findViewById(R.id.user_name);
				holder.time = (TextView) convertView.findViewById(R.id.user_time);
				holder.content = (TextView) convertView.findViewById(R.id.content);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			MerchantComment comment = mCommentList.get(position);
			
			holder.uName.setText(comment.user_name);
			/*if(comment.isSA) {
				holder.uName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.v_icon_small, 0);
			} else {
				holder.uName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			}*/
			holder.time.setText(WeddingDetailActivity.dateDiff(Long.valueOf(comment.time_diff)));
			holder.content.setText(comment.content);
			ImageLoader.getInstance().displayImage(comment.head_img, holder.userAvatar, options);
			
			return convertView;
		}

		class ViewHolder {
			TextView uName, time, content;
			ImageView userAvatar, image;
		}
	}
	
	class CommentListRequest extends StringRequest {
		
		public CommentListRequest(String url, Listener<String> listener, ErrorListener errorListener) {
			super(Method.POST, url, listener, errorListener);
		}

		@Override
		protected Map<String, String> getParams() throws AuthFailureError {
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("user_id", "");
			params.put("sa_loc_id", sa_loc_id == null ? "" : sa_loc_id);
			params.put("page_index", String.valueOf(page_index));
			params.put("page_size", String.valueOf(PAGE_SIZE));
			return params;
		}
		
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		dismissLoadingDialog();
	}

	@Override
	public void onResponse(String response) {
		D.debug("MARTIN", response);
		dismissLoadingDialog();
		MerCommentInfo info = null;
		try {
			info = new Gson().fromJson(response, MerCommentInfo.class);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		}
		if(info != null && Constants.SUCCESS.equals(info.op_code)) {
			page_index++;
			if(info.sa_lp_ls != null) {
				mCommentList.addAll(info.sa_lp_ls);
				if(info.sa_lp_ls.size() < PAGE_SIZE){
					hasMore = false;
				}
				mAdapter.notifyDataSetChanged();
			}
		}
		
		
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		switch (scrollState) {
		case OnScrollListener.SCROLL_STATE_IDLE:
			if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
				if(hasMore) {
					showLoadingDialog();
					getRequestQueue().add(request);
				}
			}
			break;
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
	}

}
