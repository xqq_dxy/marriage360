package com.marriage.delegate;

public class StepMakeCardFactory {

	public static StepMakeCard newStepMakeCard(int step) {
		switch (step) {
		case StepMakeCard.MAKE_CARD_STEP_INPUT_CODE:
			return new InputCodeMakeCardImpl();
		case StepMakeCard.MAKE_CARD_STEP_INPUT_INFO:
			return new InputInfoMakeCardImpl();
		case StepMakeCard.MAKE_CARD_STEP_CHOICE_THEME:
			return new ChoiceThemeMakeCardImpl();
		case StepMakeCard.MAKE_CARD_STEP_CHOICE_MUSIC:
			return new ChoiceMusicMakeCardImpl();
		case StepMakeCard.MAKE_CARD_STEP_PREVIEW_CARD:
			return new PreviewMakeCardImpl();
		}
		return null;
	}

}
