package com.marriage.delegate;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.MainActivity;
import com.marriage.activity.WeddingWebViewActivity;
import com.marriage.bean.PreviewResultInfo;
import com.marriage.bean.PreviewReuslt;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.ToastHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PreviewMakeCardImpl extends StepMakeCard implements View.OnClickListener {

	ListView mList;
	WeddingCardAdapter mAdapter;
	PreviewReuslt preCard;
	
	@Override
	public void onCreate() {
		setContentView(R.layout.fragment_e_card_list);
		mList = (ListView) findViewById(android.R.id.list);
		View btn = findViewById(R.id.back_btn);
		btn.setVisibility(View.VISIBLE);
		btn.setOnClickListener(this);
		btn = findViewById(R.id.right_btn);
		btn.setVisibility(View.VISIBLE);
		btn.setOnClickListener(this);
		mAdapter = new WeddingCardAdapter(this);
		mList.setAdapter(mAdapter);
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CARD_PREVIEW;
		StringRequest request = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String response) {
						endLoading();
						PreviewResultInfo info = null;
						try {
							info = new Gson().fromJson(response, PreviewResultInfo.class);
						} catch (JsonSyntaxException e) {
							e.printStackTrace();
							ToastHelper.showShortToast("错误");
							return;
						}
						if(Constants.SUCCESS.equals(info.op_code)) {
							mAdapter.setData(preCard = info.xt_info);
							mAdapter.notifyDataSetChanged();
							addFooterView();
						} else if(!TextUtils.isEmpty(info.op_info)) {
							ToastHelper.showShortToast(info.op_info);
						} else {
							ToastHelper.showShortToast("错误");
						}
					}
					
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						endLoading();
						ToastHelper.showShortToast("网络异常");
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("xt_id", mXitieId);
				return params;
			}
		};
		
		requestLoading();
		MApp.get().getRequestQueue().add(request);
	}
	
	private void addFooterView() {
		LinearLayout ll = new LinearLayout(mContext);
		AbsListView.LayoutParams params = new AbsListView.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		ll.setLayoutParams(params);
		Button btn = new Button(mContext);
		btn.setId(R.id.btn_start_make);
		btn.setBackgroundResource(R.drawable.btn);
		btn.setTextColor(Color.WHITE);
		btn.setText(R.string.string_card_preview);
		LinearLayout.LayoutParams bparams = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, mContext.getResources()
						.getDimensionPixelSize(R.dimen.btn_height));
		bparams.topMargin = mContext.getResources().getDimensionPixelSize(
				R.dimen.btn_margitn_top);
		bparams.leftMargin = mContext.getResources().getDimensionPixelSize(
				R.dimen.btn_margitn_left);
		bparams.rightMargin = mContext.getResources().getDimensionPixelSize(
				R.dimen.btn_margitn_right);
		btn.setLayoutParams(bparams);
		btn.setOnClickListener(this);
		ll.addView(btn);
		mList.addFooterView(ll);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.back_btn:
			mContext.finish();
			break;
		case R.id.right_btn:
			Intent intent = new Intent(getContext(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(intent);
			break;
		case R.id.btn_start_make:		//预览
			if (preCard != null && !TextUtils.isEmpty(preCard.url)) {
				Intent goTemplate = new Intent(MApp.get(),
						WeddingWebViewActivity.class);
				goTemplate.putExtra(Constants.BUNDLE_WEBVIEW_TITLE, MApp.get()
						.getString(R.string.string_card_preview));
				goTemplate.putExtra(Constants.BUNDLE_WEBVIEW_URL, preCard.url);
				getContext().startActivity(goTemplate);
			}
			break;
		case R.id.btn_modify:	//修改
			nextStep(MAKE_CARD_STEP_INPUT_INFO);
			mContext.finish();
			break;
		case R.id.btn_send:
			
			break;
		}
	}
	
	@Override
	public boolean backPressed() {
		return false;
	}


	static class WeddingCardAdapter extends BaseAdapter {

		DisplayImageOptions option = new DisplayImageOptions.Builder()
				.cacheInMemory(true).cacheOnDisk(true)
//				.showImageOnLoading(R.drawable.default_iphone)
//				.showImageForEmptyUri(R.drawable.default_iphone)
//				.showImageOnFail(R.drawable.default_iphone)
				.bitmapConfig(Config.RGB_565).build();

		OnClickListener onclick;
		PreviewReuslt data;

		public WeddingCardAdapter(OnClickListener onclick) {
			this.onclick = onclick;
		}
		
		public void setData(PreviewReuslt data) {
			this.data = data;
		}

		@Override
		public int getCount() {
			return data != null ? 1 : 0;
		}

		@Override
		public Object getItem(int position) {
			return data != null ? data : null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			HolderView holder;
			if (convertView == null) {
				holder = new HolderView();
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.item_list_wedding_card, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.iv_wedding_card_pic);
				holder.name = (TextView) convertView
						.findViewById(R.id.tv_mr_ms);
				holder.date = (TextView) convertView
						.findViewById(R.id.tv_wedding_date);
				holder.msg = (TextView) convertView
						.findViewById(R.id.tv_wedding_notify_msg);
				holder.btnModify = (TextView) convertView
						.findViewById(R.id.btn_modify);
				holder.btnLeaverMsg = (TextView) convertView
						.findViewById(R.id.btn_relative_msg);
				holder.btnSend = (TextView) convertView
						.findViewById(R.id.btn_send);
				convertView.setTag(holder);
			} else {
				holder = (HolderView) convertView.getTag();
			}
			ImageLoader.getInstance().displayImage(data.index_img, holder.icon,
					option);

			holder.name.setText(MApp.get().getString(
					R.string.string_placeholder_bride_bridegroom, data.mname,
					data.fname));
			holder.date.setText(data.marry_date);
//			holder.msg.setText(data.get(position).title);

			holder.btnModify.setTag(position);
			holder.btnModify.setOnClickListener(onclick);

			holder.btnLeaverMsg.setVisibility(View.GONE);
//			holder.btnLeaverMsg.setTag(position);
//			holder.btnLeaverMsg.setOnClickListener(onclick);

			holder.btnSend.setTag(data.url);
			holder.btnSend.setOnClickListener(onclick);

			return convertView;
		}

		static class HolderView {
			TextView name, date, msg, btnModify, btnLeaverMsg, btnSend;
			ImageView icon;
		}

	}
	
}
