package com.marriage.delegate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.Theme;
import com.marriage.bean.ThemeInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.ToastHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ChoiceThemeMakeCardImpl extends StepMakeCard implements View.OnClickListener, Listener<String>, ErrorListener, OnItemClickListener {

	private GridView mGV;
	private ThemeAdapter mAdapter;
	private int height;
	private int checkedId = -1;
	
	@Override
	public void onCreate() {
		setContentView(R.layout.activity_makecard_choice_theme);
		findViewById(R.id.back_btn).setOnClickListener(this);
		findViewById(R.id.make_card_next).setOnClickListener(this);
		mGV = (GridView)findViewById(R.id.gridView1);
		mGV.setOnItemClickListener(this);
		mAdapter = new ThemeAdapter();
		mGV.setAdapter(mAdapter);
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CARD_GET_THEME;
		StringRequest request = new StringRequest(Method.POST, url, this, this) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("xt_id", mXitieId);
				return params;
			}
		};
		requestLoading();
		MApp.get().getRequestQueue().add(request);
		
		caluHeight();
	}

	private void caluHeight() {
		DisplayMetrics dm = mContext.getResources().getDisplayMetrics();
		int themePadding = mContext.getResources().getDimensionPixelSize(R.dimen.theme_padding);
		int widthOffset = (dm.widthPixels - themePadding * 4) / 3;
		height = (widthOffset * 189) / 151;
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.back_btn) {
			finish();
		} else if(v.getId() == R.id.make_card_next) {
			setChoiceTheme(checkedId);
		}
	}
	
	private void setChoiceTheme(final int theme_id) {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CARD_SET_THEME;
		StringRequest request = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String response) {
						endLoading();
						BaseApiResult result = null;
						try {
							result = new Gson().fromJson(response,
									BaseApiResult.class);
						} catch (JsonSyntaxException e) {
							e.printStackTrace();
							return;
						}
						if (Constants.SUCCESS.equals(result.op_code)) {
							nextStep(MAKE_CARD_STEP_CHOICE_MUSIC);
						} else if (!TextUtils.isEmpty(result.op_info)) {
							ToastHelper.showShortToast(result.op_info);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						endLoading();
						ToastHelper.showShortToast("网络异常");
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				String userId = MApp.get().isLogin() ? MApp.get().getUser().user.user_id
						: "";
				params.put("theme_id", String.valueOf(theme_id));
				params.put("xt_id", mXitieId);
				params.put("user_id", userId);
				return params;
			}
		};
		requestLoading();
		MApp.get().getRequestQueue().add(request);
	}
	
	class ThemeAdapter extends BaseAdapter {
		List<Theme> themes;

		public void setData(List<Theme> themes) {
			this.themes = themes;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return themes != null ? themes.size() : 0;
		}

		@Override
		public Object getItem(int position) {
			return themes != null ? themes.get(position) : null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_theme, null);
			ImageView theme = (ImageView)convertView.findViewById(R.id.theme_img);
			theme.getLayoutParams().height = height;
			ImageLoader.getInstance().displayImage(themes.get(position).preview, theme);
			TextView tv = (TextView)convertView.findViewById(R.id.theme_name);
			tv.setText(themes.get(position).name);
			View checked = convertView.findViewById(R.id.checked);
			if(checkedId == themes.get(position).id) {
				checked.setVisibility(View.VISIBLE);
			} else {
				checked.setVisibility(View.INVISIBLE);
			}
			
			return convertView;
		}

	}

	@Override
	public void onErrorResponse(VolleyError error) {
		endLoading();
		ToastHelper.showShortToast("网络异常");
	}

	@Override
	public void onResponse(String response) {
		endLoading();
		ThemeInfo info = null;
		try {
			info = new Gson().fromJson(response, ThemeInfo.class);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			ToastHelper.showShortToast("错误");
			return;
		}
		
		if(Constants.SUCCESS.equals(info.op_code)) {
			checkedId = info.themes.get(0).id;
			mAdapter.setData(info.themes);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Theme t = (Theme)parent.getAdapter().getItem(position);
		if(t != null) {
			checkedId = t.id;
			mAdapter.notifyDataSetChanged();
		}
		
	}
	
}
