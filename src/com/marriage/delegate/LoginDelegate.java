package com.marriage.delegate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.UserInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuth;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.openapi.legacy.UsersAPI;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

public class LoginDelegate {

	private static final String TAG = "LoginDelegate";

	private static final String QQ_APPKEY = "BKNnjznABmHK8gmk";

	private static final String QQ_APPID = "1101961152";

	// private static final String TEST_ID = "222222";

	private static final String QQ_SCOPE = "get_user_info,add_topic";

	private static final String SINA_WEIBO_APPKEY = "3544173230";

	private static final String SINA_WEIBO_APP_SECRET = "1970de5782a3118bf731c6f38b58b74b";

	private static final String SINA_WEIBO_REDIRECT_URL = "https://api.weibo.com/oauth2/default.html";

	private static final String SINA_WEIBO_SCOPE = "email,direct_messages_read,direct_messages_write,"
			+ "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
			+ "follow_app_official_microblog," + "invitation_write";

	private SsoHandler mWeiboHandler;

	private Activity context;

	private Tencent mTencent;

	private LoginDelegateListener loginListener;

	public LoginDelegate(Activity c) {
		context = c;
	}

	public void login(final String email, final String pwd) {
		onLoginStart();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_LOGIN;
		StringRequest sr = new StringRequest(Method.POST, url, listener,
				errListener) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("email", email);
				params.put("user_pwd", pwd);
				return params;
			}

		};
		MApp.get().getRequestQueue().add(sr);
	}

	public void loginByQQ(Fragment f) {
		if (mTencent == null) {
			mTencent = Tencent.createInstance(QQ_APPID, MApp.get());
		}
		mTencent.login(f, "all", qqListener);

	}

	public void loginBySinaWeibo(Activity activity) {
		WeiboAuth weiboAuth = new WeiboAuth(activity, SINA_WEIBO_APPKEY,
				SINA_WEIBO_REDIRECT_URL, SINA_WEIBO_SCOPE);
		mWeiboHandler = new SsoHandler(activity, weiboAuth);
		mWeiboHandler.authorize(sinaWeiboListener);

	}

	public void setLoginDelegateListener(LoginDelegateListener l) {
		loginListener = l;
	}

	public SsoHandler getWeiboSsoHandler() {
		return mWeiboHandler;
	}

	private void getUserInfoByQQ(final String openId, final String nickName) {
		onLoginStart();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_QQ_LOGIN;
		StringRequest sr = new StringRequest(Method.POST, url, listener,
				errListener) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("id", openId);
				params.put("name", nickName);
				return params;
			}
		};
		MApp.get().getRequestQueue().add(sr);
	}

	private void getUserInfoBySinaWeibo(final String uid, final String name,
			final String token) {
		onLoginStart();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_SINA_LOGIN;
		StringRequest sr = new StringRequest(Method.POST, url, listener,
				errListener) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("id", uid);
				params.put("name", name);
				params.put("sina_token", token);
				return params;
			}
		};
		MApp.get().getRequestQueue().add(sr);
	}

	private void loginFailure(String msg) {
		if (loginListener != null) {
			loginListener.onLoginFailure(msg);
		}
	}

	private IUiListener qqListener = new IUiListener() {

		@Override
		public void onError(UiError arg0) {
			// TODO Auto-generated method stub
			String errMsg = arg0.errorMessage;
			loginFailure(errMsg);
		}

		@Override
		public void onComplete(Object arg0) {
			// TODO Auto-generated method stub
			JSONObject json;
			try {
				String ret = arg0.toString();
				D.info(TAG, "QQ ret: " + ret);
				json = new JSONObject(ret);

				if (json.has("openid")) {
					getQQUserInfo(json.getString("openid"));
					return;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			loginFailure(MApp.get().getString(R.string.login_err));
		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub
			if (loginListener != null) {
				loginListener.onLoginCancel();
			}
		}
	};

	private void getQQUserInfo(final String id) {
		if (mTencent == null) {
			mTencent = Tencent.createInstance(QQ_APPID, MApp.get());
		}
		// mTencent.requestAsync(com.tencent.connect.common.Constants.g, arg1,
		// arg2, arg3, arg4)
		onLoginStart();
		com.tencent.connect.UserInfo uInfo = new com.tencent.connect.UserInfo(
				MApp.get(), mTencent.getQQToken());
		uInfo.getUserInfo(new IUiListener() {

			@Override
			public void onError(UiError arg0) {
				// TODO Auto-generated method stub
				String errMsg = arg0.errorMessage;
				loginFailure(errMsg);
			}

			@Override
			public void onComplete(Object arg0) {
				// TODO Auto-generated method stub
				JSONObject json;
				try {
					String ret = arg0.toString();
					D.info(TAG, "QQinfo ret: " + ret);
					json = new JSONObject(ret);

					if (json.has("nickname")) {
						getUserInfoByQQ(id, json.getString("nickname"));
						return;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				loginFailure(MApp.get().getString(R.string.login_err));
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				if (loginListener != null) {
					loginListener.onLoginCancel();
				}
			}
		});
	}

	private WeiboAuthListener sinaWeiboListener = new WeiboAuthListener() {

		@Override
		public void onWeiboException(WeiboException arg0) {
			// TODO Auto-generated method stub
			String errMsg = arg0.getMessage();
			loginFailure(errMsg);
		}

		@Override
		public void onComplete(Bundle arg0) {
			// TODO Auto-generated method stub
			Oauth2AccessToken mAccessToken = Oauth2AccessToken
					.parseAccessToken(arg0);
			if (mAccessToken.isSessionValid()) {
				String uid = mAccessToken.getUid();
				String token = mAccessToken.getToken();
				getWeiboProfile(mAccessToken, uid, token);
			} else {
				loginFailure(MApp.get().getString(R.string.login_err));
			}
		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub
			if (loginListener != null) {
				loginListener.onLoginCancel();
			}
		}
	};

	private void getWeiboProfile(Oauth2AccessToken accessToken,
			final String id, final String token) {
		D.info(TAG, "sinaWeibo profile: " + "getWeiboProfile");
		onLoginStart();
		UsersAPI userAPI = new UsersAPI(accessToken);
		userAPI.show(Long.parseLong(accessToken.getUid()),
				new RequestListener() {

					@Override
					public void onIOException(IOException e) {
						// TODO Auto-generated method stub
						D.error("getWeiboProfile+onIOException", e.getMessage());
					}

					@Override
					public void onError(WeiboException e) {
						// TODO Auto-generated method stub
						D.error("getWeiboProfile+onError", e.getMessage());
					}

					@Override
					public void onComplete4binary(
							ByteArrayOutputStream responseOS) {
						// TODO Auto-generated method stub
						D.error("getWeiboProfile+onComplete4binary",
								new String(responseOS.toByteArray()));
					}

					@Override
					public void onComplete(String response) {
						// TODO Auto-generated method stub
						D.info(TAG, "sinaWeibo profile: " + response);
						try {
							JSONObject json = new JSONObject(response);
							if (json.has("screen_name")) {
								final String weiboUserName = json
										.getString("screen_name");
								context.runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										getUserInfoBySinaWeibo(id,
												weiboUserName, token);
									}
								});

							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					}
				});
	}

	private Listener<String> listener = new Listener<String>() {

		@Override
		public void onResponse(String arg0) {
			// TODO Auto-generated method stub
			D.debug(TAG, "login response: " + arg0);
			UserInfo userInfo = new Gson().fromJson(arg0, UserInfo.class);
			if (userInfo == null) {
				Toast.makeText(MApp.get(), "出错啦~~~", Toast.LENGTH_SHORT).show();
				return;
			}
			if (Constants.SUCCESS.equals(userInfo.op_code)) {
				MApp.get().setUser(userInfo);
				onLoginSuccess();
			} else {
				onLoginFailure(userInfo.op_info);
			}
		}

	};

	private ErrorListener errListener = new ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError arg0) {
			// TODO Auto-generated method stub
			onLoginFailure(MApp.get().getString(R.string.net_err));
		}

	};

	private void onLoginSuccess() {
		if (loginListener != null) {
			loginListener.onLoginSuccess();
		}
	}

	private void onLoginFailure(String msg) {
		if (loginListener != null) {
			loginListener.onLoginFailure(msg);
		}
	}

	private void onLoginCancel() {
		if (loginListener != null) {
			loginListener.onLoginCancel();
		}
	}

	private void onLoginStart() {
		if (loginListener != null) {
			loginListener.onLoginStart();
		}
	}

	public interface LoginDelegateListener {
		public void onLoginStart();

		public void onLoginFailure(String msg);

		public void onLoginCancel();

		public void onLoginSuccess();
	}
}
