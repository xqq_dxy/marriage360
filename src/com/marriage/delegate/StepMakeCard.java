package com.marriage.delegate;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import com.marriage.R;
import com.marriage.activity.BaseActivity;
import com.marriage.dialog.WeddingDialog;

public abstract class StepMakeCard {
	
	public static final String EXTRA_MAKE_CARD_STEP = "extra_make_card_step";
	public static final String EXTRA_CARD_ID = "extra_card_id";
	
	public static final int MAKE_CARD_STEP_INPUT_CODE = 1;
	public static final int MAKE_CARD_STEP_INPUT_INFO = 2;
	public static final int MAKE_CARD_STEP_CHOICE_THEME = 3;
	public static final int MAKE_CARD_STEP_CHOICE_MUSIC = 4;
	public static final int MAKE_CARD_STEP_PREVIEW_CARD = 5;
	
	protected BaseActivity mContext;
	protected String mXitieId;
	
	public final void init(BaseActivity context, String xitie_id) {
		mContext = context;
		mXitieId = xitie_id;
	}
	
	public void onCreate() {}
	
	public void setContentView(int layoutResID) {
		mContext.setContentView(layoutResID);
	}
	
	public View findViewById(int id) {
        return mContext.findViewById(id);
    }
	
	public void nextStep(int step) {
		Intent intent = new Intent(mContext, mContext.getClass());
		intent.putExtra(EXTRA_MAKE_CARD_STEP, step);
		intent.putExtra(EXTRA_CARD_ID, mXitieId);
		mContext.startActivity(intent);
		mContext.finish();
	}
	
	protected void back(){
		finish();
	}
	
	protected BaseActivity getContext(){
		return mContext;
	}
	
	protected void requestLoading(){
		mContext.showLoading();
	}
	
	protected void endLoading(){
		mContext.endLoading();
	}
	
	public void onActivityResult(int requestCode,int resultCode,Intent data){
		
	}
		
	protected void finish() {
		WeddingDialog dialog = new WeddingDialog.Builder(getContext())
				.setTitle(R.string.string_use_card_code_hint)
				.setMessage("确定退出制作吗？")
				.setPositiveButton(R.string.string_dialog_ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								mContext.finish();
							}
						})
				.setNegativeButton(R.string.string_dialog_cancel, null)
				.create();
		dialog.show();
	}
	
	public boolean backPressed() {
		return true;
	}
}
