package com.marriage.delegate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.ImageUploadBean;
import com.marriage.bean.XitieImg;
import com.marriage.bean.XitieInfoBean;
import com.marriage.dialog.WeddingDialog;
import com.marriage.dialog.WeddingShareAddDialog;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.FileUtil;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.adapter.CardImageAdapter;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.sleepbot.datetimepicker.time.TimePickerDialog.OnTimeSetListener;

public class InputInfoMakeCardImpl extends StepMakeCard implements
		View.OnClickListener, OnItemClickListener, OnDateSetListener,
		OnTimeSetListener {

	private static final int REQUEST_OPEN_CAMERA_COVER = 0x0001;
	private static final int REQUEST_OPEN_GALLERY_COVER = 0x0002;
	private static final int REQUEST_OPEN_CAMERA_NORMAL = 0x0003;
	private static final int REQUEST_OPEN_GALLERY_NORMAL = 0x0004;

	private static final String TAG = "InputInfoMakeCardImpl";

	public static final String DATEPICKER_TAG = "datepicker";
	public static final String TIMEPICKER_TAG = "timepicker";

	private static final String IMG_PICK_TAG_COVER = "COVER";
	private static final String IMG_PICK_TAG_NORMAL = "NORMAL";

	private CheckBox mOpenCard;

	private EditText mCardTitleInput, mGroomNameInput, mGroomPhoneInput,
			mBrideNameInput, mBridePhoneInput, mHotelNameInput,
			mHotelAddressInput, mRemarkInput;
	private TextView mWeddingDateShow;
	private GridView mImages;
	private String mCardTile, mGroomName, mGroomPhone, mBrideName, mBridePhone,
			mHotelName, mHotelAddress, mRemark, mWedingDate;

	private String hour;
	private String minute;
	private String week;

	private CardImageAdapter mAdapter;

	private WeddingShareAddDialog imgPickDialog;

	@Override
	public void onCreate() {
		setContentView(R.layout.activity_makecard_info_input);
		initViews();
		loadCardInfo();
	}

	private void initViews() {
		findViewById(R.id.back_btn).setOnClickListener(this);
		findViewById(R.id.make_card_next).setOnClickListener(this);
		findViewById(R.id.make_card_wedding_date_input)
				.setOnClickListener(this);
		mOpenCard = (CheckBox) findViewById(R.id.make_card_open);
		mCardTitleInput = (EditText) findViewById(R.id.make_card_title_input);
		mGroomNameInput = (EditText) findViewById(R.id.make_card_groom_name_input);
		mGroomPhoneInput = (EditText) findViewById(R.id.make_card_groom_tel_input);
		mBrideNameInput = (EditText) findViewById(R.id.make_card_bride_name_input);
		mBridePhoneInput = (EditText) findViewById(R.id.make_card_bride_tel_input);
		mHotelNameInput = (EditText) findViewById(R.id.make_card_hotel_name_input);
		mHotelAddressInput = (EditText) findViewById(R.id.make_card_hotel_address_input);
		mRemarkInput = (EditText) findViewById(R.id.make_card_remark_input);
		mWeddingDateShow = (TextView) findViewById(R.id.make_card_wedding_date_show);
		mImages = (GridView) findViewById(R.id.make_card_images);
		mImages.setOnItemClickListener(this);

		mAdapter = new CardImageAdapter(getContext());
		mAdapter.addData(new CardImgObj());
		mAdapter.addData(new CardImgObj());
		mImages.setAdapter(mAdapter);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			back();
			break;

		case R.id.make_card_next:
			goNext();
			break;
		case R.id.make_card_wedding_date_input:
			goSetWeddingDay();
			break;
		case R.id.share_weixin:
			goPickPhotoByCamera(IMG_PICK_TAG_COVER.equals(imgPickDialog
					.getTag()));
			imgPickDialog.dismiss();
			break;
		case R.id.share_weixin_friends:
			goPickPhotoFromGallery(IMG_PICK_TAG_COVER.equals(imgPickDialog
					.getTag()));
			imgPickDialog.dismiss();
			break;
		case R.id.share_cancle:
			imgPickDialog.dismiss();
			break;
		}
	}

	private void goNext() {
		if (checkInput()) {
			uploadCardInfo();
		}
	}

	private void goSetWeddingDay() {
		final Calendar calendar = Calendar.getInstance();
		DatePickerDialog dpd = DatePickerDialog.newInstance(this,
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), false);
		dpd.show(getContext().getSupportFragmentManager(), DATEPICKER_TAG);
	}

	private void goSetWeddingTime() {
		final Calendar calendar = Calendar.getInstance();
		final TimePickerDialog tpd = TimePickerDialog.newInstance(this,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), true, false);
		tpd.show(getContext().getSupportFragmentManager(), TIMEPICKER_TAG);
	}

	private void goPickPhotoFromGallery(boolean isCover) {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		getContext().startActivityForResult(
				intent,
				isCover ? REQUEST_OPEN_GALLERY_COVER
						: REQUEST_OPEN_GALLERY_NORMAL);
	}

	private void goPickPhotoByCamera(boolean isCover) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		getContext().startActivityForResult(
				intent,
				isCover ? REQUEST_OPEN_CAMERA_COVER
						: REQUEST_OPEN_CAMERA_NORMAL);
	}

	private boolean checkInput() {
		mCardTile = mCardTitleInput.getText().toString().trim();
		if (TextUtils.isEmpty(mCardTile)) {
			ToastHelper.showShortToast(R.string.prompt_pls_input_title_first);
			return false;
		}

		mGroomName = mGroomNameInput.getText().toString().trim();
		if (TextUtils.isEmpty(mGroomName)) {
			ToastHelper
					.showShortToast(R.string.prompt_pls_input_groom_name_first);
			return false;
		}

		mGroomPhone = mGroomPhoneInput.getText().toString().trim();
		/*
		 * if (TextUtils.isEmpty(mGroomPhone)) { ToastHelper
		 * .showShortToast(R.string.prompt_pls_input_groom_phone_first); return
		 * false; }
		 */

		mBrideName = mBrideNameInput.getText().toString().trim();
		if (TextUtils.isEmpty(mBrideName)) {
			ToastHelper
					.showShortToast(R.string.prompt_pls_input_bride_name_first);
			return false;
		}

		mBridePhone = mBridePhoneInput.getText().toString().trim();
		/*
		 * if (TextUtils.isEmpty(mBridePhone)) { ToastHelper
		 * .showShortToast(R.string.prompt_pls_input_bride_phone_first); return
		 * false; }
		 */

		if (TextUtils.isEmpty(mWedingDate) || TextUtils.isEmpty(hour)
				|| TextUtils.isEmpty(minute) || TextUtils.isEmpty(week)) {
			ToastHelper
					.showShortToast(R.string.prompt_pls_input_wedding_date_first);
			return false;
		}

		mHotelName = mHotelNameInput.getText().toString().trim();
		if (TextUtils.isEmpty(mHotelName)) {
			ToastHelper
					.showShortToast(R.string.prompt_pls_input_hotel_name_first);
			return false;
		}

		mHotelAddress = mHotelAddressInput.getText().toString().trim();
		if (TextUtils.isEmpty(mHotelAddress)) {
			ToastHelper
					.showShortToast(R.string.prompt_pls_input_hotel_address_first);
			return false;
		}

		mRemark = mRemarkInput.getText().toString().trim();
		return true;
	}

	private void bindData(XitieInfoBean bean) {
		mCardTitleInput.setText(bean.xt_info.title == null ? ""
				: bean.xt_info.title);
		mGroomNameInput.setText(bean.xt_info.mname == null ? ""
				: bean.xt_info.mname);
		mGroomPhoneInput.setText(bean.xt_info.xldh == null ? ""
				: bean.xt_info.xldh);
		mBrideNameInput.setText(bean.xt_info.fname == null ? ""
				: bean.xt_info.fname);
		mBridePhoneInput.setText(bean.xt_info.xndh == null ? ""
				: bean.xt_info.xndh);
		String date = "";
		if (!TextUtils.isEmpty(bean.xt_info.marry_date)
				&& !TextUtils.isEmpty(bean.xt_info.hour)
				&& !TextUtils.isEmpty(bean.xt_info.min)) {
			mWedingDate = bean.xt_info.marry_date;
			hour = bean.xt_info.hour;
			minute = bean.xt_info.min;
			week = bean.xt_info.week;
			date = bean.xt_info.marry_date + " " + bean.xt_info.hour + ":"
					+ bean.xt_info.min;
		}
		mWeddingDateShow.setText(date);
		mHotelNameInput.setText(bean.xt_info.hotel == null ? ""
				: bean.xt_info.hotel);
		mHotelAddressInput.setText(bean.xt_info.addr == null ? ""
				: bean.xt_info.addr);
		mRemarkInput.setText(bean.xt_info.content == null ? ""
				: bean.xt_info.content);
		mOpenCard.setChecked("0".equals(bean.xt_info.is_visi));
		mAdapter.getItem(0).imgId = mXitieId;
		mAdapter.getItem(0).imgUrl = bean.xt_info.index_img;
		if (bean.xiangce_info != null){
			for(int i=0;i<bean.xiangce_info.size();i++){
				mAdapter.insertData(CardImgObj.fromXitieImg(bean.xiangce_info.get(i)), mAdapter.getCount()-1);
			}
		}
		mAdapter.notifyDataSetChanged();
	}

	private void loadCardInfo() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_CARD_INFO;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						D.debug(TAG, "load xitie info ret: " + arg0);
						endLoading();
						try {
							XitieInfoBean bean = new Gson().fromJson(arg0,
									XitieInfoBean.class);
							if (bean != null && bean.xt_info != null) {
								bindData(bean);
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						endLoading();
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("xt_id", mXitieId);
				return params;
			}

		};
		requestLoading();
		MApp.get().getRequestQueue().add(sr);
	}

	private void uploadCardInfo() {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_SET_CARD_INFO;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						endLoading();
						D.debug(TAG, "setCardInfo ret: " + arg0);
						try {
							BaseApiResult result = new Gson().fromJson(arg0,
									BaseApiResult.class);
							if (result != null
									&& Constants.SUCCESS.equals(result.op_code)) {
								nextStep(MAKE_CARD_STEP_CHOICE_THEME);
							} else {
								ToastHelper.showShortToast(result.op_info);
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
							ToastHelper.showShortToast(R.string.net_err);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						endLoading();
						ToastHelper.showShortToast(R.string.net_err);
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("xt_id", mXitieId);
				params.put("title", mCardTile);
				params.put("xndh", mBridePhone);
				params.put("xldh", mGroomPhone);
				params.put("fname", mBrideName);
				params.put("mname", mGroomName);
				params.put("hotel", mHotelName);
				params.put("addr", mHotelAddress);
				params.put("marry_date", mWedingDate);
				params.put("hour", hour);
				params.put("min", minute);
				params.put("week", week);
				params.put("content", mRemark);
				params.put("is_visi", mOpenCard.isChecked() ? "0" : "1");
				return params;
			}

		};
		requestLoading();
		MApp.get().getRequestQueue().add(sr);
	}

	private void uploadPic(final boolean isCover, final Uri uri) {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CARD_UPLOAD_IMG;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						endLoading();
						D.debug(TAG, "upload img ret: " + arg0);
						try {
							ImageUploadBean bean = new Gson().fromJson(arg0,
									ImageUploadBean.class);
							if (bean != null
									&& Constants.SUCCESS.equals(bean.op_code)) {
								processImgUploadOk(isCover, bean);
								ToastHelper
										.showShortToast(R.string.card_img_upload_success);
							} else {
								ToastHelper.showShortToast(bean.op_info);
							}
						} catch (Exception e) {
							// TODO: handle exception
							ToastHelper.showShortToast(R.string.net_err);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						endLoading();
						ToastHelper.showShortToast(R.string.net_err);
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				String imgStr = null;
				try {
					imgStr = Base64
							.encodeToString(FileUtil
									.inputStreamTOByte(getContext()
											.getContentResolver()
											.openInputStream(uri)),
									Base64.DEFAULT);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Map<String, String> params = new HashMap<String, String>();
				params.put("id", mXitieId);
				params.put("is_cover", isCover ? "1" : "0");
				params.put("img", imgStr == null ? "" : imgStr);
				return params;
			}

		};
		requestLoading();
		MApp.get().getRequestQueue().add(sr);

	}

	private void deletePic(final boolean isCover, final String id,
			final int position) {
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CARD_DELETE_IMG;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String arg0) {
						// TODO Auto-generated method stub
						endLoading();
						D.debug(TAG, "delete img ret: " + arg0);
						try {
							BaseApiResult bean = new Gson().fromJson(arg0,
									BaseApiResult.class);
							if (bean != null
									&& Constants.SUCCESS.equals(bean.op_code)) {
								processImgDeleteOk(position);
								ToastHelper
										.showShortToast(R.string.card_img_delete_success);
							} else {
								ToastHelper.showShortToast(bean.op_info);
							}
						} catch (Exception e) {
							// TODO: handle exception
							ToastHelper.showShortToast(R.string.net_err);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						endLoading();
						ToastHelper.showShortToast(R.string.net_err);
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("id", id);
				params.put("is_cover", isCover ? "1" : "0");
				return params;
			}

		};
		requestLoading();
		MApp.get().getRequestQueue().add(sr);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		if (position == mAdapter.getCount() - 1) {
			showImagePickDialog(false);
			return;
		}

		CardImgObj data = mAdapter.getItem(position);
		if (data.imgUrl == null) {
			showImagePickDialog(position == 0);
		} else {
			showImgDeleteDialog(position, R.string.confirm_delete_this_img);
		}
	}

	private void showImagePickDialog(boolean isCover) {
		if (imgPickDialog == null) {
			imgPickDialog = new WeddingShareAddDialog(this);
		}
		imgPickDialog.show(getContext().getSupportFragmentManager(),
				isCover ? IMG_PICK_TAG_COVER : IMG_PICK_TAG_NORMAL);

	}

	private void showImgDeleteDialog(final int position, int msgResId) {
		new WeddingDialog.Builder(getContext())
				.setMessage(msgResId)
				.setPositiveButton(R.string.confirm,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								if (position == 0) {
									deletePic(true, mXitieId, position);
								} else {
									deletePic(false,
											mAdapter.getItem(position).imgId,
											position);
								}
							}
						}).setNegativeButton(R.string.cancel, null).show();
	}

	private void processImgDeleteOk(int position) {
		if (position == 0) {
			mAdapter.getItem(position).clear();
			mAdapter.notifyDataSetChanged();
		} else {
			mAdapter.removeData(position);
		}
	}

	private void processImgUploadOk(boolean isCover, ImageUploadBean bean) {
		if (isCover) {
			mAdapter.getItem(0).fillData(bean);
			mAdapter.notifyDataSetChanged();
		} else {
			CardImgObj obj = CardImgObj.from(bean);
			mAdapter.insertData(obj, mAdapter.getCount() - 1);
		}
	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day, int week) {
		// TODO Auto-generated method stub
		mWedingDate = year + "-" + (month + 1) + "-" + day;
		this.week = new DateFormatSymbols().getWeekdays()[week].toUpperCase(Locale.getDefault());
		D.debug(TAG, "week: " + this.week);
		goSetWeddingTime();
	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		this.hour = String.valueOf(hourOfDay);
		this.minute = String.valueOf(minute);
		String weddingTimeShow = mWedingDate + " " + this.hour + ":"
				+ this.minute;
		mWeddingDateShow.setText(weddingTimeShow);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_OPEN_CAMERA_COVER:
			if (resultCode == Activity.RESULT_OK) {
				processImgFromCamera(data, true);
			}
			break;
		case REQUEST_OPEN_CAMERA_NORMAL:
			if (resultCode == Activity.RESULT_OK) {
				processImgFromCamera(data, false);
			}
			break;
		case REQUEST_OPEN_GALLERY_COVER:
			if (resultCode == Activity.RESULT_OK) {
				processImgFromGallery(data, true);
			}
			break;
		case REQUEST_OPEN_GALLERY_NORMAL:
			if (resultCode == Activity.RESULT_OK) {
				processImgFromGallery(data, false);
			}
			break;

		}

	}

	private File generateImageOutputFile() {
		File file = new File(getContext().getCacheDir(), String.valueOf(System
				.currentTimeMillis()) + ".jpg");
		return file;
	}

	private void processImgFromCamera(Intent data, boolean isCover) {
		if (data != null) {
			Bundle bundle = data.getExtras();
			if (bundle != null && bundle.get("data") != null) {
				Bitmap bitmap = (Bitmap) bundle.get("data");
				File fileName = generateImageOutputFile();
				FileOutputStream fos;
				try {
					fos = new FileOutputStream(fileName);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
					Uri uri = Uri.fromFile(fileName);
					uploadPic(isCover, uri);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void processImgFromGallery(Intent data, boolean isCover) {
		Uri uri = data.getData();
		if (uri != null) {
			uploadPic(isCover, uri);
		}
	}

	public static class CardImgObj {
		public String imgUrl;
		public String imgId;

		public void clear() {
			imgUrl = null;
			imgId = null;
		}

		public void fillData(ImageUploadBean data) {
			imgUrl = data.img_path;
			imgId = data.id;
		}

		public static CardImgObj from(ImageUploadBean data) {
			CardImgObj obj = new CardImgObj();
			obj.fillData(data);
			return obj;
		}
		
		public static CardImgObj fromXitieImg(XitieImg img){
			CardImgObj obj = new CardImgObj();
			obj.imgId = img.id;
			obj.imgUrl = img.img_path;
			return obj;
		}

	}

}
