package com.marriage.delegate;

import java.util.HashMap;
import java.util.Map;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.CheckCodeResult;
import com.marriage.dialog.WeddingDialog;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ToastHelper;

public class InputCodeMakeCardImpl extends StepMakeCard implements View.OnClickListener, Listener<String>, ErrorListener {

	private EditText mIdNoText;
	
	@Override
	public void onCreate() {
		setContentView(R.layout.activity_make_wedding_card);
		
		findViewById(R.id.btn_start_make).setOnClickListener(this);
		findViewById(R.id.back_btn).setOnClickListener(this);
		mIdNoText = (EditText) findViewById(R.id.et_id_no);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.btn_start_make) {
			if(TextUtils.isEmpty(mIdNoText.getText().toString())) {
				ToastHelper.showShortToast("串号不能为空");
				return;
			}
			showConfirmDialog();
		} else if(v.getId() == R.id.back_btn) {
			finish();
		}
	}
	
	@Override
	public boolean backPressed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void showConfirmDialog() {
		WeddingDialog dialog = new WeddingDialog.Builder(getContext())
				.setTitle(R.string.string_use_card_code_hint)
				.setMessage(R.string.string_whether_use_card)
				.setPositiveButton(R.string.string_dialog_ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								checkCode(mIdNoText.getText().toString());
							}
						})
				.setNegativeButton(R.string.string_dialog_cancel, null)
				.create();
		dialog.show();
	}
	

	private void checkCode(final String code) {
		if(TextUtils.isEmpty(code)) {
			ToastHelper.showShortToast("串号不能为空");
			return;
		}
		
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_CARD_CHECK_CODE;
		StringRequest request = new StringRequest(Method.POST, url, this, this) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("code", code);
				params.put("user_id", MApp.get().getUser().user.user_id);
				return params;
			}
			
		};
		requestLoading();
		MApp.get().getRequestQueue().add(request);
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		endLoading();
		ToastHelper.showShortToast("网络异常");
	}

	@Override
	public void onResponse(String response) {
		D.debug("MARTIN", response);
		endLoading();
		CheckCodeResult result;
		try {
			result = new Gson().fromJson(response, CheckCodeResult.class);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			ToastHelper.showShortToast("错误");
			return;
		}
		
		if (Constants.SUCCESS.equals(result.op_code)
				&& !TextUtils.isEmpty(result.xt_id)) {
			mXitieId = result.xt_id;
			nextStep(MAKE_CARD_STEP_INPUT_INFO);
		} else if(!TextUtils.isEmpty(result.op_info)) {
			ToastHelper.showShortToast(result.op_info);
		}
	}
	
}
