package com.marriage.delegate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.Music;
import com.marriage.bean.MusicInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.adapter.MusicAdapter;

public class ChoiceMusicMakeCardImpl extends StepMakeCard implements OnClickListener, OnItemClickListener{

	private static final String TAG = "ChoiceMusicMakeCardImpl";
	
	private ListView mMusicList;
	private MusicAdapter mAdapter;
	@Override
	public void onCreate() {
		setContentView(R.layout.activity_makecard_music_choice);
		mAdapter = new MusicAdapter(getContext());
		initViews();
		loadMusicList();
	}

	private void initViews(){
		mMusicList = (ListView) findViewById(R.id.make_card_music_list);
		mMusicList.setOnItemClickListener(this);
		mMusicList.setAdapter(mAdapter);
		findViewById(R.id.back_btn).setOnClickListener(this);
		findViewById(R.id.make_card_next).setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.back_btn:
			back();
			break;

		case R.id.make_card_next:
			checkChosedMusic();
			break;
		}
	}

	private void checkChosedMusic(){
		int chosed = mAdapter.getChosedPosition();
		if (chosed == -1){
			ToastHelper.showShortToast(R.string.prompt_pls_chose_a_music_first);
		}else{
			setChoseMusic(chosed);
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		mAdapter.setChosedPosition(position);
	}
	
	private void bindMusicData(List<Music> musics){
		if (musics == null){
			return;
		}
		mAdapter.setDatas(musics);
		for(int i=0;i<musics.size();i++){
			if (musics.get(i).checked){
				mAdapter.setChosedPosition(i);
				break;
			}
		}
	}
	
	private void loadMusicList(){
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_CARD_MUSIC_LIST;
		StringRequest sr = new StringRequest(Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {
				// TODO Auto-generated method stub
				D.debug(TAG, "MUSIC RET: "+arg0);
				try {
					MusicInfo musicInfo = new Gson().fromJson(arg0, MusicInfo.class);
					if(musicInfo != null && Constants.SUCCESS.equals(musicInfo.op_code)){
						bindMusicData(musicInfo.music_list);
					}else{
						ToastHelper.showShortToast(musicInfo.op_info);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					ToastHelper.showShortToast(R.string.net_err);
				}
				endLoading();
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				ToastHelper.showShortToast(R.string.net_err);
				endLoading();
			}
		}){

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("xt_id", mXitieId);
				return params;
			}
			
		};
		requestLoading();
		MApp.get().getRequestQueue().add(sr);
	}
	
	private void setChoseMusic(final int p){
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_SET_CARD_MUSIC;
		StringRequest sr = new StringRequest(Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {
				// TODO Auto-generated method stub
				endLoading();
				D.debug(TAG, "setMusic ret: "+arg0);
				try {
					BaseApiResult result = new Gson().fromJson(arg0, BaseApiResult.class);
					if (result != null && Constants.SUCCESS.equals(result.op_code)){
						nextStep(MAKE_CARD_STEP_PREVIEW_CARD);
					}else{
						ToastHelper.showShortToast(result.op_info);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					ToastHelper.showShortToast(R.string.net_err);
				}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				endLoading();
				ToastHelper.showShortToast(R.string.net_err);
			}
		}){

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Music chosedData = mAdapter.getItem(p);
				Map<String, String> params = new HashMap<String, String>();
				params.put("music_id", String.valueOf(chosedData.id));
				params.put("xt_id", mXitieId);
				params.put("user_id", MApp.get().getUser().user.user_id);
				return params;
			}
		};
		requestLoading();
		MApp.get().getRequestQueue().add(sr);
	}
}
