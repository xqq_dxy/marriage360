package com.marriage.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.marriage.fragment.MineCommentFragment;

/**
 * 我的评论
 * 
 * @author Martin
 * 
 */
public class MineCommentActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {

		super.onCreate(arg0);
		if (arg0 == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content,
							Fragment.instantiate(this,
									MineCommentFragment.class.getName(),
									getIntent().getExtras())).commit();
		}
	}

}
