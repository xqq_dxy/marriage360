package com.marriage.activity;

import com.marriage.fragment.MinePraiseFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class MinePraiseActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		if (arg0 == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content,
							Fragment.instantiate(this,
									MinePraiseFragment.class.getName()),
							"MinePraiseFragment").commit();
		}
	}
}
