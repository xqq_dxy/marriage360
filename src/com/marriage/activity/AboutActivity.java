package com.marriage.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.marriage.fragment.AboutFragment;

public class AboutActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			AboutFragment af = new AboutFragment();
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			ft.add(android.R.id.content, af,
					AboutFragment.class.getSimpleName()).commit();
		}
	}
}
