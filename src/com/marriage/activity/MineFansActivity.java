package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.fragment.MineFansFragment;

public class MineFansActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			MineFansFragment mff = new MineFansFragment();
			Intent incoming = getIntent();
			if (incoming != null){
				mff.setArguments(incoming.getExtras());
			}
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content, mff,
							"min_fans").commit();
		}
	}

}
