package com.marriage.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.PublishComment;
import com.marriage.bean.PublishCommentInfo;
import com.marriage.bean.TopicDetailImages;
import com.marriage.bean.TopicDetailInfo;
import com.marriage.bean.UserInfo;
import com.marriage.bean.VoteUser;
import com.marriage.dialog.CommentDialog;
import com.marriage.dialog.CommentDialog.CommentSendExecutor;
import com.marriage.dialog.ShareDialog;
import com.marriage.fragment.BaseFragment;
import com.marriage.utility.AppConfig;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.PageRecord;
import com.marriage.utility.ShareUtils;
import com.marriage.utility.ToastHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;

public class WeddingDetailActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			Fragment frg = new WeddingDetailFragment();
			frg.setArguments(getIntent().getExtras());
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, frg, "topics_detail").commit();
		}
		
	}
	
	public static String dateDiff(long diff) {
		if(diff <= 60)
			return String.valueOf(diff) + "秒";
		diff = diff / 60;
		if (diff <= 60)
			return String.valueOf(diff) + "分钟";
		diff = diff / 60;
		if (diff <= 24)
			return String.valueOf(diff) + "小时";
		diff = diff / 24;
		if (diff <= 365)
			return String.valueOf(diff) + "天";
		diff = diff / 365;
		return String.valueOf(diff) + "年";
	}
	
	public static String dateDiffPrivate(long diff) {
		long temp = diff;
		if(diff <= 60)
			return String.valueOf(diff) + "秒前";
		diff = diff / 60;
		if (diff <= 60)
			return String.valueOf(diff) + "分钟前";
		diff = diff / 60;
		if (diff <= 24)
			return String.valueOf(diff) + "小时前";
		SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = new Date(System.currentTimeMillis() - (temp * 1000));
			return formate.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@SuppressLint("ValidFragment")
	static class WeddingDetailFragment extends BaseFragment implements
			View.OnClickListener, OnScrollListener, CommentSendExecutor, BitmapDisplayer {

		String topicId;
		Button btnComment, btnLike, btnShare, addFollow;
		ListView listView;
		ImageView ivAvatar;
		TextView tvName, tvTime, tvWishContent, tvCategory, tvLikeNum;
		LinearLayout llLikeAvatar;
//		ViewPager llImageWall;
//		MyAdapter mPagerAdapter;
		StringRequest mRequest;
		CommentRequest mCommentRequest;
		TopicDetailInfo mInfo;
		CommentAdapter mCommentAdapter;
		boolean loading;
		List<PublishComment> mComment = new ArrayList<PublishComment>();
		PageRecord page = new PageRecord(1, 100);
		ShareDialog mDialog;
		CommentDialog mCommentDialog;
		LinearLayout mImageWall;
		String sl_id, slName;
		

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			Bundle arg = getArguments();
			if (arg != null) {
				topicId = arg.getString("topic_id");
				sl_id = arg.getString("sl_id");
				slName = arg.getString("slName");
			}
			String url = AppConfig.INSTANCE.SERVER
					+ Constants.URL_TOPICS_DETAIL;
			mRequest = new StringRequest(Method.POST, url,
					new Listener<String>() {

						@Override
						public void onResponse(String response) {
							D.debug("MARTIN", response);
							dismissLoadingDialog();
							TopicDetailInfo info = null;
							try {
								info = new Gson().fromJson(response,
										TopicDetailInfo.class);
							} catch (JsonSyntaxException e) {
								return;
							}

							if (Constants.SUCCESS.equals(info.op_code)) {
								bindView(info);
							} else {
								ToastHelper.showShortToast(info.op_info);
							}
							if(mComment.size() == 0)
								mCommentRequest.nextPage();
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							dismissLoadingDialog();
							ToastHelper.showShortToast("网络不给力");
						}
					}) {

				@Override
				protected Map<String, String> getParams()
						throws AuthFailureError {
					Map<String, String> params = new HashMap<String, String>();
					params.put("topic_id", topicId);
					if (MApp.get().isLogin()) {
						params.put("user_id", MApp.get().getUser().user.user_id);
					}
					return params;
				}

			};

			mCommentRequest = new CommentRequest();
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(
					R.layout.activity_weddng_share_detail_layout, container,
					false);
			return view;
		}

		private void bindView(TopicDetailInfo info) {
			if (getActivity() == null)
				return;
			mInfo = info;
			if(!TextUtils.isEmpty(slName)) {
				tvName.setText(slName);
			} else {
				tvName.setText(info.topic_model.topic_user_name);
			}
			if (!info.is_sa) {
				/*tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.v_icon_grey_small, 0);*/
			}

			ImageLoader.getInstance().displayImage(info.topic_user_head_img,
					ivAvatar, ImageUtil.getCircleDisplayImageOptions());
			ivAvatar.setOnClickListener(this);
			tvTime.setText(getString(R.string.string_placeholder_time_city,
					WeddingDetailActivity.dateDiffPrivate(Long
							.valueOf(info.time_diff)),
							info.topic_model.topic_city_name));
			tvWishContent.setText(info.topic_model.content);
			tvCategory.setText("#" + info.topic_model.topic_cate_name);
//			mPagerAdapter.swapData(info.topic_model.imgs);
			tvLikeNum.setText(getString(
					R.string.string_placeholder_liked_people,
					info.topic_model.fav_count));
			String userId = MApp.get().isLogin() ? MApp.get().getUser().user.user_id
					: null;
			boolean isSameUser = TextUtils.equals(
					info.topic_model.topic_user_id, userId);
			if(isSameUser) {
				addFollow.setVisibility(View.GONE);
			} else {
				setFollow(info.focus);
			}
			bindLikedLayout(info);
			bindImageWall(info);
			bindFavBackound(info);
		}
		
		private void bindFavBackound(TopicDetailInfo info) {
			if(info.is_fav) {
				btnLike.setBackgroundResource(R.drawable.details_menu_liked);
			} else {
				btnLike.setBackgroundResource(R.drawable.details_menu_like);
			}
		}

		private void setFollow(boolean isFollow) {
			if (isFollow) {
				addFollow.setSelected(true);
				addFollow.setText(R.string.string_has_followed);
			} else {
				addFollow.setSelected(false);
				addFollow.setText(R.string.string_add_follow);
			}
		}
		
		private void bindImageWall(TopicDetailInfo info) {
			List<TopicDetailImages> urls = info.topic_model.imgs;
			mImageWall.removeAllViews();
			if(urls != null) {
				for (int i = 0; i < urls.size(); i++) {
					ImageView iv = new ImageView(getActivity());
					iv.setScaleType(ScaleType.FIT_XY);
					ImageLoader.getInstance().displayImage(urls.get(i).imgs_path,
							iv, option);
//					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//							LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//					if (i != urls.size() - 1) {
//						params.bottomMargin = CommonUtils.dip2Px(getActivity(), 10);
//					}
					mImageWall.addView(iv);
				}
			}
		}
		
		private DisplayImageOptions option = new DisplayImageOptions.Builder()
				.cacheInMemory(true).cacheOnDisk(true).displayer(this)
//				.showImageOnLoading(R.drawable.default_iphone)
				.bitmapConfig(Config.RGB_565).build();
		
		@Override
		public void display(Bitmap bitmap, ImageAware imageAware,
				LoadedFrom arg2) {
			// TODO Auto-generated method stub
			if (!imageAware.isCollected() && isVisible()) {
				ImageView image = (ImageView) imageAware
						.getWrappedView();
				
				int swidth = image.getContext().getResources().getDisplayMetrics().widthPixels;
				int width = swidth - CommonUtils.dip2Px(getActivity(), 20);
				
				float scale = (float)bitmap.getWidth() / (float)bitmap.getHeight();
				
				int height = (int)(width / scale);
				android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams)image.getLayoutParams();
				if(params == null) {
					params = new android.widget.LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				}
				params.bottomMargin = CommonUtils.dip2Px(getActivity(), 10);
				params.height = height;
				image.setLayoutParams(params);
				image.setImageBitmap(bitmap);
			}
		}

		/**
		 * 
		 * @param startTime
		 *            单位毫秒
		 * @param endTime
		 *            单位毫秒
		 * @return
		 */
		public static String dateDiff(long startTime, long endTime) {
			long diff = (endTime - startTime) / 1000; // 秒
			if (diff <= 60)
				return String.valueOf(diff) + "秒";
			diff = diff / 60;
			if (diff <= 60)
				return String.valueOf(diff) + "分钟";
			diff = diff / 60;
			if (diff <= 60)
				return String.valueOf(diff) + "小时";
			diff = diff / 24;
			if (diff <= 365)
				return String.valueOf(diff) + "天";
			diff = diff / 365;
			return String.valueOf(diff) + "年";
		}

		private void bindLikedLayout(TopicDetailInfo info) {
			int count = info.vote_user != null ? info.vote_user.size() : 0;
//			try {
//				count = Integer.valueOf(info.vote_user_count);
//			} catch (NumberFormatException e) {
//				e.printStackTrace();
//			}
			if (count == 0) {
				llLikeAvatar.setVisibility(View.GONE);
				return;
			}
			llLikeAvatar.setVisibility(View.VISIBLE);
			llLikeAvatar.removeAllViews();
			float density = getResources().getDisplayMetrics().density;
			int wh = (int) (27 * density + 0.5f);
			int gutter = (int) (11 * density + 0.5f);

			int avWidth = llLikeAvatar.getWidth()
					- llLikeAvatar.getPaddingLeft()
					- llLikeAvatar.getPaddingRight() - wh;

			boolean full = false;

			DisplayImageOptions options = ImageUtil
					.getCircleDisplayImageOptions();

			for (int i = 0; i < count; i++) {
				if (avWidth < wh + gutter) {
					full = true;
					break;
				}

				ImageView image = new ImageView(getActivity());
				image.setId(R.id.publish_detail_liked_user_avatar);
				image.setTag(info.vote_user.get(i));
				image.setOnClickListener(this);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						wh, wh);
				image.setLayoutParams(params);
				avWidth = avWidth - wh;
				if (avWidth > gutter) {
					params.rightMargin = gutter;
					avWidth = avWidth - gutter;
				}
				ImageLoader.getInstance().displayImage(
						info.vote_user.get(i).avatar_small, image, options);
				llLikeAvatar.addView(image);
			}

			if (full) {
				ImageView image = new ImageView(getActivity());
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						wh, wh);
				image.setLayoutParams(params);
				image.setImageResource(R.drawable.visitor_more);
				llLikeAvatar.addView(image);
			}

		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			btnComment = (Button) view.findViewById(R.id.btn_commment);
			btnLike = (Button) view.findViewById(R.id.btn_like);
			btnShare = (Button) view.findViewById(R.id.btn_share);

			listView = (ListView) view.findViewById(android.R.id.list);
			View header = LayoutInflater.from(view.getContext()).inflate(
					R.layout.activity_wedding_share_detail_header, listView,
					false);
			listView.addHeaderView(header);
			mCommentAdapter = new CommentAdapter(mComment);
			listView.setAdapter(mCommentAdapter);
			listView.setOnScrollListener(this);
			ivAvatar = (ImageView) view.findViewById(R.id.iv_header_icon);

			tvName = (TextView) view.findViewById(R.id.tv_name);
			tvTime = (TextView) view.findViewById(R.id.tv_time);
			addFollow = (Button) view.findViewById(R.id.btn_follow_me);
			tvWishContent = (TextView) view.findViewById(R.id.tv_wish_content);
			tvCategory = (TextView) view.findViewById(R.id.tv_cat_name);
			tvLikeNum = (TextView) view.findViewById(R.id.tv_like);
//			llImageWall = (ViewPager) view.findViewById(R.id.layout_image_wall);
//			llImageWall.setAdapter(mPagerAdapter = new MyAdapter());
//			PageIndicator pi = (PageIndicator)view.findViewById(R.id.vp);
//			pi.setViewPager(llImageWall);
			llLikeAvatar = (LinearLayout) view.findViewById(R.id.like_layout);
			view.findViewById(R.id.back_btn).setOnClickListener(this);
			
			mImageWall = (LinearLayout) view.findViewById(R.id.layout_image_wall);


			btnComment.setOnClickListener(this);
			btnLike.setOnClickListener(this);
			btnShare.setOnClickListener(this);
			addFollow.setOnClickListener(this);

			view.post(new Runnable() {

				@Override
				public void run() {
					showLoadingDialog();
					getRequestQueue().add(mRequest);
				}
			});

			super.onViewCreated(view, savedInstanceState);
		}

		static class MyAdapter extends PagerAdapter {

			private List<TopicDetailImages> mData;

			@Override
			public boolean isViewFromObject(View view, Object object) {
				return view == object;
			}

			@Override
			public int getCount() {
				return mData != null ? mData.size() : 0;
			}

			public void swapData(List<TopicDetailImages> data) {
				mData = data;
				notifyDataSetChanged();
			}

			@Override
			public Object instantiateItem(ViewGroup container, int position) {
				ImageView iv = new ImageView(container.getContext());
//				iv.setBackgroundResource(R.drawable.loading_failure);
				iv.setScaleType(ScaleType.CENTER_CROP);
				ImageLoader.getInstance().displayImage(
						mData.get(position).imgs_path, iv);
				container.addView(iv);
				return iv;
			}

			@Override
			public void destroyItem(ViewGroup container, int position,
					Object object) {
				container.removeView((View) object);
			}

		}

		class CommentRequest extends StringRequest {
			
			public CommentRequest() {
				super(Method.POST, AppConfig.INSTANCE.SERVER
						+ Constants.URL_GET_TOPIC_COMMENT, new Listener<String>() {

					@Override
					public void onResponse(String response) {
						D.debug("MARTIN", response);
						loading = false;
						dismissLoadingDialog();
						PublishCommentInfo comm = null;
						try {
							comm = new Gson().fromJson(response,
									PublishCommentInfo.class);
						} catch (JsonSyntaxException e) {
							e.printStackTrace();
							return;
						}
						if(Constants.SUCCESS.equals(comm.op_code) && comm.reply_topic_ls != null) {
							page.preNextPager();
							mComment.addAll(comm.reply_topic_ls);
							mCommentAdapter.notifyDataSetChanged();
							page.setTotalSize(100);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						loading = false;
						dismissLoadingDialog();
						ToastHelper.showLongToast(R.string.net_err);
					}
				});
			}

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("topic_id", mInfo.topic_model.topic_id);
				params.put("page_index",
						String.valueOf(page.currentPageIndex()));
				params.put("page_size", String.valueOf(page.getPageSize()));
				return params;
			}

			public void clear() {
				page.clear();
				mComment.clear();
			}
			
			public void nextPage() {
				if (page.hasNextPager() && !loading && mInfo != null) {
					loading = true;
					getRequestQueue().add(this);
				}
			}

		}

		class CommentAdapter extends BaseAdapter {

			DisplayImageOptions options = ImageUtil
					.getCircleDisplayImageOptions();

			List<PublishComment> comment;

			public CommentAdapter(List<PublishComment> comment) {
				this.comment = comment;
			}

			@Override
			public int getCount() {
				return comment.size();
			}

			@Override
			public Object getItem(int position) {
				return comment.get(position);
			}

			@Override
			public long getItemId(int position) {
				return 0;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				ViewHolder holder;
				if (convertView == null) {
					holder = new ViewHolder();
					convertView = LayoutInflater.from(parent.getContext())
							.inflate(R.layout.item_list_publish_detail_comment,
									parent, false);
					holder.userAvatar = (ImageView) convertView
							.findViewById(R.id.user_avatar);
					holder.name = (TextView) convertView
							.findViewById(R.id.user_name);
					holder.time = (TextView) convertView
							.findViewById(R.id.user_time);
					holder.content = (TextView) convertView
							.findViewById(R.id.content);
					convertView.setTag(holder);
				} else {
					holder = (ViewHolder) convertView.getTag();
				}
				ImageLoader.getInstance().displayImage(
						comment.get(position).avatar_small, holder.userAvatar,
						options);
				holder.userAvatar.setTag(position);
				holder.userAvatar.setOnClickListener(WeddingDetailFragment.this);
				holder.name.setText(comment.get(position).user_name);
				holder.time.setText(comment.get(position).reply_time);
				holder.content.setText(comment.get(position).content);

				return convertView;
			}

			class ViewHolder {
				ImageView userAvatar;
				TextView name, time, content;
			}

		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.back_btn:
				getActivity().finish();
				break;
			case R.id.btn_follow_me:
				onFollowMe();
				break;
			case R.id.btn_commment:
				onComment();
				break;
			case R.id.btn_like:
				if(mInfo != null) {
					onLike(!mInfo.is_fav);
				}
				break;
			case R.id.btn_share:
				if(mDialog == null) {
					mDialog = new ShareDialog(this);
				}
				mDialog.show(getFragmentManager(), "SHARE_DIALOG");
				break;
				
			case R.id.share_sms:
				ShareUtils.shareToSms(getActivity(), mInfo.topic_model.content + mInfo.share_url, "");
				mDialog.dismissAllowingStateLoss();
				break;
			case R.id.share_mail:
				ShareUtils.shareToEmail(getActivity(), "", Constants.SHARE_EMAIL_SUBJECT, mInfo.topic_model.content + mInfo.share_url);
				mDialog.dismissAllowingStateLoss();
				break;
			case R.id.share_weixin:
				ShareUtils.shareToWeixin(getActivity(),
						mInfo.topic_model.content, "", mInfo.share_url,
						Constants.SHARE_TITLE, false);
				mDialog.dismissAllowingStateLoss();
				break;
			case R.id.share_weixin_cycle:
				ShareUtils.shareToWeixin(getActivity(),
						mInfo.topic_model.content, "", mInfo.share_url,
						Constants.SHARE_TITLE, true);
				mDialog.dismissAllowingStateLoss();
				break;
			case R.id.share_weibo:
				ShareUtils.shareToWeibo(getActivity(), Constants.SHARE_TITLE, mInfo.topic_model.content + mInfo.share_url);
				mDialog.dismissAllowingStateLoss();
				break;
			case R.id.share_qq:
				ShareUtils.shareToQQ(getActivity(), Constants.SHARE_TITLE, mInfo.topic_model.content + mInfo.share_url);
				mDialog.dismissAllowingStateLoss();
				break;
			case R.id.share_cancle:
				if(mDialog != null) {
					mDialog.dismissAllowingStateLoss();
				}
				break;
			case R.id.iv_header_icon:	//头像
				goMechantDetail();
				break;
			case R.id.user_avatar:
				Integer position = (Integer)v.getTag();
				if(position == null) return;
				PublishComment comment = mComment.get(position.intValue());
				if(comment == null) return;
				if(!comment.is_sa) {
					Intent goNormalUser = new Intent(MApp.get(),NormalUserInfoActivity.class);		//普通用户
					goNormalUser.putExtra(Constants.BUNDLE_USER_ID, comment.user_id);
					goNormalUser.putExtra(Constants.BUNDLE_USER_AVATAR, comment.avatar_small);
					goNormalUser.putExtra(Constants.BUNDLE_USER_NAME, comment.user_name);
					goNormalUser.putExtra(Constants.BUNDLE_USER_CITY, comment.city_name);
					startActivity(goNormalUser);
				} else {
					Intent goMerchantDetail = new Intent(MApp.get(),MerchantDetailActivity.class);		//商家用户
					goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_NAME, comment.sl_name);
					goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, comment.sl_id);
					startActivity(goMerchantDetail);
				}
				
				break;
			case R.id.publish_detail_liked_user_avatar:
				VoteUser vu = (VoteUser)v.getTag();
				if(vu != null) {
					if(!vu.v_is_sa) {
						Intent goNormalUser = new Intent(MApp.get(),NormalUserInfoActivity.class);		//普通用户
						goNormalUser.putExtra(Constants.BUNDLE_USER_ID, vu.user_id);
						goNormalUser.putExtra(Constants.BUNDLE_USER_AVATAR, vu.avatar_small);
						goNormalUser.putExtra(Constants.BUNDLE_USER_NAME, vu.v_user_name);
						goNormalUser.putExtra(Constants.BUNDLE_USER_CITY, vu.v_city_name);
						startActivity(goNormalUser);
					} else {
						Intent goMerchantDetail = new Intent(MApp.get(),MerchantDetailActivity.class);		//商家用户
						goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_NAME, vu.v_sl_name);
						goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, vu.v_sl_id);
						startActivity(goMerchantDetail);
					}
				}
				break;
			}
		}
		
		public void goMechantDetail() {
			if(!TextUtils.isEmpty(mInfo.sl_id) && !TextUtils.isEmpty(mInfo.sl_name)) {
				Intent goMerchantDetail = new Intent(MApp.get(), MerchantDetailActivity.class);
				goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_NAME, mInfo.sl_name);		
				goMerchantDetail.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, mInfo.sl_id);
				startActivity(goMerchantDetail);
			}
			
		}
		
		private void onComment() {
			if (!MApp.get().isLogin()){
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivityForResult(intent, 11);
				return;
			}
			if(mCommentDialog == null)
				mCommentDialog = new CommentDialog(R.string.string_publish_hint_comment);
			mCommentDialog.setCommentSendExecutor(this);
			mCommentDialog.show(getFragmentManager(), "COMMENT_DIALOG");
		}

		private void onFollowMe() {
			if (!MApp.get().isLogin()) {
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivityForResult(intent, 11);
			} else {
				doFollow(!mInfo.focus);
			}
		}
		
		private void onLike(final boolean whetherLike) {
			if (!MApp.get().isLogin()){
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivityForResult(intent, 11);
				return;
			}
			
			String url = AppConfig.INSTANCE.SERVER + Constants.URL_USER_LIKE;
			StringRequest request = new StringRequest(Method.POST, url,
					new Listener<String>() {

						@Override
						public void onResponse(String response) {
							D.debug("MARTIN", response);
							dismissLoadingDialog();
							BaseApiResult info = null;
							try {
								info = new Gson().fromJson(response,
										BaseApiResult.class);
							} catch (JsonSyntaxException e) {
								e.printStackTrace();
							}
							if (Constants.SUCCESS.equals(info.op_code)) {
								ToastHelper.showLongToast(whetherLike ? "已赞" : "已取消赞");
								mInfo.is_fav = whetherLike;
								if (!whetherLike){
									Intent i = new Intent(Constants.ACTION_CANCEL_PRAISE);
									i.putExtra(Constants.BUNDLE_TOPIC_ID, topicId);
									MApp.get().sendBroadcast(i);
								}
								bindFavBackound(mInfo);
								try {
									int old = Integer.valueOf(mInfo.topic_model.fav_count);
									if(whetherLike)
										old++;
									else 
										old--;
									if(old < 0) {
										old = 0;
									}
									mInfo.topic_model.fav_count = String.valueOf(old);
									tvLikeNum.setText(getString(R.string.string_placeholder_liked_people, mInfo.topic_model.fav_count));
									getRequestQueue().add(mRequest);
								} catch (NumberFormatException e) {
									e.printStackTrace();
								}
								if(getActivity() != null) {
									Intent data = getActivity().getIntent();
									data.putExtra("likeNum", mInfo.topic_model.fav_count);
									getActivity().setResult(Activity.RESULT_OK, data);
								}
							} else {
								ToastHelper.showLongToast(info.op_info);
							}
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							dismissLoadingDialog();
							ToastHelper.showLongToast(R.string.net_err);
						}
					}) {

				@Override
				protected Map<String, String> getParams()
						throws AuthFailureError {
					Map<String, String> map = new HashMap<String, String>();
					UserInfo user = MApp.get().getUser();
					map.put("fav_flag", whetherLike ? "1" : "0");
					map.put("user_id", user.user.user_id);
					map.put("user_name", user.user.user_name);
					map.put("topic_id", mInfo.topic_model.topic_id);
					return map;
				}

			};
			showLoadingDialog();
			getRequestQueue().add(request);
		}

		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			showLoadingDialog();
			getRequestQueue().add(mRequest);
			mCommentRequest.clear();
			mCommentRequest.nextPage();
			super.onActivityResult(requestCode, resultCode, data);
		}

		private void doFollow(final boolean follow) {
			String url = AppConfig.INSTANCE.SERVER + Constants.URL_ADD_FOLLOW;

			StringRequest request = new StringRequest(Method.POST, url,
					new Listener<String>() {

						@Override
						public void onResponse(String response) {
							dismissLoadingDialog();
							BaseApiResult info = null;
							try {
								info = new Gson().fromJson(response,
										BaseApiResult.class);
							} catch (JsonSyntaxException e) {
								e.printStackTrace();
							}
							if (Constants.SUCCESS.equals(info.op_code)) {
								mInfo.focus = follow;
								setFollow(follow);
								if (follow)
									ToastHelper
											.showLongToast(R.string.string_has_followed);
								else
									ToastHelper
											.showLongToast(R.string.string_has_cancle_followed);
							} else {
								ToastHelper.showLongToast(info.op_info);
							}
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							dismissLoadingDialog();
							ToastHelper.showLongToast(R.string.net_err);
						}
					}) {

				@Override
				protected Map<String, String> getParams()
						throws AuthFailureError {
					Map<String, String> map = new HashMap<String, String>();
					UserInfo user = MApp.get().getUser();
					map.put("focus_user_id", user.user.user_id);
					map.put("focus_user_name", user.user.user_name);
					map.put("focused_user_id", mInfo.topic_model.topic_user_id);
					map.put("focused_user_name",
							mInfo.topic_model.topic_user_name);
					map.put("focus_flag", follow ? "1" : "-1");
					return map;
				}

			};
			showLoadingDialog();
			getRequestQueue().add(request);
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			switch (scrollState) {
			case OnScrollListener.SCROLL_STATE_IDLE:
				if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
					mCommentRequest.nextPage();
					D.error("MARTIN", ">>>>>>>>>>>>加载更多<<<<<<<<<<<");
				}
				break;
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {}

		@Override
		public void sendComment(final String comment) {
			showLoadingDialog();
			String url = AppConfig.INSTANCE.SERVER
					+ Constants.URL_TOPIC_COMMENT;
			StringRequest request = new StringRequest(Method.POST, url,
					new Listener<String>() {

						@Override
						public void onResponse(String response) {
							dismissLoadingDialog();
							BaseApiResult info = null;
							try {
								info = new Gson().fromJson(response,
										BaseApiResult.class);
							} catch (JsonSyntaxException e) {
								e.printStackTrace();
							}
							if(Constants.SUCCESS.equals(info.op_code)) {
								if(mCommentDialog != null)
									mCommentDialog.dismissAllowingStateLoss();
								onActivityResult(0, 0, null);
							}
						}
					}, new ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							dismissLoadingDialog();
						}
					}) {

				@Override
				protected Map<String, String> getParams()
						throws AuthFailureError {
					UserInfo user = MApp.get().getUser();
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("user_id", user.user.user_id);
					params.put("user_name", user.user.user_name);
					params.put("content", comment);
					params.put("topic_id", mInfo.topic_model.topic_id);
					params.put("reply_user_id", mInfo.topic_model.topic_user_id);
					params.put("reply_user_name",
							mInfo.topic_model.topic_user_name);
					return params;
				}
			};
			
			getRequestQueue().add(request);
		}
	}

}
