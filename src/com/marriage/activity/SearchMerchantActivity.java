package com.marriage.activity;

import com.marriage.fragment.SearcheMerchantFragment;

import android.content.Intent;
import android.os.Bundle;

public class SearchMerchantActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			SearcheMerchantFragment smf = new SearcheMerchantFragment();
			Intent incoming = getIntent();
			if (incoming != null) {
				smf.setArguments(incoming.getExtras());
			}
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content, smf, SearcheMerchantFragment.TAG)
					.commit();
		}
	}
}
