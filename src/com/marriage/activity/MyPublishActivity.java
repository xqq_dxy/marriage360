package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.fragment.MyPublishFragment;

public class MyPublishActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			MyPublishFragment mpf = new MyPublishFragment();
			Intent data = getIntent();
			if (data != null) {
				mpf.setArguments(data.getExtras());
			}
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, mpf, MyPublishFragment.TAG)
					.commit();
		}
	}
}
