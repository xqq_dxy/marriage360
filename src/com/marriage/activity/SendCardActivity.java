package com.marriage.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.R;
import com.marriage.bean.ContactPeople;
import com.marriage.bean.ContactPeopleInfo;
import com.marriage.bean.XTList;
import com.marriage.utility.Constants;
import com.marriage.utility.PreferenceHelper;
import com.marriage.utility.ShareUtils;
import com.marriage.utility.ToastHelper;

public class SendCardActivity extends BaseActivity implements OnClickListener {

	private TextView mEditBtn;
	private ContactPeopleInfo mContact;
	private SendAdapter mAdapter;
	private boolean mEditMode;
	private XTList mXT;
	private TextView title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mXT = (XTList) getIntent().getSerializableExtra("XT");
		if (mXT == null) {
			finish();
			return;
		}

		setContentView(R.layout.activity_send_card);
		findViewById(R.id.btn_add_from_contact).setOnClickListener(this);
		findViewById(R.id.btn_add_direct).setOnClickListener(this);
		findViewById(R.id.back_btn).setOnClickListener(this);
		title = (TextView)findViewById(R.id.title);
		ListView mListView = (ListView) findViewById(android.R.id.list);
		mAdapter = new SendAdapter();
		mListView.setAdapter(mAdapter);

		String jsonData = PreferenceHelper.getString(
				PreferenceHelper.PreferenceKeys.CONTACT_PEOPLE, null);

		mContact = convertToBean(jsonData);
		if (mContact != null) {
			mAdapter.setData(mContact.info);
		}

		mEditBtn = (TextView) findViewById(R.id.edit_btn);
		mEditBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_add_from_contact: // 通讯录添加
			Intent intent = new Intent(Intent.ACTION_PICK,
					ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, 1);
			break;
		case R.id.btn_add_direct: // 直接添加
			showDialog(1, null);
			break;
		case R.id.edit_btn: // 编辑
			mEditMode = !mEditMode;
			if (!mEditMode) {
				mEditBtn.setText(R.string.string_edit);
			} else {
				mEditBtn.setText(R.string.string_cancel);
			}
			mAdapter.notifyEditMode();
			break;

		case R.id.delete_btn:
			Integer p = (Integer) v.getTag();
			if (p != null) {
				mContact.info.remove(p.intValue());
				mAdapter.notifyDataSetChanged();
			}
			break;
		case R.id.wx_invite: {
			String content = String.format(Constants.SHARE_CONTENT_HOLD_PLACE, mXT.mname, mXT.fname, mXT.marry_date, mXT.hour, mXT.min, mXT.hotel, "");
			ShareUtils.shareToWeixin(this, content, "", mXT.url, Constants.SHARE_TITLE, false);
			break;
		}
		case R.id.sms_send:
			Integer position = (Integer)v.getTag();
			String content = String.format(Constants.SHARE_CONTENT_HOLD_PLACE, mXT.mname, mXT.fname, mXT.marry_date, mXT.hour, mXT.min, mXT.hotel, mXT.url);
			ShareUtils.shareToSms(this, content, mContact.info.get(position.intValue()).phone);
			break;
		case R.id.title:
			Bundle args = new Bundle();
			args.putInt("position", ((Integer) v.getTag()).intValue());
			showDialog(2, args);
			break;
		case R.id.back_btn: // 后退
			finish();
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		if (id == 1) { // 直接添加
			AlertDialog.Builder b = new AlertDialog.Builder(this);
			View view = this.getLayoutInflater().inflate(
					R.layout.dialog_add_invite_people, null);
			final EditText name = (EditText) view.findViewById(R.id.et_name);
			final EditText phone = (EditText) view.findViewById(R.id.et_phone);
			final EditText title = (EditText) view.findViewById(R.id.et_title);
			b.setView(view);
			b.setPositiveButton("确定", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					String sname = name.getText().toString();
					String sphone = phone.getText().toString();
					String stitle = title.getText().toString();
					if (TextUtils.isEmpty(sname) || TextUtils.isEmpty(sphone)) {
						ToastHelper.showShortToast("未添加好友,姓名或手机号为空");
						return;
					}

					ContactPeople contact = new ContactPeople();
					contact.name = sname;
					contact.phone = sphone;
					contact.title = stitle;
					if (mContact == null) {
						mContact = new ContactPeopleInfo();
						mContact.info = new ArrayList<ContactPeople>();
					}
					mContact.info.add(contact);
					mAdapter.setData(mContact.info);
				}
			}).setNegativeButton("取消", null);
			return b.show();
		} else if (id == 2) {
			final int position = args.getInt("position");

			AlertDialog.Builder b = new AlertDialog.Builder(this);
			View view = this.getLayoutInflater().inflate(
					R.layout.dialog_modify_title, null);
			final EditText title = (EditText) view.findViewById(R.id.et_title);
			title.setText(mContact.info.get(position).title);
			b.setView(view);
			b.setPositiveButton("确定", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					mContact.info.get(position).title = title.getText()
							.toString();
					mAdapter.setData(mContact.info);
				}
			}).setNegativeButton("取消", null);
			return b.show();
		}
		return null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			ContentResolver reContentResolverol = getContentResolver();
			Uri contactData = data.getData();
			Cursor cursor = getContentResolver().query(contactData, null, null,
					null, null);
			cursor.moveToFirst();
			String name = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
			String phoneNum = "";
			String contactId = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts._ID));
			Cursor phone = reContentResolverol.query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = "
							+ contactId, null, null);
			while (phone.moveToNext()) {
				phoneNum = phone
						.getString(phone
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			}

			ContactPeople people = new ContactPeople();
			people.name = name;
			people.phone = phoneNum;
			if (mContact == null) {
				mContact = new ContactPeopleInfo();
				mContact.info = new ArrayList<ContactPeople>();
			}
			mContact.info.add(people);
			mAdapter.setData(mContact.info);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private ContactPeopleInfo convertToBean(String jsonData) {
		if (jsonData != null) {
			try {
				ContactPeopleInfo info = new Gson().fromJson(jsonData,
						ContactPeopleInfo.class);
				return info;
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private String convertToJsonStr(ContactPeopleInfo data) {
		if (data != null) {
			try {
				return new Gson().toJson(data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	class SendAdapter extends BaseAdapter {
		List<ContactPeople> mData;

		private void setData(List<ContactPeople> data) {
			mData = data;
			notifyDataSetChanged();
		}

		@Override
		public void notifyDataSetChanged() {
			title.setText("您将发送喜帖给"+getCount()+"位好友：");
			super.notifyDataSetChanged();
		}

		private void notifyEditMode() {
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mData != null ? mData.size() : 0;
		}

		@Override
		public ContactPeople getItem(int position) {
			return mData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = LayoutInflater.from(parent.getContext()).inflate(
						R.layout.item_list_send_card, parent, false);
				holder = new ViewHolder();
				holder.del = (ImageView) convertView
						.findViewById(R.id.delete_btn);
				holder.name = (TextView) convertView.findViewById(R.id.name);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				holder.wxInvite = (TextView) convertView
						.findViewById(R.id.wx_invite);
				holder.smsSend = (TextView) convertView
						.findViewById(R.id.sms_send);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.del.setOnClickListener(SendCardActivity.this);
			holder.title.setOnClickListener(SendCardActivity.this);
			holder.title.setTag(position);
			holder.del.setTag(position);
			holder.name.setText(getItem(position).name);
			holder.title.setText(getItem(position).title);
			holder.wxInvite.setOnClickListener(SendCardActivity.this);
			holder.wxInvite.setTag(position);
			holder.smsSend.setOnClickListener(SendCardActivity.this);
			holder.smsSend.setTag(position);
			if (mEditMode) {
				holder.del.setVisibility(View.VISIBLE);
			} else {
				holder.del.setVisibility(View.GONE);
			}

			return convertView;
		}

	}

	static class ViewHolder {
		TextView name, title, wxInvite, smsSend;
		ImageView del;
	}

	@Override
	protected void onDestroy() {
		String jsonData = convertToJsonStr(mContact);
		if (jsonData != null) {
			PreferenceHelper.putString(
					PreferenceHelper.PreferenceKeys.CONTACT_PEOPLE, jsonData);
		}
		super.onDestroy();
	}

}
