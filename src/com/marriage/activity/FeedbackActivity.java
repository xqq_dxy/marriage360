package com.marriage.activity;

import android.os.Bundle;

import com.marriage.fragment.FeedbackFragment;

public class FeedbackActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			FeedbackFragment ff = new FeedbackFragment();
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content, ff,
							FeedbackFragment.class.getSimpleName()).commit();
		}
	}
}
