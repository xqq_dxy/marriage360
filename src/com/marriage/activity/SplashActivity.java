package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.marriage.R;
import com.marriage.utility.D;

public class SplashActivity extends BaseActivity implements Runnable{

	private static final int[] resIds = {R.drawable.splash_240_320,R.drawable.splash_320_480,
		R.drawable.splash_480_800,R.drawable.splash_720_1280};
	private static final double[] scales = {0.75,0.66667,0.6,0.5625};
	
	private static final String TAG = "SplashActivity";
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_splash);
		
		ImageView img = (ImageView) findViewById(R.id.splash_img);
		
		int screenWidth = getResources().getDisplayMetrics().widthPixels;
		int screenHeight = getResources().getDisplayMetrics().heightPixels;
		D.info(TAG, "screenWidth: "+screenWidth);
		D.info(TAG, "screenHeight: "+screenHeight);
		double scale = (double)screenWidth / (double)screenHeight;
		int chose = 0;
		double diff = Math.abs(scale - scales[0]);
		for(int i=1;i<scales.length;i++){
			double temp = Math.abs(scale - scales[i]);
			if (temp < diff){
				chose = i;
				diff = temp;
			}
		}
		if (resIds.length > chose){
			img.setImageResource(resIds[chose]);
		}
		
		D.info(TAG, "chose: "+chose);
		new Handler().postDelayed(this, 2000);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Intent goMain = new Intent(this, MainActivity.class);
		startActivity(goMain);
		finish();
	}
	
	
}
