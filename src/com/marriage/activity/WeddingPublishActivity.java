package com.marriage.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.TopicCategory;
import com.marriage.bean.TopicCategoryInfo;
import com.marriage.bean.TopicTagCat;
import com.marriage.bean.TopicTagCatInfo;
import com.marriage.bean.UserInfo;
import com.marriage.dialog.WeddingProgressDialog;
import com.marriage.dialog.WeddingShareAddDialog;
import com.marriage.fragment.BaseFragment;
import com.marriage.fragment.WeddingShareFragment.CategoryRequest;
import com.marriage.utility.AppConfig;
import com.marriage.utility.BitmapFileUtil;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ToastHelper;
import com.marriage.widget.adapter.CommonBaseAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * With 603d1b1 compare
 * @author xydu
 *
 */
public class WeddingPublishActivity extends BaseActivity implements
		OnClickListener, OnItemClickListener {

	public final static String EXTRA_INPUT_URI = "extra_input_URI";
	private final static int CODE_REQUEST_TAKE_PHOTO = 0x07171;
	private final static int CODE_REQUEST_PICKER_IMAGE = 0x07172;
	private final static int CODE_REQUEST_PICKER_CITY = 0x07174;

	private GridView mGridView;
	private WeddingShareAddDialog mDialog;
	private List<Uri> mUri = new ArrayList<Uri>();
	private ImagesAdapter mAdapter;
	private TextView mCity, mTag, mTitle;
	private String mCityId;
	private PopupWindow mCurrentPop;
	private List<TopicCategory> mTopicCat;
	private CategoryRequest mTopicRequest;
	private CategoryAdapter mTopicCatAdapter;
	private TagAdapter mTagAdapter;
	private SparseArray<TopicTagCatInfo> mTagData = new SparseArray<TopicTagCatInfo>();
	private WeddingProgressDialog mLoadingDialog;
	private ListView list1, list2;
	
	private int mList1ChoicedPosition = -1, mList2ChoicedPosition = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Uri uri = (Uri) getIntent().getParcelableExtra(EXTRA_INPUT_URI);
		if (uri != null) {
			mUri.add(uri);
		}
		setContentView(R.layout.activity_publish_layout);

		findViewById(R.id.back_btn).setOnClickListener(this);
		
		findViewById(R.id.fabu_add_image).setOnClickListener(this);
		mGridView = (GridView) findViewById(R.id.fabu_gv);
		mGridView.setAdapter(mAdapter = new ImagesAdapter());
		mCity = (TextView) findViewById(R.id.fabu_show_city);
		mTitle = (TextView) findViewById(R.id.fabu_biaoti);
		mTag = (TextView) findViewById(R.id.fabu_show_type);

		mCity.setText(MApp.get().getUser().user.city_name);
		mCityId = MApp.get().getUser().user.city_id;
		
		findViewById(R.id.fabu_btn).setOnClickListener(this);
		findViewById(R.id.fabu_type).setOnClickListener(this);
		findViewById(R.id.fabu_city_btn).setOnClickListener(this);
		
		mCurrentPop = new PopupWindow(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		mCurrentPop.setFocusable(true);
		mCurrentPop.setBackgroundDrawable(new BitmapDrawable());
		mCurrentPop.setOutsideTouchable(true);
		mCurrentPop.setHeight(getResources().getDisplayMetrics().heightPixels / 3);
		mCurrentPop.update();
		
		View ctag = LayoutInflater.from(this).inflate(R.layout.category_tag_list, null);
		list1 = (ListView)ctag.findViewById(R.id.list1);
		list2 = (ListView)ctag.findViewById(R.id.list2);
		list1.setOnItemClickListener(this);
		list2.setOnItemClickListener(this);
		mTopicCatAdapter = new CategoryAdapter(this);
		list1.setAdapter(mTopicCatAdapter);
		list1.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		
		mTagAdapter = new TagAdapter(this);
		list2.setAdapter(mTagAdapter);
		
		mCurrentPop.setContentView(ctag);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back_btn:
			new AlertDialog.Builder(this)
					.setMessage(R.string.string_dialog_publish_give_up)
					.setPositiveButton(R.string.string_dialog_ok,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}

							})
					.setNegativeButton(R.string.string_dialog_cancel, null)
					.create().show();
			break;
		case R.id.fabu_add_image:
			if (mDialog == null) {
				mDialog = new WeddingShareAddDialog(this);
			}
			mDialog.show(getSupportFragmentManager(), "ADD_DIALOG");
			break;
		case R.id.fabu_btn:
			if(checkParams())
				return;
			goPublish();
			break;
		case R.id.fabu_type: // tag
			onTagBtnClick();
			break;
		case R.id.fabu_city_btn: { // citys
			Intent intent = new Intent(this, CityActivity.class);
			startActivityForResult(intent, CODE_REQUEST_PICKER_CITY);
			break;
		}
		case R.id.share_weixin: {
			mDialog.dismissAllowingStateLoss();
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(intent, CODE_REQUEST_TAKE_PHOTO);
			break;
		}
		case R.id.share_weixin_friends: {
			mDialog.dismissAllowingStateLoss();
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(intent, CODE_REQUEST_PICKER_IMAGE);
			break;
		}
		case R.id.share_cancle:
			mDialog.dismissAllowingStateLoss();
			break;
		case R.id.delete_btn:{
			Integer position = (Integer)v.getTag();
			if(position != null) {
				mUri.remove(position.intValue());
				mAdapter.notifyDataSetChanged();				
			}
			break;
		}
		}
	}
	
	private void onTagBtnClick() {
		if(mTopicCat == null) {
			String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPIC_CATEGORY;
			mTopicRequest = new CategoryRequest(Method.POST, url, new Listener<String>() {
				
				@Override
				public void onResponse(String reponse) {
					D.debug("分类", reponse);
					TopicCategoryInfo info;
					try {
						info = new Gson()
						.fromJson(reponse, TopicCategoryInfo.class);
					} catch (JsonSyntaxException e) {
						return;
					}
					mTopicCat = info.topic_cate_tag_ls;
					if (Constants.SUCCESS.equals(info.op_code)) {
						if (mTopicCatAdapter != null) {
							mTopicCatAdapter.setDatas(mTopicCat);
						}
					}
				}
			}, null);
			BaseFragment.getRequestQueue().add(mTopicRequest);
		}
		
		mCurrentPop.showAsDropDown(findViewById(R.id.fabu_type));
		
	}

	private void goPublish() {
		PublishRequest request = new PublishRequest(new Listener<String>() {

			@Override
			public void onResponse(String response) {
				D.error("MARTIN", response);
				
				if(mLoadingDialog != null && mLoadingDialog.isShowing())
					mLoadingDialog.dismiss();
				JSONObject jo;
				try {
					jo = new JSONObject(response);
					if(Constants.SUCCESS.equals(jo.opt("op_code"))) {
						ToastHelper.showShortToast("发布成功 :)");
						Intent data = new Intent();
						data.putExtra("reload", true);
						setResult(Activity.RESULT_OK, data);
						finish();
						return;
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				ToastHelper.showShortToast("错误啦 :(");
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				D.error("MARTIN", error.toString());
				if(mLoadingDialog != null && mLoadingDialog.isShowing())
					mLoadingDialog.dismiss();
				ToastHelper.showShortToast("错误啦 :(");
			}
		});
		
		BaseFragment.getRequestQueue().add(request);
		if(mLoadingDialog == null) {
			mLoadingDialog = new WeddingProgressDialog(this);
			mLoadingDialog.setCancelable(false);
		}
		if(mLoadingDialog.isShowing()) 
			mLoadingDialog.dismiss();
		
		mLoadingDialog.show();
		
	}

	private boolean checkParams() {
		if(mUri.size()>0){
			if(!TextUtils.isEmpty(mTitle.getText().toString().trim())){
				if(!TextUtils.isEmpty(mTag.getText().toString().trim())){
					if(!TextUtils.isEmpty(mCity.getText().toString().trim()) && !TextUtils.isEmpty(mCityId)){
						return false;
					}else{
						ToastHelper.showShortToast("请选择城市");
					}
				}else{
					ToastHelper.showShortToast( "请选择标签");
				}
			}else{
				ToastHelper.showShortToast("请输入标题");
			}
		}else{
			ToastHelper.showShortToast("请选择发布的图片");
		}
		return true;
	}
	

	private File generateImageOutputFile() {
		File file = new File(getCacheDir(), String.valueOf(System
				.currentTimeMillis()) + ".jpg");
		return file;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == CODE_REQUEST_TAKE_PHOTO) {
				if (data != null) {
					Bundle bundle = data.getExtras();
					if (bundle != null && bundle.get("data") != null) {
						Bitmap bitmap = (Bitmap) bundle.get("data");
						File fileName = generateImageOutputFile();
						FileOutputStream fos;
						try {
							fos = new FileOutputStream(fileName);
							bitmap.compress(Bitmap.CompressFormat.JPEG, 100,
									fos);
							mUri.add(Uri.fromFile(fileName));
							mAdapter.notifyDataSetChanged();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
			} else if (requestCode == CODE_REQUEST_PICKER_IMAGE) {
				Uri uri = data.getData();
				if (uri != null) {
					mUri.add(uri);
					mAdapter.notifyDataSetChanged();
				}
			} else if (requestCode == CODE_REQUEST_PICKER_CITY) {
				mCity.setText(data.getExtras().getString(CityActivity.KEY_CITY_NAME));
				mCityId = data.getExtras().getString(CityActivity.KEY_CITY_ID);
			}
		}
	};

	class ImagesAdapter extends BaseAdapter {

		public int getCount() {
			// TODO Auto-generated method stub
			return mUri.size();
		}

		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(int arg0, View arg1, ViewGroup arg2) {
			
			View view = LayoutInflater.from(arg2.getContext()).inflate(R.layout.item_grid_publish, null);
			ImageView imageView = (ImageView)view.findViewById(R.id.icon);
			View delete = (View)view.findViewById(R.id.delete_btn);
			delete.setOnClickListener(WeddingPublishActivity.this);
			delete.setTag(arg0);
			int width = arg2.getContext().getResources().getDisplayMetrics().widthPixels;
			int height = arg2.getContext().getResources().getDisplayMetrics().heightPixels;
			imageView.setLayoutParams(new RelativeLayout.LayoutParams(width / 5,
					height / 7));
			ImageLoader.getInstance().displayImage(mUri.get(arg0).toString(),
					imageView);
			return view;
		}
	}
	
	class PublishRequest extends StringRequest {

		
		public PublishRequest(Listener<String> listener, ErrorListener errorListener) {
			super(Method.POST, AppConfig.INSTANCE.SERVER + Constants.URL_ADD_TOPIC, listener, errorListener);
			setRetryPolicy(new DefaultRetryPolicy(
					Constants.IMAGE_UPLOAD_TIMEOUT,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		}

		@Override
		protected Map<String, String> getParams() throws AuthFailureError {
			HashMap<String, String> params = new HashMap<String, String>();
			UserInfo user = MApp.get().getUser();
			if (user != null) {
				params.put("user_id", user.user.user_id);
				params.put("user_name", user.user.user_name);
				params.put("city_id", mCityId);
				params.put("title", mTitle.getText().toString().trim());
				params.put("topic_cate_id",
						mTopicCat.get(mList1ChoicedPosition).tag_cate_id);
				params.put("tag_id",
						mTagData.get(mList1ChoicedPosition).topic_tag_ls
								.get(mList2ChoicedPosition).tag_tag_id);
				params.put("tag_name",
						mTagData.get(mList1ChoicedPosition).topic_tag_ls
								.get(mList2ChoicedPosition).tag_tag_name);
				generalImageData(params);
			}
			return params;
		}
		
		private void generalImageData(Map<String, String> data) {
			JSONObject j = new JSONObject();
			for (int i = 0; i < mUri.size(); i++) {
				try {
					j.put("topic_img" + (i+1), getBase64Data(i));
				} catch (JSONException e) {
					e.printStackTrace();
					return;
				}
			}
			data.put("topic_imgs", j.toString());
		}
		
		private String getBase64Data(int position) {
			Uri uri = mUri.get(position);
			InputStream in;
			String decodedString = "";
			try {
				in = getContentResolver().openInputStream(uri);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				int read;
				byte[] buffer = new byte[4096];
				while((read = in.read(buffer)) != -1) {
					baos.write(buffer, 0, read);
				}
				
				byte[] data = baos.toByteArray();
//				int testOldSize = data.length;
//				String test = "";
//				if (BuildConfig.DEBUG) {
//					Bitmap testBm = BitmapFactory
//							.decodeByteArray(data, 0, data.length);
//					test = "\n[old]bitmap内存占用:" + testBm.getAllocationByteCount()
//							+ "---[old]分辨率(W x H):" + testBm.getWidth() + "x"
//							+ testBm.getHeight();
//					if (test != null && !testBm.isRecycled()) {
//						testBm.recycle();
//						testBm = null;
//					}
//				}
				
				
				Bitmap bm = BitmapFileUtil.decodeBitmapFromByteArray(data, BitmapFileUtil.WIDTH_LIMIT, BitmapFileUtil.HEIGHT_LIMIT);
				
				baos = new ByteArrayOutputStream();
				bm.compress(CompressFormat.JPEG, 80, baos);
				data = baos.toByteArray();
				decodedString = new String(Base64.encode(data, 0, data.length, Base64.DEFAULT));
				
//				if (BuildConfig.DEBUG) {
//					D.debug("MARTIN",
//							"*****************\n图片" + (position + 1) + "\n[old]文件占用:"
//									+ testOldSize + test
//									+ ": \n[new]bitmap内存占用(decoded后800x480):"
//									+ bm.getAllocationByteCount()
//									+ "---[new]分辨率(W x H):" + bm.getWidth() + "x"
//									+ bm.getHeight()
//									+ "\n[new]文件占用(compress quilty 80):"
//									+ data.length + "\nBase64 size:"
//									+ decodedString.length());
//				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return decodedString;
		}
		
	}
	
	static class CategoryAdapter extends CommonBaseAdapter<TopicCategory> {


		public CategoryAdapter(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ChickableTextView v = (ChickableTextView) convertView;
			if (v == null) {
				v = new ChickableTextView(context);
				v.setPadding(CommonUtils.dip2Px(context, 15),
						CommonUtils.dip2Px(context, 10),
						CommonUtils.dip2Px(context, 5),
						CommonUtils.dip2Px(context, 10));
				v.setBackgroundColor(Color.WHITE);
				v.setTextSize(15);
				v.setTextColor(context.getResources().getColor(
						R.color.edit_text_color));
			}
			v.setText(getItem(position).toString());
			return v;
		}

	}
	
	static class TagAdapter extends CommonBaseAdapter<TopicTagCat> {
		public TagAdapter(Context context) {
			super(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ChickableTextView v = (ChickableTextView) convertView;
			if (v == null) {
				v = new ChickableTextView(context);
				v.setPadding(CommonUtils.dip2Px(context, 15),
						CommonUtils.dip2Px(context, 10),
						CommonUtils.dip2Px(context, 5),
						CommonUtils.dip2Px(context, 10));
				v.setBackgroundColor(Color.WHITE);
				v.setTextSize(15);
				v.setTextColor(context.getResources().getColor(
						R.color.edit_text_color));
			}
			v.setText(getItem(position).toString());
			return v;
		}
		
	}
	
	static class ChickableTextView extends TextView implements Checkable {

		public ChickableTextView(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void setChecked(boolean checked) {
			// TODO Auto-generated method stub
			if(checked) {
				setBackgroundColor(Color.parseColor("#FF1493"));
			} else {
				setBackgroundColor(Color.WHITE);
			}
		}

		@Override
		public boolean isChecked() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void toggle() {
			// TODO Auto-generated method stub
			
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if(parent.getId() == R.id.list1) {
			mList1ChoicedPosition = position;
			showList2(position);
		} else if (parent.getId() == R.id.list2) {
			mList2ChoicedPosition = position;
			mTag.setText(mTopicCat.get(mList1ChoicedPosition).tag_cate_name
					+ " -->"
					+ mTagData.get(mList1ChoicedPosition).topic_tag_ls
							.get(mList2ChoicedPosition).tag_tag_name);
			if(mCurrentPop != null && mCurrentPop.isShowing()) {
				mCurrentPop.dismiss();
			}
		}
		
	}
	
	private void showList2(final int position) {
		TopicTagCatInfo tag = mTagData.get(position);
		if(tag == null) {
			String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPIC_TAG_CATEGORY;;
			StringRequest request = new StringRequest(Method.POST, url, new TagListener(position), null) {

				@Override
				protected Map<String, String> getParams()
						throws AuthFailureError {
					HashMap<String, String> params = new HashMap<String, String>();
					params.put("topic_cate_id", mTopicCat.get(position).tag_cate_id);
					return params;
				}
				
			};
			mTagAdapter.setDatas(new ArrayList<TopicTagCat>());
			BaseFragment.getRequestQueue().add(request);
		} else {
			if(tag.topic_tag_ls == null) {
				tag.topic_tag_ls = new ArrayList<TopicTagCat>();
			}
			mTagAdapter.setDatas(tag.topic_tag_ls);
		}
	}
	
	class TagListener implements Listener<String> {

		int cPosition;
		
		public TagListener(int position) {
			cPosition = position;
		}
		
		
		@Override
		public void onResponse(String response) {

			D.debug("分类", response);
			TopicTagCatInfo info;
			try {
				info = new Gson()
					.fromJson(response, TopicTagCatInfo.class);
			} catch (JsonSyntaxException e) {
				return;
			}
			if (Constants.SUCCESS.equals(info.op_code)) {
				mTagData.put(cPosition, info);
				if(mList1ChoicedPosition == cPosition) {
					if(info.topic_tag_ls == null) {
						info.topic_tag_ls = new ArrayList<TopicTagCat>();
					}
					mTagAdapter.setDatas(info.topic_tag_ls);
				}
			}
		}
	}
}
