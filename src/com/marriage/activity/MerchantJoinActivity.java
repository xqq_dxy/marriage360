package com.marriage.activity;

import android.os.Bundle;

import com.marriage.fragment.MerchantJoinFragment;

public class MerchantJoinActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			MerchantJoinFragment mjf = new MerchantJoinFragment();
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, mjf, MerchantJoinFragment.TAG)
					.commit();
		}
	}

}
