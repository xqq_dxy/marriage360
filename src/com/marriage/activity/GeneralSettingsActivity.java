package com.marriage.activity;

import com.marriage.fragment.GeneralSettingsFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

public class GeneralSettingsActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		if (arg0 == null) {
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			ft.add(android.R.id.content,
					Fragment.instantiate(this,
							GeneralSettingsFragment.class.getName())).commit();
		}
	}

}
