package com.marriage.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue.RequestFilter;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.dialog.WeddingDialog;
import com.marriage.dialog.WeddingProgressDialog;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;

public class BaseActivity extends FragmentActivity {

	private WeddingProgressDialog mWeddingProgressDialog;

	public void showLoading() {

		if (mWeddingProgressDialog != null
				&& mWeddingProgressDialog.isShowing()) {
			mWeddingProgressDialog.dismiss();
		}
		mWeddingProgressDialog = new WeddingProgressDialog(this);
		mWeddingProgressDialog.show();
	}

	public void endLoading() {

		if (mWeddingProgressDialog != null
				&& mWeddingProgressDialog.isShowing()) {
			mWeddingProgressDialog.dismiss();
		}
	}

	@Override
	public void onPause() {
		MApp.get().getRequestQueue().cancelAll(filter);
		super.onPause();
		unregisterUpdateReceiver();
	}

	private RequestFilter filter = new RequestFilter() {

		@Override
		public boolean apply(Request<?> request) {
			// TODO Auto-generated method stub
			String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_TOPICS;
			if(url.equals(request.getUrl())) return false;
			return true;
		}

	};

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		registerUpdateReceiver();
	}

	private void registerUpdateReceiver() {
		IntentFilter filter = new IntentFilter(Constants.ACTION_NEW_UPDATE);
		registerReceiver(updateReceiver, filter);
	}

	private void unregisterUpdateReceiver() {
		try {
			unregisterReceiver(updateReceiver);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private BroadcastReceiver updateReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			endLoading();
			if (intent == null) {
				return;
			}
			String action = intent.getAction();
			if (!Constants.ACTION_NEW_UPDATE.equals(action)) {
				return;
			}
			String fileUrl = intent
					.getStringExtra(Constants.BUNDLE_UPDATE_FILE_URL);
			if (TextUtils.isEmpty(fileUrl)) {
				return;
			}
			showUpdateDialog(fileUrl);
		}
	};

	private void showUpdateDialog(final String fileUrl) {
		new WeddingDialog.Builder(this)
				.setMessage(R.string.find_new_version)
				.setNegativeButton(R.string.cancel, null)
				.setPositiveButton(R.string.confirm,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Intent fire = new Intent(Intent.ACTION_VIEW);
								fire.setData(Uri.parse(fileUrl));
								startActivity(fire);
							}
						}).show();
	}
}
