package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.fragment.WebFragment;

public class WeddingWebViewActivity extends BaseActivity {
	
	WebFragment wf;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			wf = new WebFragment();
			Intent incoming = getIntent();
			if (incoming != null) {
				wf.setArguments(incoming.getExtras());
			}
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content, wf,
							WebFragment.class.getSimpleName()).commit();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (!wf.onBackPressed()){
			super.onBackPressed();
		}
	}
}
