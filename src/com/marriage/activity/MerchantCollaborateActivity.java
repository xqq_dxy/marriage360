package com.marriage.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.marriage.R;

public class MerchantCollaborateActivity extends Activity implements OnClickListener {
	
	private TextView tel;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_he_zuo);
        
        findViewById(R.id.back_btn).setOnClickListener(this);
        tel = (TextView) findViewById(R.id.about_tel);
        tel.setOnClickListener(this);
        String text = getString(R.string.explain_24);
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new UnderlineSpan(), 0, ss.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tel.setText(ss);
    }
    
    @Override
    public void onClick(View v){
    	switch (v.getId()) {
		case R.id.back_btn:
			finish();
			break;
		case R.id.about_tel:
			String phone = getString(R.string.hunjia_phone);
			call(phone);
			break;
		}
    }
    
    private void call(String number){
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" + number));
		startActivity(intent);
	}
}
