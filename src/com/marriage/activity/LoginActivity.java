package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.delegate.LoginDelegate;
import com.marriage.fragment.LoginFragment;
import com.sina.weibo.sdk.auth.sso.SsoHandler;

public class LoginActivity extends BaseActivity {

	private LoginDelegate loginDelegate;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		loginDelegate = new LoginDelegate(this);
		if (arg0 == null){
			LoginFragment lf = LoginFragment.newInstance(loginDelegate);
			if (getIntent() != null){
				lf.setArguments(getIntent().getExtras());
			}
			getSupportFragmentManager().beginTransaction().add(android.R.id.content, lf, LoginFragment.class.getSimpleName()).commit();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		SsoHandler h = loginDelegate.getWeiboSsoHandler();
		if (h != null){
			h.authorizeCallBack(requestCode, resultCode, data);
		} 
	}

}
