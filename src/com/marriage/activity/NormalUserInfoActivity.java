package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.fragment.NormalUserFragment;

public class NormalUserInfoActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			NormalUserFragment nuf = new NormalUserFragment();
			Intent incoming = getIntent();
			if (incoming != null) {
				nuf.setArguments(incoming.getExtras());
			}
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, nuf, NormalUserFragment.TAG)
					.commit();

		}

	}
}
