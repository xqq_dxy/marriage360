package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.fragment.MyFocusFragment;

public class MyFocusActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			MyFocusFragment mff = new MyFocusFragment();
			Intent data = getIntent();
			if (data != null) {
				mff.setArguments(data.getExtras());
			}
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, mff, MyFocusFragment.TAG)
					.commit();
		}
	}
}
