package com.marriage.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.marriage.R;
import com.marriage.delegate.StepMakeCard;
import com.marriage.delegate.StepMakeCardFactory;
import com.marriage.dialog.WeddingDialog;

public class MakeCardActivity extends BaseActivity {

	private int mStep;
	private StepMakeCard mMakeCard;
	private String mXTid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		generatIntentData(getIntent());
		mMakeCard = StepMakeCardFactory.newStepMakeCard(mStep);
		mMakeCard.init(this, mXTid);
		mMakeCard.onCreate();
	}

	private void generatIntentData(Intent data) {
		if (data == null) {
			return;
		}
		mStep = data.getIntExtra(StepMakeCard.EXTRA_MAKE_CARD_STEP,
				StepMakeCard.MAKE_CARD_STEP_INPUT_CODE);
		mXTid = data.getStringExtra(StepMakeCard.EXTRA_CARD_ID);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (mMakeCard != null){
			mMakeCard.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void onBackPressed() {
		if(mMakeCard.backPressed()) {
			WeddingDialog dialog = new WeddingDialog.Builder(this)
			.setTitle(R.string.string_use_card_code_hint)
			.setMessage("确定退出制作吗？")
			.setPositiveButton(R.string.string_dialog_ok,
					new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					finish();
				}
			})
			.setNegativeButton(R.string.string_dialog_cancel, null)
			.create();
			dialog.show();
		} else {
			super.onBackPressed();
		}

	}
	
}
