package com.marriage.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.marriage.MApp;
import com.marriage.R;
import com.marriage.dialog.WeddingDialog;
import com.marriage.fragment.BaseFragment;
import com.marriage.fragment.ECardFragment;
import com.marriage.fragment.MerchantFragment;
import com.marriage.fragment.MineFragment;
import com.marriage.fragment.WeddingShareFragment;
import com.marriage.update.UpdateService;
import com.marriage.utility.Constants;
import com.marriage.widget.AwesomeFragmentTabHost;
import com.marriage.widget.AwesomeFragmentTabHost.InterceptTabListener;

public class MainActivity extends BaseActivity implements InterceptTabListener{

	private AwesomeFragmentTabHost mTabHost;

	private boolean hasCheckedUpdate = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MApp.get().refereshLocation();
		
		setContentView(R.layout.activity_home_tab_content_layout);
		mTabHost = (AwesomeFragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(getString(R.string.string_wedding_share)),
						R.drawable.selector_tab_index_bg), WeddingShareFragment.class,
				null);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(getString(R.string.string_merchant)),
						R.drawable.selector_tab_second_bg), MerchantFragment.class, null);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(getString(R.string.string_wedding_card)),
						R.drawable.selector_tab_third_bg), ECardFragment.class, null);
		mTabHost.addTab(
				setIndicator(this, mTabHost.newTabSpec(getString(R.string.string_mine)),
						R.drawable.selector_tab_fourth_bg), MineFragment.class, null);
		mTabHost.setInterceptTabListener(this);
		
		initIntentData();
	}
	
	private void initIntentData(){
		Intent incoming = getIntent();
		if (incoming == null){
			return;
		}
		int defaultTab = incoming.getIntExtra(Constants.BUNDLE_MAIN_TAB_KEY, 0);
		if (defaultTab > 0){
			mTabHost.setCurrentTab(defaultTab);
		}
	}

	public TabSpec setIndicator(Context ctx, TabSpec spec, int resid) {
		// TODO Auto-generated method stub
		TextView v = (TextView) LayoutInflater.from(ctx).inflate(
				R.layout.activity_home_tab_item, mTabHost.getTabWidget(), false);
		v.setCompoundDrawablesWithIntrinsicBounds(0, resid, 0, 0);
		v.setText(spec.getTag());
		return spec.setIndicator(v);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!hasCheckedUpdate){
			hasCheckedUpdate = true;
			Intent startUpdateCheck = new Intent(this, UpdateService.class);
			startService(startUpdateCheck);
		}
	}

	@Override
	public void onBackPressed() {
		Fragment frg =  getSupportFragmentManager().findFragmentByTag(mTabHost.getCurrentTabTag());
		if(frg != null && frg instanceof BaseFragment) {
			if(((BaseFragment)frg).onBackPressed()) {
				return;
			}
		}
		if(mTabHost.getCurrentTab() != 0){
			mTabHost.setCurrentTab(0);
			return;
		}
		showQuitConfirmDialog();
//		super.onBackPressed();
	}

	@Override
	public boolean onInterceptTab(String tabId) {
		// TODO Auto-generated method stub
		if (tabId.equals(getString(R.string.string_mine))){
			if (MApp.get().getUser() != null && MApp.get().getUser().user != null){
				return false;
			}
			Intent goLogin = new Intent(this, LoginActivity.class);
			startActivity(goLogin);
			return true;
		}
		return false;
	}
	
	public void onLogout(){
		mTabHost.setCurrentTab(0);
	}
	
	@Override
	protected void onNewIntent(final Intent intent) {
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				final String cTabTag = mTabHost.getCurrentTabTag();
				Fragment frg = (BaseFragment) getSupportFragmentManager()
						.findFragmentByTag(cTabTag);
				if (frg != null && frg instanceof BaseFragment) {
					((BaseFragment) frg).onNewIntent(intent);
				}
			}

		});
	}

	public void setCurrentTab(int position){
		mTabHost.setCurrentTab(position);
	}
	
	private void showQuitConfirmDialog(){
		WeddingDialog dialog = new WeddingDialog.Builder(this).setMessage(R.string.confirm_quit).setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						finish();
					}
				},0);
				
			}
		}).setNegativeButton(R.string.cancel, null).create();
		dialog.show();
	}
	
}
