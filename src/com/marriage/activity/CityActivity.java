package com.marriage.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.marriage.OnCityChoiceListener;
import com.marriage.R;
import com.marriage.bean.City;
import com.marriage.fragment.CityFragment;

/**
 * 
 * @author Martin
 * 
 */
public class CityActivity extends BaseActivity implements OnCityChoiceListener {

	public static final String KEY_CITY_NAME = "city_name";
	public static final String KEY_CITY_ID = "city_id";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_city);
		if (savedInstanceState == null) {
			CityFragment frg = new CityFragment();
			frg.setOnCityChoiceListener(this);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.city_content, frg).commit();
		}

		findViewById(R.id.back_btn).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void onChoice(City city) {
		Intent in = new Intent();
		in.putExtra(CityActivity.KEY_CITY_NAME, city.name);
		in.putExtra(CityActivity.KEY_CITY_ID, city.id);
		setResult(Activity.RESULT_OK, in);
		finish();
	}
}
