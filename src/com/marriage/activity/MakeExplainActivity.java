package com.marriage.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.marriage.R;

public class MakeExplainActivity extends Activity implements OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_explain);
        findViewById(R.id.back_btn).setOnClickListener(this);
        
    }
    
    @Override
    public void onClick(View v){
    	switch (v.getId()) {
		case R.id.back_btn:
			finish();
			break;
		}
    }
}
