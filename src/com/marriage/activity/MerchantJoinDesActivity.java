package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.fragment.MerchantJoinDesFragment;

public class MerchantJoinDesActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			MerchantJoinDesFragment mjdf = new MerchantJoinDesFragment();
			Intent incoming = getIntent();
			if (incoming != null) {
				mjdf.setArguments(incoming.getExtras());
			}
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content, mjdf,
							MerchantJoinDesFragment.TAG).commit();
		}
	}
}
