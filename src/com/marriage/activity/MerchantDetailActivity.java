package com.marriage.activity;

import android.content.Intent;
import android.os.Bundle;

import com.marriage.fragment.MerchantDetailFragment;

public class MerchantDetailActivity extends BaseActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		if (arg0 == null) {
			MerchantDetailFragment fragment = new MerchantDetailFragment();
			Intent inComing = getIntent();
			if (inComing != null) {
				fragment.setArguments(inComing.getExtras());
			}
			getSupportFragmentManager()
					.beginTransaction()
					.add(android.R.id.content, fragment,
							MerchantDetailFragment.TAG).commit();
		}
	}
}
