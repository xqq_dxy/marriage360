package com.marriage.utility;

import android.widget.Toast;

import com.marriage.MApp;

public class ToastHelper {

	public static Toast showShortToast(String text){
		Toast t = Toast.makeText(MApp.get(), text, Toast.LENGTH_SHORT);
		t.show();
		return t;
	}
	
	public static Toast showShortToast(int textId){
		String text = MApp.get().getString(textId);
		return showShortToast(text);
	}
	
	public static Toast showLongToast(String text){
		Toast t = Toast.makeText(MApp.get(), text, Toast.LENGTH_LONG);
		t.show();
		return t;
	}
	
	public static Toast showLongToast(int textId){
		String text = MApp.get().getString(textId);
		return showLongToast(text);
	}
	
}
