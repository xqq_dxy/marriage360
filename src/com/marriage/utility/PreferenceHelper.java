package com.marriage.utility;

import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.marriage.MApp;

public class PreferenceHelper {

	public class PreferenceKeys{
		public static final String USER_INFO = "userInfo";
		public static final String CHOICED_CITY_ID = "choiced_city_id";
		public static final String CHOICED_CITY_NAME = "choiced_city_name";
		public static final String CHOICED_CATEGORY_ID = "choiced_category_id";
		public static final String CHOICED_CATEGORY_NAME = "choiced_category_name";
		public static final String CHOICED_SORT_ID = "choiced_sort_id";
		public static final String CHOICED_SORT_NAME = "choiced_sort_name";
		
		public static final String CONTACT_PEOPLE = "contact_people";
		
		public static final String SETTING_ENABLE_MOBILE_NET_DOWNLOAD_IMAGE = "setting_enable_mobile_net_download_image";
		public static final String SETTING_ENABLE_PUSH = "setting_enable_push";
		
	}
	
	public static void setInt(String key, int value) {
		PreferenceManager.getDefaultSharedPreferences(MApp.get()).edit()
				.putInt(key, value).commit();
	}

	public static void setString(String key, String value) {
		PreferenceManager.getDefaultSharedPreferences(MApp.get()).edit()
				.putString(key, value).commit();
	}

	public static void setBoolean(String key, boolean value) {
		PreferenceManager.getDefaultSharedPreferences(MApp.get()).edit()
				.putBoolean(key, value).commit();
	}

	public static void setLong(String key, long value) {
		PreferenceManager.getDefaultSharedPreferences(MApp.get()).edit()
				.putLong(key, value).commit();
	}

	public static void setFloat(String key, float value) {
		PreferenceManager.getDefaultSharedPreferences(MApp.get()).edit()
				.putFloat(key, value).commit();
	}

	public static int getInt(String key, int defValue) {
		return PreferenceManager.getDefaultSharedPreferences(MApp.get())
				.getInt(key, defValue);
	}

	public static String getString(String key, String defValue) {
		return PreferenceManager.getDefaultSharedPreferences(MApp.get())
				.getString(key, defValue);
	}

	public static boolean getBoolean(String key, boolean defValue) {
		return PreferenceManager.getDefaultSharedPreferences(MApp.get())
				.getBoolean(key, defValue);
	}

	public static long getLong(String key, long defValue) {
		return PreferenceManager.getDefaultSharedPreferences(MApp.get())
				.getLong(key, defValue);
	}

	public static float getFloat(String key, float defValue) {
		return PreferenceManager.getDefaultSharedPreferences(MApp.get())
				.getFloat(key, defValue);
	}
	
	public static void putString(String key, String value) {
		PreferenceManager.getDefaultSharedPreferences(MApp.get()).edit()
				.putString(key, value).commit();
	}
}
