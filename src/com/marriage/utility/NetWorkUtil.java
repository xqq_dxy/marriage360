/**
 * 
 */
package com.marriage.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;

/**
 * <p>开启网络连接</p>
 * @author Your name
 * @version $Id: NetWorkUtil.java, v 0.1 2013-6-8 上午11:24:15 fanmanrong Exp $
 */
public class NetWorkUtil {
    /**
     * 判断网络是否连通
     * 
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * 获得网络类型
     * @param context
     * @return 
     */
    public static String getNetworkType(Context context) {
		String type = "";
		ConnectivityManager conMan = (ConnectivityManager) 
				context.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (conMan == null
				|| conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) == null
				|| conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI) == null)
			return "unknown";

		// mobile
		State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState();

		// wifi
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.getState();

		if (mobile == State.CONNECTED || mobile == State.CONNECTING) {
			// type = "mobile";
			type = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
					.getSubtypeName();
			if (type == null || "".endsWith(type)) {
				type = "mobile";
			}
		} else if (wifi == State.CONNECTED || wifi == State.CONNECTING) {
			// type = "wifi";
			type = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
					.getSubtypeName();
			if (type == null || "".endsWith(type)) {
				type = "wifi";
			}
		} else {
			type = "unknown";
		}
		return type;
	}

}
