package com.marriage.utility;

public class PageRecord {
	int pageIndex;
	final int pageSize;
	int totalSize;
	int totalPagerNum;
	
	public PageRecord(int initialPageIndex, int pageSize) {
		if(initialPageIndex <= 0) {
			initialPageIndex = 1;
		}
		if(pageSize <= 0) {
			pageSize = 10;
		}
		this.pageIndex = initialPageIndex;
		this.pageSize = pageSize;
		this.totalPagerNum = 1;
	}
	
	
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
		this.totalPagerNum = (int) Math.ceil(totalSize / pageSize);
	}
	
	public boolean hasNextPager() {
		return totalPagerNum >= pageIndex;
	}
	
	public void preNextPager() {
		pageIndex++;
	}
	
	public int currentPageIndex() {
		return pageIndex;
	}
	
	public int getPageSize() {
		return pageSize;
	}
	
	public void clear() {
		this.pageIndex = 1;
		this.totalSize = 0;
		this.totalPagerNum = 1;
	}
	
}
