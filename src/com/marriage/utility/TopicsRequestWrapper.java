package com.marriage.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.bean.Topics;
import com.marriage.bean.TopicsInfo;

public class TopicsRequestWrapper implements Listener<String>, ErrorListener {
	PageRecord page;
	TopicReponse reponse;
	RequestQueue queue;
	String cityId;
	String categoryId, categoryTagName;
	String sortId;
	StringRequest request;
	List<Topics> topics = new ArrayList<Topics>();

	public TopicsRequestWrapper(RequestQueue queue, TopicReponse reponse, String cityId, String categoryId, String categoryTagName, String sortId) {
		request = new StringRequest(Method.POST, AppConfig.INSTANCE.SERVER
				+ Constants.URL_GET_TOPICS, this, this) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> map = new HashMap<String, String>();
				String userId = MApp.get().isLogin() ? MApp.get().getUser().user.user_id : "";
				map.put("fun_page", "1");
				map.put("user_id", userId);
				map.put("tag_name", TopicsRequestWrapper.this.categoryTagName);
				map.put("cate_id", TopicsRequestWrapper.this.categoryId);
				map.put("sort", TopicsRequestWrapper.this.sortId);
				map.put("city_id", TopicsRequestWrapper.this.cityId);
				map.put("page_index", String.valueOf(page.currentPageIndex()));
				map.put("page_size", String.valueOf(page.getPageSize()));
				D.error("MARTIN", map.toString());
				return map;
			}
		};
		page = new PageRecord(1, 20);
		this.queue = queue;
		this.reponse = reponse;
		this.cityId = cityId;
		this.categoryId = categoryId;
		this.categoryTagName = categoryTagName;
		this.sortId = sortId;
	}

	public interface TopicReponse {
		void onReponse(List<Topics> topics);
		void onErrorReponse(VolleyError error);
	}

	public boolean addQueue() {
		if (page.hasNextPager()) {
			queue.add(request);
			return true;
		}
		return false;
	}
	
	public boolean emptyAddQueue() {
		if((topics == null || topics.isEmpty())) {
			queue.add(request);
			return true;
		}
		return false;
	}
	
	public boolean onCityChanged(String cityId) {
		if(!TextUtils.equals(this.cityId, cityId)) {
			clear();
			this.cityId = cityId;
			addQueue();
			return true;
		}
		return false;
	}
	
	public boolean onCategoryChanged(String categoryId, String categoryTagName) {
		if(!TextUtils.equals(this.categoryId, categoryId) || !TextUtils.equals(this.categoryTagName, categoryTagName)) {
			clear();
			this.categoryId = categoryId;
			this.categoryTagName = categoryTagName;
			return addQueue();
		}
		return false;
	}
	
	public boolean onSortChanged(String sortId) {
		if(!TextUtils.equals(this.sortId, sortId)) {
			clear();
			this.sortId = sortId;
			addQueue();
			return true;
		}
		return false;
	}
	
	public void clear() {
		topics.clear();
		page.clear();
	}

	@Override
	public void onResponse(String reponse) {
		D.debug("TOPICS", reponse);
		/*File file = new File("/sdcard/logcat.txt");
		if(file.exists()) {
			file.delete();
		}
		try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			OutputStream os = new FileOutputStream(file);
			os.write(reponse.getBytes());
			os.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		TopicsInfo tops = null;
		try {
			tops = new Gson().fromJson(reponse, TopicsInfo.class);
		} catch (JsonSyntaxException e) {
			this.reponse.onErrorReponse(new VolleyError(e));
			return;
		} catch (NumberFormatException e) {
			this.reponse.onErrorReponse(new VolleyError(e));
			return;
		}
		
		if (Constants.SUCCESS.equals(tops.op_code)) {
			if(tops.topic_list != null) {
				int totelNum = Integer.valueOf(tops.topic_list_total);
				page.setTotalSize(totelNum);
				page.preNextPager();
				if(tops.topic_list != null)
					topics.addAll(tops.topic_list);
				this.reponse.onReponse(this.topics);
			} else {
				ToastHelper.showShortToast("么有数据 :(");
				this.reponse.onErrorReponse(new VolleyError(tops.op_info));
			}
			
		} else {
			ToastHelper.showShortToast(tops.op_info);
			this.reponse.onErrorReponse(new VolleyError(tops.op_info));
		}
	}

	public List<Topics> getTopicsList() {
		return topics;
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		ToastHelper.showShortToast("网络不给力");
		reponse.onErrorReponse(error);
	}
}
