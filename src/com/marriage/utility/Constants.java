package com.marriage.utility;

public class Constants {

	public static final int AVATAR_REAL_SIZE = 200;
	public static final int AVATAR_QUALITY = 100;
	
	public static final int IMAGE_UPLOAD_TIMEOUT = 15000;
	
	/**API URLS**/
	/** 首页轮换图片列表 **/
	public static final String URL_GET_ADVLS = "/AppIndex/getadvls";
	public static final String URL_REGISTER = "/User/register";
	public static final String URL_GET_CITY = "/City/getcityls";
	public static final String URL_LOGIN = "/User/login";
	public static final String URL_MERCHANT_LIST = "/SupplierAccount/getsals";
	public static final String URL_MERCHANT_CATEGORY = "/Cate/getcatels";
	public static final String URL_GET_TOPIC_CATEGORY = "/TopicTagCate/gettopiccatels";
	public static final String URL_GET_TOPIC_TAG_CATEGORY = "/TopicTagCate/gettopiccatetagls";
	public static final String URL_GET_TOPICS = "/Topic/gettopicls";
	public static final String URL_MERCHANT_DETAIL = "/SupplierAccount/getsadtldtl";
	public static final String URL_TOPICS_DETAIL = "/Topic/gettopicdtl";
	public static final String URL_ADD_TOPIC = "/Topic/addtopic";
	public static final String URL_SEND_COMMENT = "/SupplierAccountDP/commentSA";
	public static final String URL_MERCHANT_SUB_CATEGORY = "/Cate/getCateTypeLs";
	public static final String URL_ADD_FOLLOW = "/User/focus";
	public static final String URL_GET_TOPIC_COMMENT = "/Topic/gettopicdtlreply";
	public static final String URL_TOPIC_COMMENT = "/Topic/commentTopic";
	public static final String URL_MERCHANT_JOIN = "/SupplierAccount/locationSA";
	public static final String URL_MERCHANT_PERSONAL_INFO = "/SupplierAccount/saPersonalInfo";
	public static final String URL_QQ_LOGIN = "/User/qqLogin";
	public static final String URL_SINA_LOGIN = "/User/sinaLogin";
	public static final String URL_FEED_BACK = "/User/feedback";
	public static final String URL_CALL_CLICK = "/SupplierAccount/clickPhone";
	public static final String URL_CLAIM_MERCHANT = "/SupplierAccount/claimLocation";
	public static final String URL_USER_LIKE = "/Topic/favTopic";
	public static final String URL_GET_FANS = "/SupplierAccount/getSAFans";
	public static final String URL_GET_FOCUS = "/SupplierAccount/getFocusLs";
	public static final String URL_GET_MERCHANT_COMMENT_LIST = "/SupplierAccountDP/getCommentSALs";
	public static final String URL_GET_XITIE_LIST = "/Xitie/getUserXtList";
	public static final String URL_SET_CARD_INFO = "/Xitie/setXtInfo";
	public static final String URL_GET_CARD_INFO = "/Xitie/getXtInfo";
	public static final String URL_GET_CARD_MUSIC_LIST = "/Xitie/getBgMusicList";
	public static final String URL_SET_CARD_MUSIC = "/Xitie/setBgMusic";
	public static final String URL_CARD_DELETE_IMG = "/Xitie/deleteImage";
	public static final String URL_CARD_UPLOAD_IMG = "/Xitie/uploadImage";
	public static final String URL_CARD_CHECK_CODE = "/Xitie/checkCode";
	public static final String URL_CARD_GET_THEME = "/Xitie/getThemeList";
	public static final String URL_CARD_SET_THEME = "/Xitie/setTheme";
	public static final String URL_CARD_GET_ADV = "/Xitie/getAdv";
	public static final String URL_CARD_PREVIEW = "/Xitie/previewXt";
	public static final String URL_DEL_TOPIC = "/Topic/delTopic";
	public static final String URL_GET_USER_INFO = "/User/getUserInfo";
	public static final String URL_GET_CITY_INFO = "/AppIndex/getcityinfo";
	public static final String URL_UPDATE = "/AppIndex/getNewVersion";
	

	/**API RETURN CODE**/
	public static final String SUCCESS = "100";
	public static final String COMMON_ERR = "200";
	
	
	/** Preference Cache key **/
	public static final String CACHE_MERCHANT_CATEGORY = "merchant_category";
	public static final String CACHE_MERCHANT_SUB_CATEGORY = "merchant_sub_category";
	
	/**
	 * Volley 的缓存时间
	 */
	public static final long MAX_AGE_CITY = 1 * 24 * 60 * 60;		//城市 cache 1 days
	public static final long MAX_AGE_TOPIC_CATEGORY = 1 * 24 * 60 * 60;		//首页分类
	
	public static final int SOCKET_TIMEOUT = 5000;
	
	public static final String BUNDLE_MERCHANT_NAME = "merchant_name";
	public static final String BUNDLE_MERCHANT_LOC_ID = "sa_loc_id";
	public static final String BUNDLE_MAIN_TAB_KEY = "main_tab_key";
	
	public final static String PACKAGE_NAME_SING_WEIBO = "com.sina.weibo";
	public final static String PACKAGE_NAME_TENCENT_QQ = "com.tencent.mobileqq";
	
	/**
	 * 分享信息
	 */
	public static final String APP_ID_WEIXIN = "wx6354f3784abf6e25";
	public static final String SHARE_SMS_CONTENT = "我们结婚啦！！ 新郎Micky 新娘Donald 诚邀您于2014-6-25 在Shangrila参加喜宴！静候光临，感谢@360婚嫁网.";
	public static final String SHARE_EMAIL_SUBJECT = "喜帖";
	public static final String SHARE_EMAIL_CONTENT = "我们结婚啦！！ 新郎Micky 新娘Donald 诚邀您于2014-6-25 在Shangrila参加喜宴！静候光临，感谢@360婚嫁网.";
	
	public static final String SHARE_CONTENT = "我们结婚啦！！ 新郎Micky 新娘Donald 诚邀您于2014-6-25 在Shangrila参加喜宴！静候光临，感谢@360婚嫁网.";
	public static final String SHARE_CONTENT_HOLD_PLACE = "我们结婚啦！！ 新郎\"%s\"新娘\"%s\"诚邀您于%s %s:%s在%s参加喜宴！静候光临，感谢@360婚嫁网. %s";
	public static final String SHARE_TITLE = "360婚嫁网";
	
	public static final String XITIE_TEMPLATE = "http://www.360hunjia.com/show/1.html";
	
	public static final String BUNDLE_LIST_TYPE = "list_type";
	public static final String BUNDLE_WEBVIEW_TITLE = "web_title";
	public static final String BUNDLE_WEBVIEW_URL = "web_url";
	public static final String BUNDLE_USER_ID = "user_id";
	public static final String BUNDLE_USER_NAME = "user_name";
	public static final String BUNDLE_USER_AVATAR = "user_avatar";
	public static final String BUNDLE_USER_CITY = "user_city";
	public static final String BUNDLE_FANS_FROM_MERCHANT = "is_from_merchant_detail";
	public static final String BUNDLE_UPDATE_FILE_URL = "update_file_url";
	public static final String BUNDLE_TOPIC_ID = "topic_id";
	
	public static final String REFERENCE_CATEGORY_NAME = "category_name";
	public static final String REFERENCE_CATEGORY_ID = "category_id";
	
	public static final String ACTION_NEW_UPDATE = "com.marriage.update.NEW_UPDATE";
	public static final String ACTION_NO_UPDATE = "com.marriage.update.NO_UPDATE";
	public static final String ACTION_CANCEL_PRAISE = "com.marriage_CANCEL_PRAISE";
}
