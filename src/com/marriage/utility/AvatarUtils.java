package com.marriage.utility;

import java.io.File;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.marriage.MApp;
import com.marriage.R;
import com.marriage.dialog.WeddingDialog;

public class AvatarUtils {

	public static final int AVATAR_WIDTH = 200;
	public static final int AVATAR_HEIGHT = 200;

	public static void openCamera(Fragment f, int requestCode) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE, null);
		intent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(new File(FileUtil.createPhotoPath())));
		try {
			f.startActivityForResult(intent, requestCode);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(MApp.get(), R.string.no_available_app,
					Toast.LENGTH_SHORT).show();
		}
	}

	public static void openGalleryForUri(Fragment f, int requestCode) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("image/*");

		f.startActivityForResult(intent, requestCode);
	}

	public static void openGallery(Fragment f, int requestCode) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("image/*");
		fillIntentCrop(intent);
		try {
			f.startActivityForResult(intent, requestCode);
		} catch (ActivityNotFoundException e) {
			new WeddingDialog.Builder(f.getActivity())
					.setTitle(R.string.dialog_prompt)
					.setMessage(R.string.settings_photo_picker_not_found)
					.setPositiveButton(R.string.confirm, null).show();
		}
	}

	public static void openGalleryStoreUri(Fragment f, int requestCode, Uri uri) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("return-data", false);
		intent.putExtra("outputX", AVATAR_WIDTH * 2);
		intent.putExtra("outputY", AVATAR_HEIGHT * 2);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		try {
			f.startActivityForResult(intent, requestCode);
		} catch (ActivityNotFoundException e) {
			new WeddingDialog.Builder(f.getActivity())
					.setTitle(R.string.dialog_prompt)
					.setMessage(R.string.settings_photo_picker_not_found)
					.setPositiveButton(R.string.confirm, null).show();
		}
	}

	public static void openGallery(Fragment f, int requestCode, Uri url) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		Uri uri = url;
		if (uri == null) {
			MediaScannerConnection.scanFile(f.getActivity(),
					new String[] { FileUtil.getPhotoDir() }, null, null);
			File photoImage = new File(FileUtil.getPhotoPath());
			if (photoImage.exists()) {
				uri = Uri.fromFile(photoImage);
			} else {
				return;
			}
		}
		intent.setDataAndType(uri, "image/*");
		fillIntentCrop(intent);
		try {
			f.startActivityForResult(intent, requestCode);
		} catch (ActivityNotFoundException e) {
			new WeddingDialog.Builder(f.getActivity())
					.setTitle(R.string.dialog_prompt)
					.setMessage(R.string.settings_photo_picker_not_found)
					.setPositiveButton(R.string.confirm, null).show();
		}

	}

	public static void openGallery(Fragment f, int requestCode, Uri url,
			Uri storeUri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		Uri uri = url;
		if (uri == null) {
			MediaScannerConnection.scanFile(f.getActivity(),
					new String[] { FileUtil.getPhotoDir() }, null, null);
			File photoImage = new File(FileUtil.getPhotoPath());
			if (photoImage.exists()) {
				uri = Uri.fromFile(photoImage);
			} else {
				return;
			}
		}
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", AVATAR_WIDTH * 2);
		intent.putExtra("outputY", AVATAR_HEIGHT * 2);
		intent.putExtra("return-data", false);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, storeUri);
		try {
			f.startActivityForResult(intent, requestCode);
		} catch (ActivityNotFoundException e) {
			new WeddingDialog.Builder(f.getActivity())
					.setTitle(R.string.dialog_prompt)
					.setMessage(R.string.settings_photo_picker_not_found)
					.setPositiveButton(R.string.confirm, null).show();
		}

	}

	private static void fillIntentCrop(Intent intent) {
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", AVATAR_WIDTH);
		intent.putExtra("outputY", AVATAR_HEIGHT);
		intent.putExtra("return-data", true);
	}

}
