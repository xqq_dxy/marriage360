package com.marriage.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

public class BitmapFileUtil {

	private static final int BMP_MAX_SIZE = 1 * 1000 * 1000;
	
	public static final int WIDTH_LIMIT = 320;
	public static final int HEIGHT_LIMIT = 320;

	public static File saveTempBmp(Bitmap bmp, Context context) {
		long now = System.currentTimeMillis();
		File dir = new File(FileUtil.AVATAR_STORAGE_BASE_PATH);
		if (!dir.exists()){
			dir.mkdirs();
		}
		String fileName = FileUtil.AVATAR_STORAGE_BASE_PATH
				+ String.valueOf(now) + ".img";
		File bmpFile = new File(fileName);
		
		if (bmpFile.exists()) {
			bmpFile.delete();
		}
		FileOutputStream fos = null;
		try {
			bmpFile.createNewFile();
			fos = new FileOutputStream(bmpFile);
			bmp.compress(CompressFormat.JPEG, 80, fos);
			fos.flush();
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bmpFile;
	}

	public static String compress(File bmp) {

		if (bmp.length() <= BMP_MAX_SIZE) {
			D.error("compress", bmp.getAbsolutePath());
			return compressFinish(bmp);
		} else {
			return compressBmp(bmp, 1);
		}
	}

	public static File checkRotate(File bmp) {
		int d = readPictureDegree(bmp.getAbsolutePath());
		if (d == 0) {
			return bmp;
		}
		Bitmap b = BitmapFactory.decodeFile(bmp.getAbsolutePath());
		b = rotaingImageView(d, b);
		String name = FileUtil.getFileName(bmp);
		File rotatedF = new File(name + ".r" + System.currentTimeMillis());
		rotatedF = bitmapToFile(b, rotatedF, 80);
		if (rotatedF != null && rotatedF.exists()) {
			bmp.delete();
			return rotatedF;
		} else {
			return bmp;
		}

	}

	private static String compressBmp(File bmp, int compress_count) {
		String name = FileUtil.getFileName(bmp);
		Bitmap b = BitmapFactory.decodeFile(bmp.getAbsolutePath());
		File r = new File(name + ".t" + String.valueOf(compress_count));
		r = bitmapToFile(b, r, 50);
		if (r != null && r.exists()) {
			bmp.delete();
		} else {
			r = bmp;
		}
		if (r.length() > BMP_MAX_SIZE) {
			compress_count++;
			D.error("compressBmp", "count: " + compress_count);
			D.error("compressBmp", "r: " + r.length() + "  " + r.getName());
			return compressBmp(r, compress_count);
		} else {
			return compressFinish(r);
		}

	}

	private static String compressFinish(File tempBmp) {
		String fileName = FileUtil.getFileName(tempBmp);
		File newFile = new File(fileName + ".jpg");
		if (tempBmp.getAbsolutePath().equals(newFile.getAbsolutePath())) {
			return success(tempBmp);
		}
		boolean ret = false;
		try {
			ret = FileUtil.copyFile(tempBmp, newFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (ret) {
			if (tempBmp != null && tempBmp.exists()) {
				tempBmp.delete();
			}
			D.error("compressFinish", newFile.getAbsolutePath());
			return success(newFile);
		} else {
			return null;
		}
	}

	private static File bitmapToFile(Bitmap bitmap, File outPut, int quality) {
		FileOutputStream fos = null;
		try {
			if (outPut.exists()) {
				outPut.delete();
			}
			outPut.createNewFile();
			fos = new FileOutputStream(outPut);
			bitmap.compress(CompressFormat.JPEG, quality, fos);
			fos.flush();
			fos.close();
			if (bitmap != null && !bitmap.isRecycled()) {
				bitmap.recycle();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return outPut;
	}

	private static String success(File f) {

		return f.getAbsolutePath();
	}

	public static int readPictureDegree(String path) {
		int degree = 0;
		try {
			ExifInterface exifInterface = new ExifInterface(path);
			int orientation = exifInterface.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return degree;
	}

	/**
	 * 
	 * 旋转图片
	 * 
	 * @param angle
	 * 
	 * @param bitmap
	 * 
	 * @return Bitmap
	 */
	public static Bitmap rotaingImageView(int angle, Bitmap bitmap) {
		// 旋转图片 动作
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		System.out.println("angle2=" + angle);
		// 创建新的图片
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
				bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		return resizedBitmap;
	}
	
	/**
	 * 解码出指定大小的Bitmap从byteArray中
	 * @param data
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static Bitmap decodeBitmapFromByteArray(byte[] data, int reqWidth, int reqHeight) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		
		options.inJustDecodeBounds = true;
		
		BitmapFactory.decodeByteArray(data, 0, data.length, options);
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
		options.inJustDecodeBounds = false;
		
		return BitmapFactory.decodeByteArray(data, 0, data.length, options);
	}
	
	/**
	 * 计算Decode sample 值
	 * @param options
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}
		return inSampleSize;
	}
}
