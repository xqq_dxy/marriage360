package com.marriage.utility;

//import com.snda.youni.AppContext;
//import com.snda.youni.R;
//import com.snda.youni.attachment.util.AttachmentUtil;
//import com.snda.youni.modules.archive.AvailableSpaceHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;

import com.marriage.MApp;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

public class ImageUtil {

	public static Bitmap loadRotatedBitmapWithSizeLimitation(Context context,
			int maximum_size, Uri uri) {
		Bitmap bm = loadBitmapWithSizeLimitation(context, maximum_size, uri);
		int degree = getImageRotateDegree(uri.getPath());
		if (bm != null && degree != 0) {
			Matrix mat = new Matrix();
			mat.postRotate(degree);
			Bitmap bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
					bm.getHeight(), mat, true);
			if (!bitmap.equals(bm)) {
				bm.recycle();
			}
			return bitmap;

		} else {
			return bm;
		}
	}

	public static Bitmap loadRotatedBitmapWithSizeLimitation(Context context,
			int width, int height, Uri uri) {
		Bitmap bm = ImageUtil.loadBitmapWithSizeLimitation(context, width,
				height, uri);
		int degree = getImageRotateDegree(uri.getPath());
		if (bm != null && degree != 0) {
			Matrix mat = new Matrix();
			mat.postRotate(degree);
			Bitmap bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
					bm.getHeight(), mat, true);
			if (!bitmap.equals(bm)) {
				bm.recycle();
			}
			return bitmap;

		} else {
			return bm;
		}
	}

	public static Bitmap loadBitmapWithSizeLimitation(Context context,
			int maximum_size, Uri uri) {
		Bitmap image = null;
		BitmapFactory.Options opts = new BitmapFactory.Options();

		InputStream is = null;
		try {
			is = context.getContentResolver().openInputStream(uri);
			opts.inJustDecodeBounds = true;
			opts.inPreferredConfig = Bitmap.Config.RGB_565;
			BitmapFactory.decodeStream(is, null, opts);
			int size = opts.outWidth * opts.outHeight;
			int downScale = 1;
			while (size > maximum_size) {
				size = (size >> 2);
				downScale = (downScale << 1);
			}
			try {
				if (is != null)
					is.close();
			} catch (java.io.IOException e) {
				e.printStackTrace();
			}
			opts.inJustDecodeBounds = false;
			opts.inSampleSize = downScale;
			is = context.getContentResolver().openInputStream(uri);
			image = BitmapFactory.decodeStream(is, null, opts);

		} catch (java.io.FileNotFoundException e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			// Do nothing - the photo will appear to be missing
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (java.io.IOException e) {
					e.printStackTrace();
				}
		}
		// saveBitmapWithGivenSize(context,image);
		return image;
	}

	public static Bitmap loadBitmapWithSizeLimitation(Context context,
			int width, int height, Uri uri) {
		Bitmap image = null;
		BitmapFactory.Options opts = new BitmapFactory.Options();

		InputStream is = null;
		try {
			is = context.getContentResolver().openInputStream(uri);
			opts.inJustDecodeBounds = true;
			opts.inPreferredConfig = Bitmap.Config.RGB_565;
			BitmapFactory.decodeStream(is, null, opts);
			// int size = opts.outWidth * opts.outHeight;
			int downScale = 1;
			// while(size>maximum_size){
			// size = (size>>2);
			// downScale = (downScale<<1);
			// }
			int widthScale = Math.round((float) opts.outWidth / width);
			int heightScale = Math.round((float) opts.outHeight / height);
			downScale = widthScale > heightScale ? heightScale : widthScale;
			if (downScale < 1) {
				downScale = 1;
			}
			try {
				if (is != null)
					is.close();
			} catch (java.io.IOException e) {
				e.printStackTrace();
			}

			opts.inJustDecodeBounds = false;
			opts.inSampleSize = downScale;
			is = context.getContentResolver().openInputStream(uri); // liujinhui
																	// removed
																	// for
																	// reopen;2013.09.09
			image = BitmapFactory.decodeStream(is, null, opts);

		} catch (java.io.FileNotFoundException e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			// Do nothing - the photo will appear to be missing
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (java.io.IOException e) {
					e.printStackTrace();
				}
		}
		return image;
	}

	public static Bitmap loadBitmapWithSizeLimitation(Context context,
			int maximum_size, String filename, String path) {
		File file = new File(path + File.separator + filename);

		Uri uri = Uri.fromFile(file);

		return loadBitmapWithSizeLimitation(context, maximum_size, uri);
	}

	public static String getImageFileSize(String dir, String filename) {
		File f = new File(dir, filename);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(f);
			try {
				int size = fis.available();
				// fis.close();
				return (size >> 10) + "kb";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	public static String getImageResolution(String dir, String filename) {
		BitmapFactory.Options opts = new BitmapFactory.Options();

		File f = new File(dir, filename);
		InputStream is = null;
		try {
			is = new FileInputStream(f);
			opts.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, opts);
			return opts.outWidth + "x" + opts.outHeight;
			// int size = opts.outWidth*opts.outWidth;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	public static String getImageFileSize(String filename) {
		File f = new File(filename);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(f);
			try {
				int size = fis.available();
				// fis.close();
				return (size >> 10) + "kb";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	public static String getImageResolution(String filename) {
		BitmapFactory.Options opts = new BitmapFactory.Options();

		File f = new File(filename);
		InputStream is = null;
		try {
			is = new FileInputStream(f);
			opts.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, opts);
			return opts.outWidth + "x" + opts.outHeight;
			// int size = opts.outWidth*opts.outWidth;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	public static Bitmap loadBitmapWithRatio(Context context, int vWidth,
			int vHeight, String filename) {
		Bitmap bm = null;
		BitmapFactory.Options opts = new BitmapFactory.Options();
		File file = new File(filename);
		InputStream is = null;
		try {
			is = new FileInputStream(file);
			opts.inJustDecodeBounds = true;
			opts.inPreferredConfig = Bitmap.Config.RGB_565;
			BitmapFactory.decodeStream(is, null, opts);

			int bWidth = opts.outWidth;
			int bHeight = opts.outHeight;
			if (bWidth == 0 || bHeight == 0) {
				return null;
			}
			int scale = 1;
			if (bWidth > vWidth && bHeight > vHeight) {
				int widthRatio = bWidth / vWidth;
				int heightRatio = bHeight / vHeight;
				scale = widthRatio < heightRatio ? widthRatio : heightRatio;
			}

			opts.inJustDecodeBounds = false;
			opts.inSampleSize = scale;
			is = new FileInputStream(file);
			bm = BitmapFactory.decodeStream(is, null, opts);
			// try {
			// is.close();
			// } catch (java.io.IOException e) {
			// e.printStackTrace();
			// }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (java.io.IOException e) {
					e.printStackTrace();
				}
		}
		return bm;
	}

	public static int getImageRotateDegree(String filepath) {
		ExifInterface exif = null;
		int orientation = -1;
		try {
			exif = new ExifInterface(filepath);
			orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					-1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		int degree = 0;
		switch (orientation) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			degree = 90;
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			degree = 180;
			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			degree = 270;
			break;
		}
		return degree;
	}

	public static Bitmap markPictureWatermark(Bitmap bitmap, Bitmap watermark) {
		synchronized (ImageUtil.class) {
			if (bitmap == null && watermark == null)
				return bitmap;
			Canvas canvas = new Canvas();
			int height = bitmap.getHeight();
			int width = bitmap.getWidth();
			int wWidth = watermark.getWidth();
			int wHeight = watermark.getHeight();
			if (width < (wWidth) || height < (wHeight)) {
				return bitmap;
			}
			int left = 12;
			int top = 0;
			top = height - wHeight - 12;
			if (top < 0) {
				if ((height - wHeight) > 0) {
					top = (height - wHeight) >> 1;
				} else
					top = 0;
			}
			Paint paint = new Paint();
			Bitmap dest = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			canvas.setBitmap(dest);
			canvas.drawBitmap(bitmap, 0, 0, paint);
			canvas.drawBitmap(watermark, left, top, paint);

			bitmap.recycle();
			canvas = null;
			return dest;
		}
	}

	public static Bitmap markPictureWatermark(Context context, Bitmap bitmap,
			int id) {
		Bitmap watermark = BitmapFactory.decodeResource(context.getResources(),
				id);
		if (watermark == null)
			return bitmap;
		Bitmap bm = markPictureWatermark(bitmap, watermark);
		watermark.recycle();
		return bm;
	}

	public static boolean markPictureWatermark(Context context, String srcPath,
			String destPath, int id) {
		int width = context.getResources().getDisplayMetrics().widthPixels;
		int height = context.getResources().getDisplayMetrics().heightPixels;
		Bitmap bm = ImageUtil.loadRotatedBitmapWithSizeLimitation(context,
				width * height, Uri.fromFile(new File(srcPath)));

		if (bm == null)
			return false;
		Bitmap dest = markPictureWatermark(context, bm, id);

		if (bm == dest) {
			bm.recycle();
			return false;
		}
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(destPath);
			dest.compress(CompressFormat.JPEG, 80, fos);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (dest != null) {
			dest.recycle();
		}
		return true;
	}

	public static DisplayImageOptions getCircleDisplayImageOptions() {
		DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder();
		builder.cacheInMemory(true).cacheOnDisk(true)
				.bitmapConfig(Config.RGB_565);
		builder.postProcessor(new BitmapProcessor() {

			@Override
			public Bitmap process(Bitmap arg0) {
				// TODO Auto-generated method stub
				return CircleBitmapMaker.getCircleBitmap(MApp.get(), arg0);
			}
		});
		return builder.build();
	}
}
