package com.marriage.utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.text.TextUtils;

import com.marriage.R;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXTextObject;
import com.tencent.mm.sdk.openapi.WXWebpageObject;

public class ShareUtils {
	public static void shareToSms(Context context, String content,
			String phoneNum) {
		if(phoneNum == null) {
			phoneNum = "";
		}
		
		Uri smsToUri = Uri.parse("smsto:" + phoneNum);
		Intent sendIntent = new Intent(Intent.ACTION_SENDTO, smsToUri);
		sendIntent.putExtra("sms_body", content);
		context.startActivity(sendIntent);
	}

	public static void shareToEmail(Context context, String reciverAddress,
			String subject, String content) {
		Intent email = new Intent(android.content.Intent.ACTION_SEND);
		email.setType("plain/text");
		email.putExtra(android.content.Intent.EXTRA_EMAIL, reciverAddress);
		email.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
		email.putExtra(android.content.Intent.EXTRA_TEXT, content);
		context.startActivity(Intent.createChooser(email, "请选择邮件发送软件"));
	}

	public static void shareToWeixin(Context context, String content, String imageurl,
				String url, String title, boolean isSendFriendsCycle) {
		final IWXAPI api = WXAPIFactory.createWXAPI(context,
				Constants.APP_ID_WEIXIN, true);
		api.registerApp(Constants.APP_ID_WEIXIN);
		
		if (!api.isWXAppInstalled()) {
			ToastHelper.showLongToast(R.string.string_install_weixin_first_please);
		} else if(!api.isWXAppSupportAPI()) {
			ToastHelper.showLongToast(R.string.string_not_sopport_weixin_api);
		} else {
			if (imageurl == null && url == null) {
				WXTextObject ob = new WXTextObject();
				ob.text = content;
				WXMediaMessage message = new WXMediaMessage();
				message.mediaObject = ob;
				message.description = content;

				SendMessageToWX.Req req = new SendMessageToWX.Req();
				req.transaction = String.valueOf(System.currentTimeMillis());
				req.message = message;
				req.scene = isSendFriendsCycle ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
				api.sendReq(req);
			} else {
				Bitmap logoBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
				WXMediaMessage localWXMediaMessage = new WXMediaMessage();
				if(!TextUtils.isEmpty(url)) {
					WXWebpageObject localWXWebpageObject = new WXWebpageObject();
					localWXWebpageObject.webpageUrl = url;
					localWXMediaMessage.mediaObject = localWXWebpageObject;
				} else {
					WXTextObject ob = new WXTextObject();
					ob.text = content;
					localWXMediaMessage.mediaObject = ob;
				}
				localWXMediaMessage.title = title;// 不能太长，否则微信会提示出错。不过博主没验证过具体能输入多长。
				localWXMediaMessage.description = content;
				byte[] logo = getLogoBitmapBytes(logoBitmap);
				if (logo != null)
					localWXMediaMessage.thumbData = logo;
				SendMessageToWX.Req localReq = new SendMessageToWX.Req();
				localReq.transaction = String.valueOf(System.currentTimeMillis());
				localReq.message = localWXMediaMessage;
				localReq.scene = isSendFriendsCycle ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
				api.sendReq(localReq);
			}
		}
	}
	
	private static byte[] getLogoBitmapBytes(Bitmap bitmap) {
		int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        float scaleW =0.8f;
        float scaleH =0.8f;
		while (true){
			try {
		        matrix.postScale(scaleW, scaleH);
		        Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, (int) width,(int) height, matrix, true);
		        
				ByteArrayOutputStream out = new ByteArrayOutputStream();		
				bitmap2.compress(Bitmap.CompressFormat.PNG, 100, out);
				byte[] arrayOfByte = out.toByteArray();
		        out.flush();   
		        out.close();
		        if(arrayOfByte.length < 32768)
		        	return arrayOfByte;
		    } catch (IOException e) {   
		        e.printStackTrace();   
		    }
			scaleW = scaleW - 0.1f;
			scaleH = scaleH - 0.1f;
		}
	}
	
	public static void shareToWeibo(Context context, String subject, String content) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, content);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		List<ResolveInfo> resolveList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		ActivityInfo info = null;
		if(resolveList != null && resolveList.size() > 0) {
			for(int i=0; i<resolveList.size(); i++) {
				ActivityInfo aInfo = resolveList.get(i).activityInfo;
				if(Constants.PACKAGE_NAME_SING_WEIBO.equals(aInfo.packageName)) {
					info = aInfo;
					break;
				}
			}
		}
		if(info != null) {
			intent.setComponent(new ComponentName(
					info.applicationInfo.packageName, info.name));
			context.startActivity(intent);
		}
	}
	
	public static void shareToQQ(Context context, String subject, String content) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, content);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		List<ResolveInfo> resolveList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		ActivityInfo info = null;
		if(resolveList != null && resolveList.size() > 0) {
			for(int i=0; i<resolveList.size(); i++) {
				ActivityInfo aInfo = resolveList.get(i).activityInfo;
				if(Constants.PACKAGE_NAME_TENCENT_QQ.equals(aInfo.packageName)) {
					info = aInfo;
					break;
				}
			}
		}
		if(info != null) {
			intent.setComponent(new ComponentName(
					info.applicationInfo.packageName, info.name));
			context.startActivity(intent);
		}
	}
	
}
