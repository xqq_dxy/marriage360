package com.marriage.utility;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

public class LocationHelper {
	Context mContext;
	LocationCallback mLocationCallback;
	LocationClient mBaiduLocationClient;

	public static String BAIDU_LOCATION_NETWORK_PROVIDER = "baidu_network_location";

	public static String TAG = LocationHelper.class.getSimpleName();
	
	public LocationHelper(Context context, LocationCallback locationCallback) {
		mContext = context.getApplicationContext();
		mLocationCallback = locationCallback;
	}

	public void requestLocation() {
		requestAndroidLocation();
	}

	public void requestAndroidLocation() {
		LocationManager locationManager = (LocationManager) mContext
				.getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			locationManager
					.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
							1000, 0, locationListener);
		} else if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
		    locationManager
            .requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000, 0, locationListener);
		} else{
		    requestBaiduLocation();
		}
	}

	public void requestBaiduLocation() {
		mBaiduLocationClient = new LocationClient(mContext);
		mBaiduLocationClient.registerLocationListener(baiduLocationListener);
		LocationClientOption option = new LocationClientOption();
		option.setPriority(LocationClientOption.NetWorkFirst);
		option.disableCache(true);
		option.setAddrType("all");
		option.setCoorType("gcj02");
		option.setScanSpan(0);
		mBaiduLocationClient.setLocOption(option);
		mBaiduLocationClient.start();
		if (mBaiduLocationClient.isStarted()) {
			mBaiduLocationClient.requestLocation();
		}
	}

	private final LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			((LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE))
					.removeUpdates(locationListener);
			if (location != null){
				updateWithNewLocation(location);
			}
		}

		public void onProviderDisabled(String provider) {

		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	private BDLocationListener baiduLocationListener = new BDLocationListener() {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null) {
				updateWithNewLocation(null);
				mBaiduLocationClient.stop();
				return;
			}

			Location locationAndroid = new Location(
					BAIDU_LOCATION_NETWORK_PROVIDER);
			locationAndroid.setLatitude(location.getLatitude());
			locationAndroid.setLongitude(location.getLongitude());
			updateWithNewLocation(locationAndroid);
			mBaiduLocationClient.stop();
		}

		@Override
		public void onReceivePoi(BDLocation location) {

		}

	};

	private void updateWithNewLocation(Location result) {
		if (mLocationCallback == null) {
			return;
		}
		mLocationCallback.onLocationReady(result);
	}

	public void clearLocationCallback() {
		mLocationCallback = null;
	}

	public interface LocationCallback {
		public void onLocationReady(Location location);
	}
}
