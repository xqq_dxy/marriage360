package com.marriage.utility;

import static android.os.Environment.MEDIA_MOUNTED;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;

import com.nostra13.universalimageloader.utils.L;

/**
 * 相机拍照或从系统相册里获取一张照片
 * @author Martin
 *
 */
public class ImagePicker {
	public static final int FROM_CAMERA = 1;
	public static final int FROM_GALLERY = 2;
	
	private Context context;
	
	public ImagePicker(Context context) {
		this.context = context;
	}
	
	public RequestParams startActivityForResult(int where, int requestCode, StartActivityForResultCompt compt) {
		if(where != FROM_CAMERA && where != FROM_GALLERY) {
			throw new IllegalStateException("where 参数不正确 ");
		}
		
		if(where == FROM_GALLERY) {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			compt.startActivityForResultCompt(intent, requestCode);
			return new RequestParams(context, null, intent, requestCode, where);
		} else {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			Uri output = null;
			if(externalStorageIsOk(context)) {
				output = Uri.fromFile(getOutputFileName(context));
				intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
			}
			compt.startActivityForResultCompt(intent, requestCode);
			return new RequestParams(context, output, intent, requestCode, where);
		}
	}
	
	
	private static File getOutputFileName(Context context) {
		File file = new File(getExternalCacheDir(context),
				String.valueOf(System.currentTimeMillis()));
		if(file.exists()){
			file.delete();
		}
		return file;
	}
	
	private static boolean externalStorageIsOk(Context context) {
		return MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
				&& hasExternalStoragePermission(context);
	}
	
	private static boolean hasExternalStoragePermission(Context context) {
		int perm = context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
		return perm == PackageManager.PERMISSION_GRANTED;
	}
	
	private static File getExternalCacheDir(Context context) {
		File dataDir = new File(new File(Environment.getExternalStorageDirectory(), "Android"), "data");
		File appCacheDir = new File(new File(dataDir, context.getPackageName()), "cache");
		if (!appCacheDir.exists()) {
			if (!appCacheDir.mkdirs()) {
				L.w("Unable to create external cache directory");
				return null;
			}
			try {
				new File(appCacheDir, ".nomedia").createNewFile();
			} catch (IOException e) {
			}
		}
		return appCacheDir;
	}
	
	
	public static class RequestParams implements Parcelable {
		
		public RequestParams(Parcel source) {
			this.output = Uri.CREATOR.createFromParcel(source);
			this.intent = Intent.CREATOR.createFromParcel(source);
			this.requestCode = source.readInt();
			this.where = source.readInt();
		}
		
		public void fillContext(Context context) {
			this.content = context;
		}
		
		public RequestParams(Context context, Uri output, Intent intent, int requestCode,
				int where) {
			super();
			this.content = context;
			this.output = output;
			this.intent = intent;
			this.requestCode = requestCode;
			this.where = where;
		}
		Context content;
		Uri output;
		Intent intent;
		int requestCode;
		int where;
		
		public Uri parseResult(int requestCode, int resultCode, Intent data) {
			if(this.requestCode == requestCode && resultCode == Activity.RESULT_OK) {
				if(where == FROM_CAMERA) {
					if(output != null) {
						return output;
					}
					if (data != null) {
						Bundle bundle = data.getExtras();
						if (bundle != null && bundle.get("data") != null) {
							Bitmap bitmap = (Bitmap) bundle.get("data");
							File fileName = generateImageOutputFile(content);
							FileOutputStream fos;
							try {
								fos = new FileOutputStream(fileName);
								bitmap.compress(Bitmap.CompressFormat.JPEG,
										100, fos);
							} catch (FileNotFoundException e) {
								return null;
							}
							return Uri.fromFile(fileName);
						}
					}
				} else if(where == FROM_GALLERY) {
					if(data != null) {
						return data.getData();
					}
				}
			}
			return null;
		}
		
		private File generateImageOutputFile(Context context) {
			File file = new File(context.getCacheDir(), String.valueOf(System.currentTimeMillis()));
			if(file.exists()) {
				file.delete();
			}
			return file;
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			Uri.writeToParcel(dest, output);
			intent.writeToParcel(dest, flags);
			dest.writeInt(requestCode);
			dest.writeInt(where);
		}
		
		public static final Parcelable.Creator<RequestParams> CREATOR = new Creator<ImagePicker.RequestParams>() {
			
			@Override
			public RequestParams[] newArray(int size) {
				// TODO Auto-generated method stub
				return new RequestParams[size];
			}
			
			@Override
			public RequestParams createFromParcel(Parcel source) {
				// TODO Auto-generated method stub
				return new RequestParams(source);
			}
		};
	}
	
	public interface StartActivityForResultCompt {
		public void startActivityForResultCompt(Intent intent, int requestCode);
	}
	
}
