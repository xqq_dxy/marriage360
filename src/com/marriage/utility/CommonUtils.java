package com.marriage.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.util.Base64;

public class CommonUtils {

	public static boolean isEmailAddress(String address){
		boolean flag = false;  
	      try{  
	       String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
	       Pattern regex = Pattern.compile(check);  
	       Matcher matcher = regex.matcher(address);  
	       flag = matcher.matches();  
	      }catch(Exception e){  
	       flag = false;  
	      }  
		
		return flag;
	}
	
	/** Read the object from Base64 string. */
	public static Object fromString(String s) throws IOException,
			ClassNotFoundException {
		byte[] data = Base64.decode(s, Base64.DEFAULT);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
				data));
		Object o = ois.readObject();
		ois.close();
		return o;
	}

	/** Write the object to a Base64 string. */
	public static String toString(Serializable o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(o);
		oos.close();
		return new String(Base64.encode(baos.toByteArray(), Base64.DEFAULT));
	}
	
	/**
	 * convert dip to px
	 * 
	 * @param ctx
	 *            Context
	 * @param dip
	 * @return
	 */
	public static int dip2Px(Context ctx, float dip) {

		float density = ctx.getResources().getDisplayMetrics().density;
		float px = dip * density + 0.5f;

		return (int) px;
	}

	/**
	 * convert px to dip
	 * 
	 * @param context
	 * @param px
	 * @return
	 */
	public static int px2dip(Context context, float px) {
		float density = context.getResources().getDisplayMetrics().density;
		int dip = (int) (px / density + 0.5f);
		return dip;
	}

	/**
	 * convert px to sp
	 * 
	 * @param context
	 * @param px
	 * @return
	 */
	public static int px2sp(Context context, float px) {
		float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (px / fontScale + 0.5f);
	}

	/**
	 * convert sp to px
	 * 
	 * @param context
	 * @param sp
	 * @return
	 */
	public static int sp2px(Context context, float sp) {
		float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (sp * fontScale + 0.5f);
	}
}
