package com.marriage.utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Environment;
import android.util.Base64;

public class FileUtil {

	private static final String PHOTO_DIR = Environment.getExternalStorageDirectory()
		    + "/DCIM/Camera";
	
	public static final String AVATAR_STORAGE_BASE_PATH = Environment
			.getExternalStorageDirectory() + File.separator + "360hunjia/avatar/";
	
	private static String sPhotoFileName;
	
	public static String createPhotoPath() {
        sPhotoFileName = getPhotoFileName();
        new File(PHOTO_DIR).mkdirs();
        return PHOTO_DIR + "/" + sPhotoFileName;
    }
	
	private static String getPhotoFileName() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("'IMG'_yyyyMMdd_HHmmss");
        return dateFormat.format(date) + ".jpg";
    }
	
	public static String getPhotoDir() {
        return PHOTO_DIR;
    }
	
	public static String getPhotoPath() {
        new File(PHOTO_DIR).mkdirs();
        return PHOTO_DIR + "/" + sPhotoFileName;
    }
	
	public static String getFileName(File f){
		String path = f.getAbsolutePath();
		String name = path.substring(0, path.lastIndexOf("."));
		return name;
	}
	
	public static boolean copyFile(File src, File dest) throws IOException{
		if (!src.exists()){
			return false;
		}
		if (!dest.exists()){
			dest.createNewFile();
		}
		FileInputStream fis = new FileInputStream(src);
		FileOutputStream fos = new FileOutputStream(dest);
		byte[] buffer = new byte[1024 * 10];
		int ret = -1;
		while ((ret = fis.read(buffer)) != -1) {
			fos.write(buffer, 0, ret);
		}
		fos.flush();
		fos.close();
		fis.close();
		return true;
	}
	
	public static String fileToString(String filePath){
		File imageFile = new File(filePath);
		if (!imageFile.exists()){
			return null;
		}
		String temp = "";
		
		try {
			InputStream fileStream = new FileInputStream(imageFile);
			String t = Base64.encodeToString(inputStreamTOByte(fileStream), Base64.DEFAULT);
			temp = t;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}
	
	public static byte[] inputStreamTOByte(InputStream in) throws IOException{   
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();   
		byte[] data = new byte[4096];   
		int count = -1;   
		while((count = in.read(data,0,4096)) != -1)   
		outStream.write(data, 0, count);   
		data = null;
		return outStream.toByteArray();   
	}
}
