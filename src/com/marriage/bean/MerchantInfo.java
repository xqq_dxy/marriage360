package com.marriage.bean;

import java.util.List;

public class MerchantInfo extends BaseApiResult {

	/*sa_user_visit_count: 1, 
	  sa_xitie_click_count: 0, 
	  sa_user_part_visits: [
	    {
	      user_id: "74", 
	      visitor_head_img: "http://api.360hunjia.com/server/public/avatar/noavatar_small.png"
	    }
	  ], 
	  sa_personal_info: {
	    focused_count: "44", 
	    faved_count: "0", 
	    dp_count: "0", 
	    phoned_count: "0", 
	    sa_type_name: "结婚小商品", 
	    sa_type_id: "12", 
	    sa_name: "hunjia123", 
	    city_name: "成都", 
	    head_img: "http://api.360hunjia.com/server/public/avatar/noavatar_small.png"
	  }*/
	public String sa_user_visit_count;
	public String sa_xitie_click_count;
	public MerchantPersonalInfo sa_personal_info;
	public List<MerchantVisitors> sa_user_part_visits;
}
