package com.marriage.bean;

public class NormalUserInfo extends User{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5421723488527905569L;

	/*user: {
    user_id: "84", 
    user_name: "hunjia123", 
    is_merchant: "1", 
    merchant_name: "hunjia123", 
    focus_count: "11", 
    focused_count: "41", 
    topic_count: "21", 
    faved_count: "76", 
    myear: "0", 
    mmonth: "0", 
    mday: "0", 
    city_id: "17", 
    city_name: "上海"
  }, */
	public String focus_count;
	public String focused_count;
	public String topic_count;
	public String faved_count;
}
