package com.marriage.bean;

public class XitieInfo {

	/*id: "4370", 
    title: "我们结婚啦！！！", 
    fname: "李怡霏 ", 
    mname: "盛昱枫 ", 
    marry_date: "2014-3-28", 
    hour: "17", 
    min: "00", 
    week: "星期五", 
    addr: "二环路西三段", 
    hotel: "宇豪金港湾酒店", 
    content: "", 
    index_img: "http://www.360hunjia.com//public/wedding/original/201408/31/126123bcec0af77f58595b5580dd864a6c.jpg", 
    code_id: "21179", 
    uid: "10252", 
    xndh: "", 
    xldh: "", 
    is_visi: "1"*/
	public String id;
	public String title;
	public String fname;
	public String mname;
	public String marry_date;
	public String hour;
	public String min;
	public String week;
	public String addr;
	public String hotel;
	public String content;
	public String index_img;
	public String code_id;
	public String uid;
	public String xndh;
	public String xldh;
	public String is_visi;
}
