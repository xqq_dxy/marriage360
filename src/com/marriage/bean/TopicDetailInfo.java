package com.marriage.bean;

import java.util.List;

public class TopicDetailInfo extends BaseApiResult {
	public TopicDetail topic_model;
	public boolean focus;
	public boolean is_sa;
	public long time_diff;
	public String topic_user_head_img;
	public String vote_user_count;
	public List<VoteUser> vote_user;
	public boolean is_fav;
	public String share_url;
	public String sl_id;
	public String sl_name;
}
