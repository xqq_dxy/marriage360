package com.marriage.bean;

import java.util.List;

public class TopicDetail {
	public String topic_id;
	public String fav_count;
	public String content;
	public String reply_count;
	public String topic_user_id;
	public String topic_user_name;
	public String topic_create_time;
	public String topic_cate_name;
	public String topic_cate_id;
	public String topic_city_id;
	public String topic_city_name;
	public String click_count;
	public List<TopicDetailImages> imgs;
}
