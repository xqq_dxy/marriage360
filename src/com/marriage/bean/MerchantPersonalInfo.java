package com.marriage.bean;

public class MerchantPersonalInfo {

	/*focused_count: "44", 
    faved_count: "0", 
    dp_count: "0", 
    phoned_count: "0", 
    sa_type_name: "结婚小商品", 
    sa_type_id: "12", 
    sa_name: "hunjia123", 
    city_name: "成都", 
    head_img: "http://api.360hunjia.com/server/public/avatar/noavatar_small.png"*/
	
	public String focused_count;
	public String faved_count;
	public String dp_count;
	public String phoned_count;
	public String sa_type_name;
	public String sa_type_id;
	public String sa_name;
	public String city_name;
	public String head_img;
}
