package com.marriage.bean;

import java.io.Serializable;

public class XTList implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4439933455989844015L;
	public String id;
	public String title;
	public String fname;
	public String mname;
	public String marry_date;
	public String hour;
	public String min;
	public String week;
	public String addr;
	public String hotel;
	public String content;
	public String index_img;
	public String code_id;
	public String uid;
	public String xndh;
	public String xldh;
	public String is_visi;
	public String url;
}
