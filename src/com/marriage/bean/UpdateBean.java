package com.marriage.bean;

public class UpdateBean extends BaseApiResult {

	/*{
		  op_code: "100", 
		  op_info: "success", 
		  is_new_version: true, 
		  new_version: {
		    vcode: "1.0", 
		    create_on: "2014-06-01 18:05:29", 
		    fileurl: "http://www.baidu.com/360cc.apk "
		  }
		}*/
	public boolean is_new_version;
	public UpdateInfo new_version;
}
