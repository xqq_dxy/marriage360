package com.marriage.bean;

import java.io.Serializable;

public class User implements Serializable{

	/*user_id: "10350", 
    user_name: "测试", 
    is_merchant: "0", 
    merchant_name: "", 
    myear: "0", 
    mmonth: "0", 
    mday: "0", 
    city_id: "17", 
    city_name: "上海"*/
	
/**
	 * 
	 */
	private static final long serialVersionUID = -5621417728884993401L;
//	"id":24117,"user_name":"andysofan1","email":"test@163.com","mobile":"15021571111" 
	public String user_id;
	public String user_name;
	public String is_merchant;
	public String merchant_name;
	public String myear;
	public String mmonth;
	public String mday;
	public String city_id;
	public String city_name;
}
