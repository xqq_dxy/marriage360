package com.marriage.bean;

public class Topics {
	public String topic_id;
	public String topic_content;
	public String id;
	public String user_id;
	public String user_name;
	public String user_head_img;
	public String has_image;
	public String reply_count;
	public String good_count;
	public String fav_count;
	public boolean is_sa;
	public String cover_img;
	public int cover_img_w;
	public int cover_img_h;
	public String images_count;
	public String sl_id;
	public String sl_name;
}
