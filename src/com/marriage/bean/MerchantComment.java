package com.marriage.bean;

public class MerchantComment {
	public String sa_dp_id;
	public String title;
	public String content;
	public String create_time;
	public String user_id;
	public String head_img;
	public String time_diff;
	public boolean isSA;
	public String user_name;
}
