package com.marriage.bean;

import java.io.Serializable;

public class HeadPics implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1557337409371055311L;
	public String big;
	public String middle;
	public String small;
}
