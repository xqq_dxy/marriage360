package com.marriage.bean;

public class PublishComment {
	public String user_id;
	public String user_name;
	public String content;
	public String reply_time;
	public String avatar_small;
	public String city_name;
	public boolean is_sa;
	public String sl_name;
	public String sl_id;
}
