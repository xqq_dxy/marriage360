package com.marriage.bean;

import java.io.Serializable;

public class UserInfo extends BaseApiResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5655747521923059941L;
	public User user;
	public HeadPics head_pics;
	public String user_type;//1:商家用户   0:普通用户
	public SupplierAccount supplier_account;
	public String left_days;
}
