package com.marriage.dialog;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.marriage.R;

public class WeddingDialog extends Dialog {

	private final Window mWindow;
	private static final int MAX_INPUT_TEXT_PUBLISH = 200;
	public AlertController mController;

	public WeddingDialog(Context context) {
		super(context, R.style.wedding_dialog);
		mWindow = getWindow();
		// mWindow.setBackgroundDrawableResource(R.color.wine_dialog_transparent_background);
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.dimAmount = 0.5f;
		getWindow().setAttributes(lp);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		mController = new AlertController(context, this, getWindow());
	}

	public EditText getEditTextInEditMode() {
		return mController.mEditTextContent;
	}

	public class WineDialogAdapter extends BaseAdapter {

		public LayoutInflater mInflater;
		public String[] mMainTitle;
		public String[] mSubTitle;
		public boolean mWithTitle;

		public WineDialogAdapter(LayoutInflater inflater, String[] mainTitle,
				String[] subTitle, boolean withTitle) {
			mInflater = inflater;
			mMainTitle = mainTitle;
			mSubTitle = subTitle;
			mWithTitle = withTitle;
		}

		@Override
		public int getCount() {
			return mMainTitle.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.dialog_wedding_list_item,
						null);
				TextView mainTitle = (TextView) convertView
						.findViewById(R.id.main_title);
				mainTitle.setText(mMainTitle[position]);
				TextView subTitle = (TextView) convertView
						.findViewById(R.id.sub_title);
				if (mSubTitle == null) {
					subTitle.setVisibility(View.GONE);
				} else {
					if (mSubTitle[position].equals("")) {
						subTitle.setVisibility(View.GONE);
					} else {
						subTitle.setText(mSubTitle[position]);
					}
				}
				if (position == 0) {
					if (mWithTitle) {
						if (mMainTitle.length == 1) {
							convertView
									.setBackgroundResource(R.drawable.seletor_dialog_list_bottom);
						} else {
							convertView
									.setBackgroundResource(R.drawable.selector_dialog_list_middle);
						}
					} else {
						convertView
								.setBackgroundResource(R.drawable.selector_dialog_list_top);
					}

				} else if (position == (mMainTitle.length - 1)) {
					convertView
							.setBackgroundResource(R.drawable.seletor_dialog_list_bottom);
				} else {
					convertView
							.setBackgroundResource(R.drawable.selector_dialog_list_middle);
				}
			}
			return convertView;
		}
	}

	public class AlertController {
		public final Context mContext;
		public final LayoutInflater mInflater;
		public final Dialog mDialogInterface;
		public View mTopTextArea;
		public View mBtnArea;
		public View meditAndIconArea;
		public View meditTextArea;
		public View mCustomView;

		public TextView mTitleTextView;
		public TextView mListViewTitle;
		public TextView mTopTextView;
		public TextView mLeftButton;
		public TextView mRightButton;
		public TextView mSingleButton;
		public CheckedTextView mCheckBox;
		public ImageView mIconImageView;
		public TextView mMessageTextView;
		public ListView mDialogListview;
		public SimpleAdapter mAdapter;
		public BaseAdapter mBaseAdapter;
		public List<Map<String, String>> mTitles;
		public String[] mMainTitle;
		public String[] mSubTitle;
		public WineDialogAdapter mListDialogAdapter;
		public EditText mEditTextContent;
		public View mTextAreaWrapper;

		public AlertController(Context context, Dialog di, Window window) {
			mContext = context;
			mDialogInterface = di;
			mWindow.requestFeature(Window.FEATURE_NO_TITLE);
			mWindow.setContentView(R.layout.dialog_wedding);
			getWindow().setBackgroundDrawableResource(
					android.R.color.transparent);
			mTopTextArea = mWindow.findViewById(R.id.top_text_area);
			mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mBtnArea = mWindow.findViewById(R.id.btn_area);
			meditAndIconArea = mWindow.findViewById(R.id.edit_and_icon_area);
			meditTextArea = mWindow.findViewById(R.id.edit_text_area);
			mCustomView = mWindow.findViewById(R.id.custom);

			mTitleTextView = (TextView) findViewById(R.id.title);
			mListViewTitle = new TextView(context);
			mListViewTitle.setLayoutParams(new AbsListView.LayoutParams(
					AbsListView.LayoutParams.MATCH_PARENT,
					AbsListView.LayoutParams.MATCH_PARENT));
			mListViewTitle
					.setBackgroundResource(R.color.main_background);
			mListViewTitle.setTextColor(context.getResources().getColor(
					R.color.top_text_color));
			mListViewTitle.setGravity(Gravity.CENTER);
			mListViewTitle.setTextSize(17);
			mTopTextView = (TextView) findViewById(R.id.text_area_content);
			mTextAreaWrapper = findViewById(R.id.text_area_wrapper);
			mDialogListview = (ListView) (mWindow
					.findViewById(R.id.dialog_listview));
			mLeftButton = (TextView) mWindow.findViewById(R.id.left_btn_text);
			mRightButton = (TextView) mWindow.findViewById(R.id.right_btn_text);
			mCheckBox = (CheckedTextView) mWindow.findViewById(R.id.checkbox);
			mSingleButton = (TextView) mWindow
					.findViewById(R.id.single_btn_text);
			mIconImageView = (ImageView) mWindow.findViewById(R.id.dialog_icon);
			mMessageTextView = (TextView) mWindow
					.findViewById(R.id.dialog_message_text);
			mEditTextContent = (EditText) mWindow
					.findViewById(R.id.edit_text_content);
		}

		public void createListView(boolean withTitle) {
			mListDialogAdapter = new WineDialogAdapter(mInflater, mMainTitle,
					mSubTitle, withTitle);
			mDialogListview.setAdapter(mListDialogAdapter);
		}

	}

	public void installContent() {
		mWindow.requestFeature(Window.FEATURE_NO_TITLE);
		mWindow.setContentView(R.layout.dialog_wedding);
	}

	public static class AlertParams {
		public static final int BUTTON_MODE = 1;
		public static final int LIST_MODE = 2;
		public static final int MULTI_MODE = 3;
		public static final int EDIT_MODE = 4;
		public static final int CUSTOM_MODE = 5;

		public int mMode = BUTTON_MODE;

		public final Context mContext;
		public final LayoutInflater mInflater;

		public String[] mMainTitle;
		public String[] mSubTitle;
		public int mTitleId = 0;
		public CharSequence mTitleText;
		public int mLeftTextId;
		public int mRightTextId;
		public CharSequence mLeftText;
		public CharSequence mRightText;
		public CharSequence mCheckedBoxText;
		public int mTopTextId;
		public CharSequence mTopText;
		public int mIconImageResource;
		public int mMessageText;
		public DialogInterface.OnClickListener mLeftBtnOnClickListener;
		public DialogInterface.OnClickListener mRightBtnOnClickListener;
		public View.OnClickListener mCheckedBoxOnClickListener;
		public AdapterView.OnItemClickListener mOnItemClickListener;
		public DialogInterface.OnClickListener mListViewDialogOnClickListener;
		public View mView;

		private boolean mWithTitle = true;

		public AlertParams(Context context) {
			mContext = context;
			mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public void apply(final AlertController controller) {
			if (mMainTitle != null) {
				mMode = AlertParams.LIST_MODE;
			}
			if ((mTitleId == 0) && (mTitleText == null)) {
				mWithTitle = false;
			}
			if (mView != null) {
				mMode = AlertParams.CUSTOM_MODE;
			}
			if (mMode == AlertParams.LIST_MODE) {
				controller.mBtnArea.setVisibility(View.GONE);
				controller.mDialogListview.setVisibility(View.VISIBLE);
				controller.mTopTextArea.setVisibility(View.GONE);
				controller.meditAndIconArea.setVisibility(View.GONE);
				controller.meditTextArea.setVisibility(View.GONE);
				controller.mMainTitle = mMainTitle;
				controller.mSubTitle = mSubTitle;

				if (mWithTitle) {
					if (mTitleText == null) {
						controller.mListViewTitle.setText(mTitleId);
					} else {
						controller.mListViewTitle.setText(mTitleText);
					}
					controller.mDialogListview.addHeaderView(
							controller.mListViewTitle, null, false);
				}
				controller.createListView(mWithTitle);
				controller.mDialogListview
						.setOnItemClickListener(mOnItemClickListener);
				if (mListViewDialogOnClickListener != null) {
					controller.mDialogListview
							.setOnItemClickListener(new OnItemClickListener() {
								public void onItemClick(AdapterView parent,
										View v, int position, long id) {
									int index = position
											- controller.mDialogListview
													.getHeaderViewsCount();
									mListViewDialogOnClickListener.onClick(
											controller.mDialogInterface, index);
									controller.mDialogInterface.dismiss();
								}
							});
				}
			} else if ((mMode == AlertParams.BUTTON_MODE)
					|| (mMode == AlertParams.CUSTOM_MODE)) {
				// BUTTON模式下，在对话框的body部分可以使用自定义的方式
				controller.mBtnArea.setVisibility(View.VISIBLE);
				controller.mDialogListview.setVisibility(View.GONE);
				controller.mTopTextArea.setVisibility(View.VISIBLE);
				controller.meditAndIconArea.setVisibility(View.GONE);
				controller.meditTextArea.setVisibility(View.GONE);
				if (mTitleText == null) {
					if (mTitleId > 0) {
						controller.mTitleTextView.setText(mTitleId);
						controller.mTextAreaWrapper
								.setBackgroundResource(android.R.color.white);
					} else {
						controller.mTitleTextView.setVisibility(View.GONE);
					}
				} else {
					controller.mTitleTextView.setText(mTitleText);
					controller.mTextAreaWrapper
							.setBackgroundResource(android.R.color.white);
				}
				if ((mTopText == null) && (mTopTextId > 0)) {
					String content = mContext.getString(mTopTextId);
					if (content.length() < 11) {
						controller.mTopTextView.setGravity(Gravity.CENTER);
					}
					controller.mTopTextView.setText(mTopTextId);
				} else if (mTopText != null) {
					if (mTopText.length() < 11) {
						controller.mTopTextView.setGravity(Gravity.CENTER);
					}
					controller.mTopTextView.setText(mTopText);
				}
				if ((mLeftText == null) && (mLeftTextId > 0)) {
					controller.mLeftButton.setText(mLeftTextId);
					controller.mSingleButton.setText(mLeftTextId);
				} else {
					controller.mLeftButton.setText(mLeftText);
					controller.mSingleButton.setText(mLeftText);
				}
				if ((mRightText == null) && (mRightTextId > 0)) {
					controller.mRightButton.setText(mRightTextId);
					controller.mSingleButton.setText(mRightTextId);
				} else {
					controller.mRightButton.setText(mRightText);
					controller.mSingleButton.setText(mRightText);
				}
				// 如果只有一组button被设置了显示文字，则说明应该进入单按钮模式
				if (((mRightText == null) && (mRightTextId == 0))
						&& ((mLeftText != null) || (mLeftTextId > 0))) {
					controller.mLeftButton.setVisibility(View.GONE);
					controller.mRightButton.setVisibility(View.GONE);
					controller.mSingleButton.setVisibility(View.VISIBLE);
					controller.mSingleButton
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									if (mLeftBtnOnClickListener != null) {
										mLeftBtnOnClickListener.onClick(
												controller.mDialogInterface, 0);
									}
									controller.mDialogInterface.dismiss();
								}
							});
				} else if (((mLeftText == null) && (mLeftTextId == 0))
						&& ((mRightText != null) || (mRightTextId > 0))) {
					controller.mLeftButton.setVisibility(View.GONE);
					controller.mRightButton.setVisibility(View.GONE);
					controller.mSingleButton.setVisibility(View.VISIBLE);
					controller.mSingleButton
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									if (mRightBtnOnClickListener != null) {
										mRightBtnOnClickListener.onClick(
												controller.mDialogInterface, 0);
									}
									controller.mDialogInterface.dismiss();
								}
							});
				}
				controller.mLeftButton
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (mLeftBtnOnClickListener != null) {
									mLeftBtnOnClickListener.onClick(
											controller.mDialogInterface, 0);
								}
								controller.mDialogInterface.dismiss();
							}
						});
				controller.mRightButton
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (mRightBtnOnClickListener != null) {
									mRightBtnOnClickListener.onClick(
											controller.mDialogInterface, 1);
								}
								controller.mDialogInterface.dismiss();
							}
						});
				if (mCheckedBoxText != null) {
					controller.mCheckBox.setVisibility(View.VISIBLE);
					controller.mCheckBox.setText(mCheckedBoxText);
					if (mCheckedBoxOnClickListener != null) {
						controller.mCheckBox
								.setOnClickListener(mCheckedBoxOnClickListener);
					}
				}

			} else if (mMode == AlertParams.MULTI_MODE) {
				controller.meditAndIconArea.setVisibility(View.VISIBLE);
				controller.mBtnArea.setVisibility(View.VISIBLE);
				controller.mDialogListview.setVisibility(View.GONE);
				controller.mTopTextArea.setVisibility(View.GONE);
				controller.meditTextArea.setVisibility(View.GONE);
				controller.mMessageTextView.setText(mMessageText);
				controller.mIconImageView
						.setBackgroundResource(android.R.color.transparent);
				controller.mIconImageView.setImageResource(mIconImageResource);
				controller.mLeftButton.setText(mLeftTextId);
				controller.mLeftButton
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (mLeftBtnOnClickListener != null) {
									mLeftBtnOnClickListener.onClick(
											controller.mDialogInterface, 0);
								}
								controller.mDialogInterface.dismiss();
							}
						});
				controller.mRightButton.setText(mRightTextId);
				controller.mRightButton
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (mRightBtnOnClickListener != null) {
									mRightBtnOnClickListener.onClick(
											controller.mDialogInterface, 1);
								}
								controller.mDialogInterface.dismiss();
							}
						});
			} else if (mMode == AlertParams.EDIT_MODE) {
				controller.meditAndIconArea.setVisibility(View.GONE);
				controller.mBtnArea.setVisibility(View.VISIBLE);
				controller.mDialogListview.setVisibility(View.GONE);
				controller.mTopTextArea.setVisibility(View.GONE);
				controller.meditTextArea.setVisibility(View.VISIBLE);
				controller.mEditTextContent
						.addTextChangedListener(new TextWatcher() {

							@Override
							public void onTextChanged(CharSequence s,
									int start, int before, int count) {
							}

							@Override
							public void beforeTextChanged(CharSequence s,
									int start, int count, int after) {
							}

							@Override
							public void afterTextChanged(Editable s) {
								if (s.length() >= MAX_INPUT_TEXT_PUBLISH) {
									Toast.makeText(
											mContext,
											mContext.getString(
													R.string.wine_max_char_warning,
													MAX_INPUT_TEXT_PUBLISH),
											Toast.LENGTH_SHORT).show();
								}
							}
						});
				controller.mLeftButton.setText(mLeftTextId);
				controller.mLeftButton
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (mLeftBtnOnClickListener != null) {
									mLeftBtnOnClickListener.onClick(
											controller.mDialogInterface, 0);
								}
							}
						});
				controller.mRightButton.setText(mRightTextId);
				controller.mRightButton
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (mRightBtnOnClickListener != null) {
									mRightBtnOnClickListener.onClick(
											controller.mDialogInterface, 1);
								}
							}
						});
			}
			// 在自定义Dialog的body的模式下，需要调整不同的模块的显示方式
			if (mMode == AlertParams.CUSTOM_MODE) {
				controller.mBtnArea.setVisibility(View.VISIBLE);
				controller.mDialogListview.setVisibility(View.GONE);
				controller.mTopTextArea.setVisibility(View.VISIBLE);
				controller.mTopTextView.setVisibility(View.GONE);
				controller.mTextAreaWrapper.setVisibility(View.GONE);
				controller.meditAndIconArea.setVisibility(View.GONE);
				controller.meditTextArea.setVisibility(View.GONE);
				controller.mCustomView.setVisibility(View.VISIBLE);
				FrameLayout custom = (FrameLayout) controller.mCustomView;
				custom.addView(mView, new LayoutParams(MATCH_PARENT,
						MATCH_PARENT));
			}
		}
	}

	public static class Builder {

		private AlertParams P = null;

		private Context mContext;

		public Builder(Context context) {
			mContext = context;
			P = new AlertParams(mContext);
		}

		/*
		 * 设置dialog显示的模式 AlertParams。BUTTON_MODE为只包含标题文字以及左右两个按钮的模式
		 * AlertParams.LIST_MODE包含一个listview
		 * AlertParams。MULTI_MODE包含图标，文字，可编辑文字区域以及两个按钮的模式
		 */
		public Builder setMode(int mode) {
			P.mMode = mode;
			return this;
		}

		public WeddingDialog create() {
			final WeddingDialog dialog = new WeddingDialog(P.mContext);
			P.apply(dialog.mController);
			return dialog;
		}

		public WeddingDialog show() {
			WeddingDialog dialog = create();
			dialog.show();
			return dialog;
		}

		public Builder setTitle(CharSequence title) {
			P.mTitleText = title;
			return this;
		}

		public Builder setTitle(int titleId) {
			P.mTitleId = titleId;
			return this;
		}

		/*
		 * 设置listview的内容，mainTitle为主标题，subTitle为副标题
		 * 副标题可以为空字符串"",当副标题为空字符串时，该行的副标题将不会显示
		 */
		public Builder setListContent(String[] mainTitle, String[] subTitle) {
			if (mainTitle.length != subTitle.length)
				return null;
			P.mMainTitle = mainTitle;
			P.mSubTitle = subTitle;
			return this;
		}

		/*
		 * 设置listview的内容，mainTitle为主标题，subTitle为副标题。这两个参数为xml资源中的string-array
		 * 副标题可以为空字符串"",当副标题为空字符串时，该行的副标题将不会显示
		 */
		public Builder setListContent(int mainTitleStrings, int subTitleStrings) {
			P.mMainTitle = mContext.getResources().getStringArray(
					mainTitleStrings);
			P.mSubTitle = mContext.getResources().getStringArray(
					subTitleStrings);
			return this;
		}

		/*
		 * 设置右按键的文字以及OnClickListener
		 */
		public Builder setRightButton(int textId,
				final DialogInterface.OnClickListener listener) {
			P.mRightTextId = textId;
			P.mRightBtnOnClickListener = listener;
			return this;
		}

		/*
		 * 设置左按键的文字以及OnClickListener
		 */
		public Builder setLeftButton(int textId,
				final DialogInterface.OnClickListener onClickListener) {
			P.mLeftTextId = textId;
			P.mLeftBtnOnClickListener = onClickListener;
			return this;
		}

		public Builder setNegativeButton(int textId,
				DialogInterface.OnClickListener listener) {
			setLeftButton(textId, listener);
			return this;
		}

		public Builder setPositiveButton(int textId,
				DialogInterface.OnClickListener listener) {
			setRightButton(textId, listener);
			return this;
		}

		public Builder setNegativeButton(CharSequence text,
				DialogInterface.OnClickListener listener) {
			P.mLeftText = text;
			P.mLeftBtnOnClickListener = listener;
			return this;
		}

		public Builder setPositiveButton(CharSequence text,
				DialogInterface.OnClickListener listener) {
			P.mRightText = text;
			P.mRightBtnOnClickListener = listener;
			return this;
		}

		/*
		 * 设置LIST_MODE模式下listview的ItemClickListener
		 */
		public Builder setItemClickListener(
				final AdapterView.OnItemClickListener onItemClickListener) {
			P.mOnItemClickListener = onItemClickListener;
			return this;
		}

		/*
		 * 设置BUTTON_MODE模式下，顶部标题位置处的文字资源
		 */
		public Builder setTopText(int textId) {
			P.mTopTextId = textId;
			return this;
		}

		public Builder setMessage(int textId) {
			P.mTopTextId = textId;
			return this;
		}

		public Builder setMessage(CharSequence message) {
			P.mTopText = message;
			return this;
		}

		/*
		 * 设置MULTI_MODE模式下，顶部位置图片右侧的文字资源
		 */
		public Builder setMessageText(int textId) {
			P.mMessageText = textId;
			return this;
		}

		/*
		 * 设置MULTI_MODE模式下，顶部位置的图片资源
		 */
		public Builder setIconImageResource(int imageResourceId) {
			P.mIconImageResource = imageResourceId;
			return this;
		}

		public Builder setItems(String[] items,
				DialogInterface.OnClickListener listener) {
			P.mMainTitle = items;
			P.mListViewDialogOnClickListener = listener;
			return this;
		}

		public Builder setCheckBox(CharSequence msg,
				View.OnClickListener listener) {
			P.mCheckedBoxText = msg;
			P.mCheckedBoxOnClickListener = listener;
			return this;
		}

		public Builder setView(View view) {
			P.mView = view;
			return this;
		}
	}

}
