package com.marriage.dialog;

import com.marriage.MApp;
import com.marriage.R;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

public class ThreeButtonDialog extends DialogFragment implements
		OnClickListener {

	private Button button1;
	private Button button2;
	private Button button3;

	private int btnResId1, btnResId2, btnResId3;

	private OnButtonClickListener listener;

	public static ThreeButtonDialog newInstance(int resId1, int resId2,
			int resId3, OnButtonClickListener l) {
		ThreeButtonDialog instance = new ThreeButtonDialog();
		instance.btnResId1 = resId1;
		instance.btnResId2 = resId2;
		instance.btnResId3 = resId3;
		instance.listener = l;
		return instance;
	}

	@Override
	public int getTheme() {
		return R.style.add_dialog_theme;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.dialog_three_button, container,
				false);
		button1 = (Button) view.findViewById(R.id.button1);
		button2 = (Button) view.findViewById(R.id.button2);
		button3 = (Button) view.findViewById(R.id.button3);

		button1.setText(btnResId1);
		button2.setText(btnResId2);
		button3.setText(btnResId3);

		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
		button3.setOnClickListener(this);

		return view;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().setGravity(Gravity.BOTTOM);

		return dialog;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Dialog dialog = getDialog();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = MApp.get().getResources().getDisplayMetrics().widthPixels;
		dialog.onWindowAttributesChanged(lp);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		if (listener == null) {
			return;
		}
		switch (id) {
		case R.id.button1:
			listener.onClick(0, v);
			break;
		case R.id.button2:
			listener.onClick(1, v);
			break;
		case R.id.button3:
			listener.onClick(2, v);
			break;
		}
	}

	public interface OnButtonClickListener {
		public void onClick(int position, View v);
	}
}
