package com.marriage.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import com.marriage.MApp;
import com.marriage.R;

@SuppressLint("ValidFragment")
public class ShareDialog extends DialogFragment {

	private OnClickListener mL;

	public ShareDialog(OnClickListener l) {
		mL = l;
	}

	@Override
	public int getTheme() {
		return R.style.add_dialog_theme;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_share_platform, container,
				false);
		view.findViewById(R.id.share_sms).setOnClickListener(mL);
		view.findViewById(R.id.share_mail).setOnClickListener(mL);
		view.findViewById(R.id.share_weixin).setOnClickListener(mL);
		view.findViewById(R.id.share_weixin_cycle).setOnClickListener(mL);
		view.findViewById(R.id.share_weibo).setOnClickListener(mL);
		view.findViewById(R.id.share_qq).setOnClickListener(mL);
		view.findViewById(R.id.share_cancle).setOnClickListener(mL);
		return view;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().setGravity(Gravity.BOTTOM);

		return dialog;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Dialog dialog = getDialog();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = MApp.get().getResources().getDisplayMetrics().widthPixels;
		dialog.onWindowAttributesChanged(lp);
	}

}
