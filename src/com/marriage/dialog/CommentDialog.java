package com.marriage.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.marriage.MApp;
import com.marriage.R;
import com.marriage.utility.ToastHelper;

public class CommentDialog extends DialogFragment implements
		View.OnClickListener {

	private Button sendComment;
	private EditText commentInput;

	private CommentSendExecutor commentSendExecutor;
	private int mHint;
	
	@Override
	public int getTheme() {
		return R.style.add_dialog_theme;
	}

	public CommentDialog() {}
	
	public CommentDialog(int hint) {
		mHint = hint;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.view_comment_dialog, container,
				false);
		sendComment = (Button) view.findViewById(R.id.comment_dialog_btn);
		sendComment.setOnClickListener(this);
		commentInput = (EditText) view.findViewById(R.id.comment_dialog_text);
		if(mHint != 0) {
			commentInput.setHint(mHint);
		}
		return view;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().setGravity(Gravity.BOTTOM);

		return dialog;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Dialog dialog = getDialog();
		WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = MApp.get().getResources().getDisplayMetrics().widthPixels;
		dialog.onWindowAttributesChanged(lp);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.comment_dialog_btn:
			goSendComment();
		}
		this.dismiss();
	}

	public void setCommentSendExecutor(CommentSendExecutor executor){
		this.commentSendExecutor = executor;
	}
	
	private void goSendComment() {
		String comment = commentInput.getText().toString().trim();
		if (TextUtils.isEmpty(comment)) {
			ToastHelper
					.showShortToast(R.string.please_input_your_cooment_first);
			return;
		}
		commentInput.setText(null);
		// TODO send comment
		if (commentSendExecutor != null){
			commentSendExecutor.sendComment(comment);
		}
	}
	
	public interface CommentSendExecutor{
		public void sendComment(String comment);
	}
}
