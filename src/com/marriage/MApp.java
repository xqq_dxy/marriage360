package com.marriage;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap.Config;
import android.location.Location;
import android.location.LocationManager;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.marriage.bean.City;
import com.marriage.bean.CityInfo;
import com.marriage.bean.UserInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.LocationHelper;
import com.marriage.utility.LocationHelper.LocationCallback;
import com.marriage.utility.PreferenceHelper;
import com.marriage.utility.PreferenceHelper.PreferenceKeys;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MApp extends Application implements LocationCallback{

	private ImageLoaderConfiguration mImageLoaderConfiguration;

	private static MApp c;
	
	private static final boolean DEBUG = true;
	
	private UserInfo user;
	
	private Location mLocation;
	private City mCity;
	
	private LocationHelper mLocationHelper;
	private LoginChangedListener listener;
	private List<LoginChangedListener> mLoginListeners;
	
	/**
	 * 全局唯一
	 */
	private static RequestQueue mQueue;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		init();
	}

	private void init() {
		initDebug();
		DisplayImageOptions dio = new DisplayImageOptions.Builder()
				.cacheInMemory(true).cacheOnDisk(true)
				.bitmapConfig(Config.RGB_565).build();
		mImageLoaderConfiguration = new ImageLoaderConfiguration.Builder(this)
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.defaultDisplayImageOptions(dio).build();
		ImageLoader.getInstance().init(mImageLoaderConfiguration);
		c = this;
		initConfig();
		loadUserFromFile();
		mLocationHelper = new LocationHelper(this, this);
	}

	private void initDebug(){
		D.setEnable(DEBUG, DEBUG, DEBUG, DEBUG, DEBUG, DEBUG);
		D.setPackageName(getPackageName());
		D.setFileLogEnable(DEBUG);
	}
	
	public static MApp get(){
		return c;
	}
	
	private void initConfig(){
		AppConfig.INSTANCE.SERVER = getString(R.string.SERVER);
	}
	
	public void logout(){
		this.user = null;
		PreferenceHelper.setString(PreferenceKeys.USER_INFO, "");
		if(listener != null) {
			listener.onLoginChanged(false);
		}
		if(mLoginListeners != null) {
			for(int i=0; i<mLoginListeners.size(); i++) {
				mLoginListeners.get(i).onLoginChanged(false);
			}
		}
	}
	
	public void setUser(UserInfo _user){
		this.user = _user;
		try {
			String toString = CommonUtils.toString(_user);
			PreferenceHelper.setString(PreferenceKeys.USER_INFO, toString);
			if(listener != null) {
				listener.onLoginChanged(true);
			}
			if(mLoginListeners != null) {
				for(int i=0; i<mLoginListeners.size(); i++) {
					mLoginListeners.get(i).onLoginChanged(true);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public UserInfo getUser(){
		if (user == null){
			loadUserFromFile();
		}
		return user;
	}
	
	public boolean isLogin() {
		return getUser() != null && getUser().user != null;
	}
	
	private void loadUserFromFile(){
		String u = PreferenceHelper.getString(PreferenceKeys.USER_INFO, "");
		try {
			user = (UserInfo) CommonUtils.fromString(u);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}
	
	public Location getCurrentLocation(){
		refereshLocation();
		return mLocation;
	}
	
	public void refereshLocation(){
		mLocationHelper.requestLocation();
	}

	@Override
	public void onLocationReady(Location location) {
		// TODO Auto-generated method stub
		mLocation = location;
	}
	
	public Location getLastKnowLocation() {
		if(mLocation != null) {
			refereshLocation();
			return mLocation;
		} else {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			mLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if(mLocation == null) {
				mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			}
			return mLocation;
		}
	}
	
	public City getCityFromServer(final Listener<City> l) {
		if(mCity != null)
			return mCity;
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_GET_CITY_INFO;
		StringRequest req = new StringRequest(Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String response) {
				// TODO Auto-generated method stub
				CityInfo cityInfo = new Gson().fromJson(response, CityInfo.class);
				if(cityInfo.city != null && !cityInfo.city.isEmpty()) {
					City city = cityInfo.city.get(0);
					city.id = city.city_id;
					city.name = city.city_name;
					mCity = city;
					l.onResponse(mCity);
				}
			}
		}, null);
		getRequestQueue().add(req);
		return null;
	}

	public RequestQueue getRequestQueue() {
		if (mQueue == null) {
			mQueue = Volley.newRequestQueue(this);
		}
		
		if(BuildConfig.DEBUG) {
//			VolleyLog.DEBUG = true;
		}
		return mQueue;
	}
	
	public void setLoginChanged(LoginChangedListener listener) {
		this.listener = listener;
	}
	
	public void addLoginChangedListener(LoginChangedListener listener) {
		if(mLoginListeners == null) {
			mLoginListeners = new ArrayList<MApp.LoginChangedListener>();
		}
		if(!mLoginListeners.contains(listener)) {
			mLoginListeners.add(listener);
		}
	}
	
	public void removeLoginChangedListener(LoginChangedListener listener) {
		if(mLoginListeners == null) {
			mLoginListeners = new ArrayList<MApp.LoginChangedListener>();
		}
		mLoginListeners.remove(listener);
	}
	
	public interface LoginChangedListener {
		void onLoginChanged(boolean isLogin);
	}
}

