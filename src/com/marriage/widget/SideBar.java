package com.marriage.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SectionIndexer;

public class SideBar extends View {
	private char[] l;
	private SectionIndexer sectionIndexter = null;
	private ExpandableListView list;
	private int m_nItemHeight = 29;
	private Context mContext;
	private Paint paint = new Paint();

	public SideBar(Context context) {
		super(context);
		mContext = context;
		init();
	}

	public SideBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		init();
	}

	public SideBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		init();
	}

	private void init() {
		l = new char[] { '全','同','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
				'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
				'X', 'Y', 'Z' };
		setBackgroundColor(0x44FFFFFF);
	}

	public void init(char[] index) {
		l = index;
		setBackgroundColor(0x44FFFFFF);
		this.invalidate();
	}

	public void setListView(ExpandableListView _list, SectionIndexer index) {
		list = _list;
		sectionIndexter = index;
	}

	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		int i = (int) event.getY();
		int idx = i / m_nItemHeight;
		if (idx >= l.length) {
			idx = l.length - 1;
		} else if (idx < 0) {
			idx = 0;
		}
		if (event.getAction() == MotionEvent.ACTION_DOWN
				|| event.getAction() == MotionEvent.ACTION_MOVE) {
			setBackgroundColor(0xffE9E9E9);
			if (sectionIndexter == null) {
				sectionIndexter = (SectionIndexer) list
						.getExpandableListAdapter();// list.getAdapter();s
			}
			int position = sectionIndexter.getPositionForSection(l[idx]);
			if (position == -1) {
				return true;
			}
			list.setSelectedGroup(position);
		} else {
			setBackgroundColor(0x44FFFFFF);
		}
		return true;
	}

	@SuppressLint("DrawAllocation")
	protected void onDraw(Canvas canvas) {
		paint.setColor(0xFFA6A9AA);
		float density = mContext.getResources().getDisplayMetrics().density;
		paint.setTextSize(13 * density);
		paint.setTextAlign(Paint.Align.CENTER);
		float widthCenter = getMeasuredWidth() / 2;
		m_nItemHeight = getMeasuredHeight() / l.length;
		for (int i = 0; i < l.length; i++) {
			canvas.drawText(String.valueOf(l[i]), widthCenter,
					(i * m_nItemHeight) + m_nItemHeight, paint);
		}
		paint.reset();
		super.onDraw(canvas);
	}
}