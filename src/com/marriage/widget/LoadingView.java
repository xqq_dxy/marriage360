package com.marriage.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marriage.R;
import com.marriage.utility.CommonUtils;

public class LoadingView extends LinearLayout {

	private ImageView _loading;
	private TextView _loadingText;
	
	public LoadingView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		iniView(context);
	}
	
	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		iniView(context);
	}
	
	private void iniView(Context context){
		this.setGravity(Gravity.CENTER);
		this.setBackgroundColor(Color.WHITE);
		this.setOrientation(HORIZONTAL);
		
		_loading = new ImageView(context);
		_loading.setBackgroundResource(R.anim.loadinganim_80_80);
		LinearLayout.LayoutParams loadingImageLayoutParams = 
			new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		this.addView(_loading, loadingImageLayoutParams);
		
		_loadingText = new TextView(context);
		_loadingText.setTextColor(context.getResources().getColor(R.color.top_text_color));
		_loadingText.setTextSize(13);
		_loadingText.setText(R.string.loading_text);
		LinearLayout.LayoutParams loadingTextLayoutParams = 
				new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		loadingTextLayoutParams.leftMargin = CommonUtils.dip2Px(context, 5);
		this.addView(_loadingText, loadingTextLayoutParams);
		
	}

	
	public void show(){
		_loading.setVisibility(View.VISIBLE);
		_loadingText.setVisibility(View.VISIBLE);
	}
	
	public void dismiss(){
		_loading.setVisibility(View.GONE);
		_loadingText.setVisibility(View.GONE);
	}
	
	public boolean isShowing(){
		return (_loading.getVisibility() == View.VISIBLE) && (_loadingText.getVisibility() == View.VISIBLE);
	}
	
	@Override
	protected void onAttachedToWindow() {
		// TODO Auto-generated method stub
		super.onAttachedToWindow();
		this.post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				AnimationDrawable ann = (AnimationDrawable) _loading.getBackground();
				ann.start();
			}
		});
	}

	@Override
	protected void onDetachedFromWindow() {
		// TODO Auto-generated method stub
		super.onDetachedFromWindow();
		this.post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				AnimationDrawable ann = (AnimationDrawable) _loading.getBackground();
				ann.stop();
			}
		});
	}

}
