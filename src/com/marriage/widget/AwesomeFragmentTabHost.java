package com.marriage.widget;

import android.content.Context;
import android.support.v4.app.FragmentTabHost;
import android.text.TextUtils;
import android.util.AttributeSet;

public class AwesomeFragmentTabHost extends FragmentTabHost {

	protected InterceptTabListener interceptTabListener;
	
	private String cachedLastTag = "";
	
	public AwesomeFragmentTabHost(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public AwesomeFragmentTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		if (interceptTabListener != null && interceptTabListener.onInterceptTab(tabId)){
			if (!TextUtils.isEmpty(cachedLastTag)){
				setCurrentTabByTag(cachedLastTag);
			}
		}else{
			super.onTabChanged(tabId);
			cachedLastTag = tabId;
		}
		
	}
	
	public void setInterceptTabListener(InterceptTabListener l){
		interceptTabListener = l;
	}
	
	public interface InterceptTabListener{
		public boolean onInterceptTab(String tabId);
	}

}
