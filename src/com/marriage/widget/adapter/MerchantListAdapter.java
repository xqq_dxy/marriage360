package com.marriage.widget.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.marriage.R;
import com.marriage.bean.Merchant;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class MerchantListAdapter extends CommonBaseAdapter<Merchant> {

	public MerchantListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public MerchantListAdapter(Context context, List<Merchant> _datas) {
		super(context, _datas);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		Holder holder = null;
		if (v == null) {
			v = inflater.inflate(R.layout.item_list_merchant, parent, false);
			holder = new Holder(v);
			v.setTag(holder);
		} else {
			holder = (Holder) v.getTag();
		}
		bindViewData(position, holder);
		return v;
	}
	
	private void bindViewData(int position,Holder holder){
		Merchant data = getItem(position);
		holder.icon.setImageDrawable(null);
		ImageLoader.getInstance().displayImage(data.index_img, holder.icon,new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				// TODO Auto-generated method stub
				arg1.setBackgroundColor(Color.WHITE);
			}
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				// TODO Auto-generated method stub
				arg1.setBackgroundDrawable(null);
			}
			
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
				// TODO Auto-generated method stub
				
			}
		});
		holder.name.setText(data.sl_name);
		holder.publish.setText(context.getString(R.string.merchant_publish_number, new Object[]{data.topic_count}));
		holder.fans.setText(context.getString(R.string.merchant_fans_number, new Object[]{data.focused_count}));
		holder.type.setText(data.sa_cate_name);
		holder.area.setText(data.area_name);
		String mergedDistance = mergeDistance(data.sa_distance);
		holder.distance.setText(mergedDistance);
		if (TextUtils.isEmpty(data.area_name) && TextUtils.isEmpty(mergedDistance)){
			holder.location.setVisibility(View.GONE);
		}else{
			holder.location.setVisibility(View.VISIBLE);
		}
	}

	private String mergeDistance(String distance){
		if (TextUtils.isEmpty(distance) || "null".equals(distance) || "-1".equals(distance)){
			return "";
		}
		double d = Double.valueOf(distance);
		int dInteger = (int) d;
		if (dInteger < 1000){
			return dInteger + "m";
		}
		double kmd = dInteger/1000d;
		DecimalFormat df = new DecimalFormat("#.0");
		return df.format(kmd) + "km";
	}
	
	private class Holder {

		ImageView icon;
		TextView name;
		TextView publish;
		TextView fans;
		TextView type;
		TextView area;
		TextView distance;
		View location;

		Holder(View v) {
			icon = (ImageView) v.findViewById(R.id.merchant_icon);
			name = (TextView) v.findViewById(R.id.merchant_name);
			publish = (TextView) v.findViewById(R.id.merchant_publish);
			fans = (TextView) v.findViewById(R.id.merchant_fans);
			type = (TextView) v.findViewById(R.id.merchant_type);
			area = (TextView) v.findViewById(R.id.merchant_area);
			distance = (TextView) v.findViewById(R.id.merchant_distance);
			location = v.findViewById(R.id.merchant_location);
			resizeIcon();
		}
		
		private void resizeIcon(){
			int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
			int iconWidth = screenWidth/3;
			LayoutParams params = icon.getLayoutParams();
			params.width = iconWidth;
			params.height = iconWidth;
			icon.setLayoutParams(params);
		}
	}
}
