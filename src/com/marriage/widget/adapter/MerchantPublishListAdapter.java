package com.marriage.widget.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.marriage.R;
import com.marriage.bean.Topics;
import com.marriage.widget.ScaleImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MerchantPublishListAdapter extends CommonBaseAdapter<Topics> implements OnCreateContextMenuListener, OnClickListener{

	private OnCreateContextMenuListener mContextListener;
	private OnClickListener mClickListener;
	
	public MerchantPublishListAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_merchant_publish, parent, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Topics data = getItem(position);
		holder.img.setImageWidth(data.cover_img_w);
		holder.img.setImageHeight(data.cover_img_h);
		holder.img.setImageDrawable(null);
		ImageLoader.getInstance().displayImage(data.cover_img, holder.img);
		
		holder.content.setText(data.topic_content);
		holder.iconNum.setText(data.images_count);
		holder.likeNum.setText(data.fav_count);
//		holder.publishTime.setText(data.)
		// TODO bind data

		convertView.setOnCreateContextMenuListener(this);
		convertView.setOnClickListener(this);
		convertView.setTag(convertView.getId(), position);
		return convertView;
	}

	private class ViewHolder {
		private ScaleImageView img;
		private TextView content, iconNum, likeNum, publishTime;

		private ViewHolder(View v) {
			img = (ScaleImageView) v.findViewById(R.id.item_iv);
			content = (TextView) v.findViewById(R.id.item_content);
			iconNum = (TextView) v.findViewById(R.id.item_images_num);
			likeNum = (TextView) v.findViewById(R.id.item_likes_num);
			publishTime = (TextView) v.findViewById(R.id.item_time);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		if(mContextListener != null) {
			mContextListener.onCreateContextMenu(menu, v, menuInfo);
		}
	}
	
	public void setOnCreateContextMenuListener(OnCreateContextMenuListener l) {
		mContextListener = l;
	}
	
	public void setOnClickListener(OnClickListener l) {
		mClickListener = l;
	}

	@Override
	public void onClick(View v) {
		if(mClickListener != null) {
			mClickListener.onClick(v);
		}
	}
	
	public void onTopicDelete(String topicId){
		if (TextUtils.isEmpty(topicId)){
			return;
		}
		int count = getCount();
		for(int i=0;i<count;i++){
			Topics t = getItem(i);
			if (t.topic_id.equals(topicId)){
				removeData(t);
				break;
			}
		}
	}

}
