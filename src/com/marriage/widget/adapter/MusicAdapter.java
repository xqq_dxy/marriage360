package com.marriage.widget.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marriage.R;
import com.marriage.bean.Music;

public class MusicAdapter extends CommonBaseAdapter<Music> {
	
	private int mChosePosition = -1;
	
	public MusicAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;
		View v = convertView;
		if (v == null){
			v = inflater.inflate(R.layout.item_list_music, parent, false);
			holder = new Holder(v);
			v.setTag(holder);
		}else{
			holder = (Holder) v.getTag();
		}
		if (mChosePosition == position){
			holder.musicChoseTagView.setVisibility(View.VISIBLE);
		}else{
			holder.musicChoseTagView.setVisibility(View.INVISIBLE);
		}
		holder.musicNameView.setText(getItem(position).name);
		return v;
	}
	
	public void setChosedPosition(int p){
		this.mChosePosition = p;
		notifyDataSetChanged();
	}
	
	public int getChosedPosition(){
		return this.mChosePosition;
	}
	
	private class Holder{
		private TextView musicNameView;
		private ImageView musicChoseTagView;
		private Holder(View v){
			musicNameView = (TextView) v.findViewById(R.id.music_name);
			musicChoseTagView = (ImageView) v.findViewById(R.id.music_choose_tag);
		}
	}

}
