package com.marriage.widget.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.marriage.R;
import com.marriage.utility.CommonUtils;

public class MerchantCategoryAdapter extends CommonBaseAdapter<String> {

	public MerchantCategoryAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public MerchantCategoryAdapter(Context context, List<String> _datas) {
		super(context, _datas);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		TextView v = (TextView) convertView;
		if (v == null){
			v = new TextView(context);
			v.setPadding(CommonUtils.dip2Px(context, 15), CommonUtils.dip2Px(context, 10), CommonUtils.dip2Px(context, 5), CommonUtils.dip2Px(context, 10));
			v.setBackgroundColor(Color.WHITE);
			v.setTextSize(15);
			v.setTextColor(context.getResources().getColor(R.color.edit_text_color));
		}
		v.setText(getItem(position));
		return v;
	}

}
