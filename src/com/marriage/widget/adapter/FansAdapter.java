package com.marriage.widget.adapter;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marriage.MApp;
import com.marriage.R;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.Fans;
import com.marriage.bean.UserInfo;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.ToastHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FansAdapter extends CommonBaseAdapter<Fans> implements
		View.OnClickListener {

	private LoadingListener mLoadingListener;
	
	private boolean isMerchantFans;
	
	public FansAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setLoadingListener(LoadingListener _l){
		mLoadingListener = _l;
	}

	public void setIsMerchantFans(boolean isMerchantFans){
		this.isMerchantFans = isMerchantFans;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		Fans data = getItem(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_list_mine_fans, parent, false);
			holder.avatar = (ImageView) convertView
					.findViewById(R.id.fans_avatar);
			holder.name = (TextView) convertView.findViewById(R.id.fans_name);
			holder.city = (TextView) convertView.findViewById(R.id.fans_city);
			holder.followBtn = (TextView) convertView
					.findViewById(R.id.follow_he);
			if (isMerchantFans){
				holder.followBtn.setVisibility(View.GONE);
			}
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		ImageLoader.getInstance().displayImage(data.user_head_img, holder.avatar, ImageUtil.getCircleDisplayImageOptions());
		holder.name.setText(data.focus_user_name);
		holder.city.setText(data.focus_user_city_name);
		holder.followBtn.setOnClickListener(this);
		holder.followBtn.setTag(data);
		/*if (data.is_sa.toUpperCase(Locale.ENGLISH).equals("TRUE")){
			holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.v_icon_small, 0);
		}else{
			holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}*/
		if (data.fucus_flag.toUpperCase(Locale.ENGLISH).equals("TRUE")){
			holder.followBtn.setText(R.string.string_has_followed);
			holder.followBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.added_follow, 0, 0, 0);
		}else{
			holder.followBtn.setText(R.string.string_add_follow);
			holder.followBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.add_follow, 0, 0, 0);
		}
		return convertView;
	}

	private class ViewHolder {
		ImageView avatar;
		TextView name, city, followBtn;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Fans d = (Fans) v.getTag();
		if (!d.fucus_flag.toUpperCase(Locale.ENGLISH).equals("TRUE")){
			doFoucus(d);
		}
	}
	
	private void doFoucus(final Fans data) {
		if (mLoadingListener != null){
			mLoadingListener.onLoading();
		}
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_ADD_FOLLOW;
		StringRequest sr = new StringRequest(Method.POST, url,
				new Listener<String>() {

					@Override
					public void onResponse(String response) {
						if (mLoadingListener != null){
							mLoadingListener.dismisLoading();
						}
						BaseApiResult info = null;
						try {
							info = new Gson().fromJson(response,
									BaseApiResult.class);
						} catch (JsonSyntaxException e) {
							e.printStackTrace();
						}
						if (Constants.SUCCESS.equals(info.op_code)) {
							data.fucus_flag = "true";
							notifyDataSetChanged();
						} else {
							ToastHelper.showShortToast(info.op_info);
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						if (mLoadingListener != null){
							mLoadingListener.dismisLoading();
						}
						ToastHelper.showShortToast(R.string.net_err);
					}
				}) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> map = new HashMap<String, String>();
				UserInfo user = MApp.get().getUser();
				map.put("focus_user_id", user.user.user_id);
				map.put("focus_user_name", user.user.user_name);
				map.put("focused_user_id", data.focus_user_id);
				map.put("focused_user_name", data.focus_user_name);
				map.put("focus_flag", "1");
				return map;
			}

		};
		MApp.get().getRequestQueue().add(sr);
	}
	
	public interface LoadingListener{
		public void onLoading();
		public void dismisLoading();
	}

}