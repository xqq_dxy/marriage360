package com.marriage.widget.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.marriage.R;
import com.marriage.delegate.InputInfoMakeCardImpl.CardImgObj;
import com.marriage.utility.CommonUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class CardImageAdapter extends CommonBaseAdapter<CardImgObj> {

	
	public CardImageAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		View v = convertView;
		CardImgObj data = getItem(position);
		
		if (v == null){
			v = inflater.inflate(R.layout.item_grid_card_img, parent, false);
			holder = new Holder(v);
			v.setTag(holder);
		}else{
			holder = (Holder) v.getTag();
		}
		if (position == 0){
			holder.coverMask.setVisibility(View.VISIBLE);
		}else{
			holder.coverMask.setVisibility(View.GONE);
		}
		String t = data.imgUrl;
		resizeImage(holder.imgView);
		if (t != null){
			ImageLoader.getInstance().displayImage(t, holder.imgView,new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					// TODO Auto-generated method stub
					arg1.setBackgroundColor(Color.WHITE);
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					// TODO Auto-generated method stub
					arg1.setBackgroundColor(Color.WHITE);
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
			});
		}else{
			holder.imgView.setImageBitmap(null);
			holder.imgView.setBackgroundResource(R.drawable.add_img);
		}
		return v;
		
	}
	
	private void resizeImage(View v){
		int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
		int margin = CommonUtils.dip2Px(context, 5*10);
		int vWidth = (screenWidth-margin)/4;
		v.getLayoutParams().width = vWidth;
		v.invalidate();
	}
	
	private class Holder{
		private ImageView imgView;
		private View coverMask;
		public Holder(View v){
			imgView = (ImageView) v.findViewById(R.id.card_img);
			coverMask = v.findViewById(R.id.card_img_mask);
		}
	}
	
}
