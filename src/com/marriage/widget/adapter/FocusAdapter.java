package com.marriage.widget.adapter;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.ProcessListener;
import com.marriage.R;
import com.marriage.bean.BaseApiResult;
import com.marriage.bean.Focus;
import com.marriage.dialog.WeddingDialog;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;
import com.marriage.utility.ImageUtil;
import com.marriage.utility.ToastHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FocusAdapter extends CommonBaseAdapter<Focus> implements OnClickListener{

	private static final String TAG = "FocusAdapter";
	
	private ProcessListener processListener;
	
	public FocusAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public void setProcessListener(ProcessListener l){
		this.processListener = l;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		Focus data = getItem(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_list_mine_focus, parent, false);
			holder.avatar = (ImageView) convertView
					.findViewById(R.id.fans_avatar);
			holder.name = (TextView) convertView.findViewById(R.id.fans_name);
			holder.city = (TextView) convertView.findViewById(R.id.fans_city);
			holder.cancelFocus = (Button) convertView.findViewById(R.id.follow_he);
			holder.cancelFocus.setTag(position);
			holder.cancelFocus.setOnClickListener(this);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		ImageLoader.getInstance().displayImage(data.user_head_img,
				holder.avatar, ImageUtil.getCircleDisplayImageOptions());
		holder.name.setText(TextUtils.isEmpty(data.focused_user_name) ? ""
				: data.focused_user_name);
		holder.city.setText(TextUtils.isEmpty(data.focused_user_city_name) ? ""
				: data.focused_user_city_name);
		/*if (data.is_sa.toUpperCase(Locale.ENGLISH).equals("TRUE")) {
			holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.v_icon_small, 0);
		} else {
			holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}*/
		return convertView;
	}

	private class ViewHolder {
		ImageView avatar;
		TextView name, city;
		Button cancelFocus;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int position = (Integer) v.getTag();
		Focus data = getItem(position);
		if (data != null){
			showCancelFocusDialog(data);
		}
	}
	
	private void showCancelFocusDialog(final Focus data){
		new WeddingDialog.Builder(context).setMessage(R.string.confirm_cancel_focus).setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				doCancelFocus(data);
			}
		}).setNegativeButton(R.string.cancel, null).show();
	}
	
	private void doCancelFocus(final Focus data){
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_ADD_FOLLOW;
		StringRequest sr = new StringRequest(Method.POST, url, new Listener<String>() {

			@Override
			public void onResponse(String arg0) {
				// TODO Auto-generated method stub
				if (processListener != null){
					processListener.onFinish();
				}
				D.info(TAG, "cancel focus ret: "+arg0);
				try {
					BaseApiResult result = new Gson().fromJson(arg0, BaseApiResult.class);
					if (result != null){
						ToastHelper.showShortToast(result.op_info);
						if (Constants.SUCCESS.equals(result.op_code)){
							datas.remove(data);
							notifyDataSetChanged();
						}
					}else{
						ToastHelper.showShortToast(R.string.net_err);
					}
							
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					ToastHelper.showShortToast(R.string.net_err);
				}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				if (processListener != null){
					processListener.onFinish();
				}
				ToastHelper.showShortToast(R.string.net_err);
			}
		}){

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				Map<String, String> params = new HashMap<String, String>();
				params.put("focus_user_id", MApp.get().getUser().user.user_id);
				params.put("focus_user_name", MApp.get().getUser().user.user_name);
				params.put("focused_user_id", data.focused_user_id);
				params.put("focused_user_name", data.focused_user_name);
				params.put("focus_flag", "-1");
				return params;
			}
			
		};
		if (processListener != null){
			processListener.onStart();
		}
		MApp.get().getRequestQueue().add(sr);
	}
}
