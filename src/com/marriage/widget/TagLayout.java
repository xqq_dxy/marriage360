package com.marriage.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.marriage.OnCityChoiceListener;
import com.marriage.R;
import com.marriage.bean.City;
import com.marriage.fragment.CityFragment;
import com.marriage.utility.D;

public class TagLayout extends LinearLayout implements View.OnClickListener, PopupWindow.OnDismissListener, OnCityChoiceListener {

	private static final int DEFAULT_HEIGHT = 380;		//dp
	private TextView tag1, tag2, tag3;
	private View tagContentView1, tagContentView2, tagContentView3;
	private PopupWindow mCurrentPop;
	private View mAnchor;
	private int mHeight;
	private ViewGroup mRightCity;
	private OnCityChoiceListener mCityChoice;
	private OnTagClickListener mTagListener;
	private String tag1DefaultName,tag2DefaultName,tag3DefaultName;
	private CityFragment mCityFrg;
	private OnItemClickListener mCategoryListener;
	private boolean cityInit;

	public TagLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOrientation(LinearLayout.VERTICAL);
		inflate(getContext(), R.layout.custom_tag_layout, this);
		mHeight = (int)(getContext().getResources().getDisplayMetrics().density * DEFAULT_HEIGHT + 0.5f);
	}
	
	public int getDefaultHeight() {
		return mHeight;
	}

	public void addTag(int index, View layout, String text) {
		if (index < 0 || index > 2) return;
		if (index == 0) {
			tag1.setText(text);
			tagContentView1 = layout;
			tag1DefaultName = text;
		} else if (index == 1) {
			tag2.setText(text);
			tagContentView2 = layout;
			tag2DefaultName = text;
		} else if (index == 2) {
			tag3.setText(text);
			tagContentView3 = layout;
			tag3DefaultName = text;
		}
	}

	public void setOnTagClickListener(OnTagClickListener l){
		mTagListener = l;
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(isInEditMode()) return;
		View view = findViewById(R.id.tag1);
		view.setOnClickListener(this);
		tag1 = (TextView) ((ViewGroup) view).getChildAt(0);

		view = findViewById(R.id.tag2);
		view.setOnClickListener(this);
		tag2 = (TextView) ((ViewGroup) view).getChildAt(0);

		view = findViewById(R.id.tag3);
		view.setOnClickListener(this);
		tag3 = (TextView) ((ViewGroup) view).getChildAt(0);
		
		mAnchor = findViewById(R.id.tag_header);
	}

	
	public boolean clearTag() {
		boolean hasSelected = tag1.isSelected() || tag2.isSelected() || tag3.isSelected();
		tag1.setSelected(false);
		tag2.setSelected(false);
		tag3.setSelected(false);
		return hasSelected;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tag1:
			tag1.setSelected(true);
			tag2.setSelected(false);
			tag3.setSelected(false);
			onTag1Click(v);
			break;
		case R.id.tag2:
			tag1.setSelected(false);
			tag2.setSelected(true);
			tag3.setSelected(false);
			onTag2Click(v);
			break;
		case R.id.tag3:
			tag1.setSelected(false);
			tag2.setSelected(false);
			tag3.setSelected(true);
			onTag3Click(v);
			break;
		/*case R.id.btn_all_country:
			D.debug("MARTIN", "全国");
			dismissPopWindow();
			if(mCityChoice != null)
				mCityChoice.onCityChoice(0, null);
			break;
		case R.id.btn_same_city:
			D.debug("MARTIN", "同城");
			dismissPopWindow();
			if(mCityChoice != null)
				mCityChoice.onCityChoice(1, null);
			break;
		case R.id.btn_choice_city:
			D.debug("MARTIN", "选择城市");
			v.setSelected(true);
			mCityFrg = new CityFragment();
			mCityFrg.setOnCityChoiceListener(this);
			mRightCity.addView(mCityFrg.onCreateView(LayoutInflater.from(getContext()), mRightCity, null));
			break;*/
		}
	}
	
	public void setTagName(int position,String name){
		switch (position) {
		case 0:
			tag1.setText(name);
			break;
		case 1:
			tag2.setText(name);
			break;
		case 2:
			tag3.setText(name);
			break;
		}
	}
	
	public void dismissPopWindow() {
		if(mCurrentPop != null && mCurrentPop.isShowing()) {
			mCurrentPop.dismiss();
		}
		mCurrentPop = null;
	}
	
	private void initPopWindow(View contentView) {
		mCurrentPop = new PopupWindow(contentView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		mCurrentPop.setFocusable(true);
		mCurrentPop.setBackgroundDrawable(new BitmapDrawable());
		mCurrentPop.setOutsideTouchable(true);
		mCurrentPop.update();
		mCurrentPop.setOnDismissListener(this);
	}
	
	private void onTag1Click(View v) {
		if (mTagListener != null){
			mTagListener.onTagClick(0, v);
		}
		dismissPopWindow();
		initPopWindow(tagContentView1);
		mCurrentPop.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#7f000000")));
		mCurrentPop.setHeight(mHeight);
		mCurrentPop.update();
		mCurrentPop.showAsDropDown(mAnchor);
//		tagContentView1.findViewById(R.id.btn_all_country).setOnClickListener(this);
//		tagContentView1.findViewById(R.id.btn_same_city).setOnClickListener(this);
//		tagContentView1.findViewById(R.id.btn_choice_city).setOnClickListener(this);
		
		if(!cityInit) {
			mRightCity = (ViewGroup) tagContentView1.findViewById(R.id.tag_city);
			mCityFrg = new CityFragment();
			mCityFrg.hasSameCity = true;
			mCityFrg.setOnCityChoiceListener(this);
			mRightCity.addView(mCityFrg.onCreateView(LayoutInflater.from(getContext()), mRightCity, null));
			cityInit = true;
		}
		
	}


	private void onTag2Click(View v){
		if (mTagListener != null){
			mTagListener.onTagClick(1, v);
		}
		
		dismissPopWindow();
		initPopWindow(tagContentView2);
		mCurrentPop.setHeight(mHeight);
		mCurrentPop.update();
		mCurrentPop.showAsDropDown(mAnchor);
	}
	
	private void onTag3Click(View v){
		if (mTagListener != null){
			mTagListener.onTagClick(2, v);
		}
		
		dismissPopWindow();
		initPopWindow(tagContentView3);
		mCurrentPop.setHeight(mHeight);
		mCurrentPop.update();
		mCurrentPop.showAsDropDown(mAnchor);
	}
	
	@Override
	public void onDismiss() {
		if(mCityFrg != null) {
			mCityFrg.cancelRequest();
		}
		clearTag();
	}

	@Override
	public void onChoice(City city) {
		D.debug("MARTIN", city.name);
		dismissPopWindow();
		if(mCityChoice != null)
			mCityChoice.onCityChoice(2, city);
	}
	
	public void setOnCityChoiceListener(OnCityChoiceListener l) {
		mCityChoice = l;
	}
	
	public interface OnCityChoiceListener {
		/**
		 * 
		 * @param type type = 0 为全国城市，type=1为同城, type=2 城市拿 data
		 * @param data 
		 */
		void onCityChoice(int type, City data);
	}
	
	public interface OnTagClickListener{
		public void onTagClick(int position,View v);
	}

}
