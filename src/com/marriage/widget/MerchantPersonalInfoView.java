package com.marriage.widget;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.marriage.MApp;
import com.marriage.R;
import com.marriage.activity.MineCommentActivity;
import com.marriage.activity.MineFansActivity;
import com.marriage.activity.MyPublishActivity;
import com.marriage.fragment.MyPublishFragment;
import com.marriage.utility.CommonUtils;
import com.marriage.utility.Constants;

public class MerchantPersonalInfoView extends LinearLayout implements OnClickListener{

	private LinearLayout commentLayout,goodLayout,fansLayout,callLayout;
	private TextView commentText,goodText,fansText,callText;
	private View commentDot,goodDot,fansDot,callDot;
	
	public MerchantPersonalInfoView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		initView(context);
	}
	
	public MerchantPersonalInfoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initView(context);
	}
	
	private void initView(Context context){
		inflate(context, R.layout.view_personal_center_merchant_btns, this);
		commentLayout = (LinearLayout) findViewById(R.id.personal_comment_btn);
		goodLayout = (LinearLayout) findViewById(R.id.personal_good_btn);
		fansLayout = (LinearLayout) findViewById(R.id.personal_fans_btn);
		callLayout = (LinearLayout) findViewById(R.id.personal_call_btn);
		
		commentLayout.setOnClickListener(this);
		goodLayout.setOnClickListener(this);
		fansLayout.setOnClickListener(this);
		callLayout.setOnClickListener(this);
		
		commentText = (TextView) findViewById(R.id.personal_comment_number);
		goodText = (TextView) findViewById(R.id.personal_good_number);
		fansText = (TextView) findViewById(R.id.personal_fans_number);
		callText = (TextView) findViewById(R.id.personal_call_number);
		
		commentDot = findViewById(R.id.personal_comment_dot);
		goodDot = findViewById(R.id.personal_good_dot);
		fansDot = findViewById(R.id.personal_fans_dot);
		callDot = findViewById(R.id.personal_call_dot);
		commentDot.setVisibility(View.GONE);
		goodDot.setVisibility(View.GONE);
		fansDot.setVisibility(View.GONE);
		callDot.setVisibility(View.GONE);
		
		mergeViewSize(context);
	}

	private void mergeViewSize(Context context){
		int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
		int layoutWidth = screenWidth - 2 * CommonUtils.dip2Px(context, 16);
		float scale = layoutWidth/981f;
		int width = (int) (225*scale);
		int margin = (int) (27 * scale);
		LayoutParams lp = (LayoutParams) commentLayout.getLayoutParams();
		lp.width = width;
		
		lp = (LayoutParams) goodLayout.getLayoutParams();
		lp.width = width;
		lp.leftMargin = margin;
		
		lp = (LayoutParams) fansLayout.getLayoutParams();
		lp.width = width;
		lp.leftMargin = margin;
		
		lp = (LayoutParams) callLayout.getLayoutParams();
		lp.width = width;
		lp.leftMargin = margin;
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch (id) {
		case R.id.personal_comment_btn:
			Intent goComment = new Intent(MApp.get(), MineCommentActivity.class);
			goComment.putExtra(Constants.BUNDLE_MERCHANT_LOC_ID, MApp.get().getUser().supplier_account.sa_loc_id);
			getContext().startActivity(goComment);
			break;
		case R.id.personal_good_btn:
			Intent goGoods = new Intent(MApp.get(), MyPublishActivity.class);
			goGoods.putExtra(Constants.BUNDLE_LIST_TYPE,
					MyPublishFragment.TYPE_PRAISE);
			getContext().startActivity(goGoods);
			break;
		case R.id.personal_fans_btn:
			Intent goFans = new Intent(MApp.get(), MineFansActivity.class);
			getContext().startActivity(goFans);
			break;
		case R.id.personal_call_btn:
			
			break;
		}
	}
	
	public void setCommentCount(String commentCount){
		commentText.setText(commentCount);
	}
	
	public void setPraiseCount(String praiseCount){
		goodText.setText(praiseCount);
	}
	
	public void setFansCount(String fansCount){
		fansText.setText(fansCount);
	}
	
	public void setCallCount(String callCount){
		callText.setText(callCount);
	}
}
