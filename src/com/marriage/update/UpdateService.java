package com.marriage.update;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.marriage.MApp;
import com.marriage.bean.UpdateBean;
import com.marriage.utility.AppConfig;
import com.marriage.utility.Constants;
import com.marriage.utility.D;

public class UpdateService extends IntentService {

	public UpdateService() {
		super("UPDATE");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		RequestFuture<String> rf = RequestFuture.newFuture();
		String url = AppConfig.INSTANCE.SERVER + Constants.URL_UPDATE;
		StringRequest sr = new StringRequest(Method.POST, url, rf, rf) {

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				// TODO Auto-generated method stub
				PackageInfo pi = null;
				try {
					pi = getPackageManager()
							.getPackageInfo(getPackageName(), 0);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Map<String, String> params = new HashMap<String, String>();
				params.put("v_code", pi == null ? "" : pi.versionName);
				params.put("app_type", "1");
				return params;
			}

		};
		try {
			D.error("UPDATE", "params: "+new String(sr.getBody()));
		} catch (AuthFailureError e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		MApp.get().getRequestQueue().add(sr);
		try {
			String result = rf.get(30, TimeUnit.SECONDS);
			D.error("UPDATE", "result: " + result);
			UpdateBean bean = new Gson().fromJson(result, UpdateBean.class);
			if (Constants.SUCCESS.equals(bean.op_code) && bean.is_new_version
					&& bean.new_version != null) {
				notifyUpdate(bean.new_version.fileurl);
			} else {
				notifyNoUpdate();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			notifyNoUpdate();
		}
	}

	private void notifyUpdate(String fileUrl) {
		Intent fire = new Intent(Constants.ACTION_NEW_UPDATE);
		fire.putExtra(Constants.BUNDLE_UPDATE_FILE_URL, fileUrl);
		sendBroadcast(fire);
	}

	private void notifyNoUpdate() {
		Intent fire = new Intent(Constants.ACTION_NO_UPDATE);
		sendBroadcast(fire);
	}

}
