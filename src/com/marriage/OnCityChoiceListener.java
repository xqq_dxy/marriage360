package com.marriage;

import com.marriage.bean.City;

public interface OnCityChoiceListener {
	void onChoice(City city);
}
